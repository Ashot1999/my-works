#include "headers/Queue.hpp"

namespace cd01 {

template <typename T, typename Sequence>
void
Queue<T, Sequence>::push(const T& d)
{
    Sequence::push_back(d);
}

template <typename T, typename Sequence>
void
Queue<T, Sequence>::pop()
{
    Sequence::pop_back();
}

template <typename T, typename Sequence>
T&
Queue<T, Sequence>::top()
{
    return *(Sequence::begin());
}

template <typename T, typename Sequence>
const T&
Queue<T, Sequence>::top() const
{
    return *(Sequence::begin());
}

template <typename T, typename Sequence>
Queue<T, Sequence>&
Queue<T, Sequence>::operator=(const Queue& rhv)
{
    Sequence::operator=(rhv.Sequence);
    return *this;
}

template <typename T, typename Sequence>
bool
Queue<T, Sequence>::empty() const
{
    return Sequence::empty();
}

template <typename T, typename Sequence>
size_t
Queue<T, Sequence>::size() const
{
    return Sequence::size();
}

template <typename T, typename Sequence>
bool
Queue<T, Sequence>::operator==(const Queue& rhv) const
{
    return Sequence::operator==(rhv);
}

template <typename T, typename Sequence>
bool
Queue<T, Sequence>::operator<(const Queue& rhv) const
{
    return Sequence::operator<(rhv);
}

} /// end of namespace cd01

