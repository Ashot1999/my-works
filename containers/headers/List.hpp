#ifndef __T_LIST_HPP__
#define __T_LIST_HPP__

#include <stddef.h>
#include <iostream>

namespace cd01 {
    template <typename T> class List;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const cd01::List<T>& rhv);
template <typename T>
std::istream& operator>>(std::istream& in, cd01::List<T>& rhv);

namespace cd01 {

template <typename T>
class List
{
private:
    struct Node {
        Node();
        Node(const T data, Node* next = NULL, Node* prev = NULL);
        T data_;
        Node* next_;
        Node* prev_;
    };

    template <typename Type>
    friend std::ostream& ::operator<<(std::ostream& out, const List<Type>& rhv);
    template <typename Type>
    friend std::istream& ::operator>>(std::istream& in, List<Type>& rhv);
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef Node* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

    class const_iterator {
        friend List<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        const value_type& operator*() const;
        const value_type* operator->() const;
        const_iterator& operator++();
        const_iterator operator++(int);
        const_iterator& operator--();
        const_iterator operator--(int);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
    protected:
        explicit const_iterator(pointer ptr);
        void setPtr(const pointer ptr); 
        pointer node() const;
        pointer next();
        pointer prev();
        void setNext(pointer node);
    private:
        pointer ptr_;
    };
 
    class iterator : public const_iterator {
        friend List<T>;
    public:
        iterator();
        iterator(const iterator& rhv);
        ~iterator();
        value_type* operator->() const;
        iterator& operator=(const iterator& rhv);
        value_type& operator*() const;
        iterator& operator++();
        iterator operator++(int);
        iterator& operator--();
        iterator operator--(int);
    private:
        explicit iterator(const pointer ptr);
    };

    class const_reverse_iterator : public const_iterator {
        friend List<T>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
        const_reverse_iterator& operator++();
        const_reverse_iterator operator++(int);
        const_reverse_iterator& operator--();
        const_reverse_iterator operator--(int);
        const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
    private:
        explicit const_reverse_iterator(const pointer ptr);
    };

    class reverse_iterator : public const_reverse_iterator {
        friend List<T>;
    public:
        reverse_iterator();
        reverse_iterator(const reverse_iterator& rhv);
        ~reverse_iterator();
        reverse_iterator& operator++();
        reverse_iterator operator++(int);
        reverse_iterator& operator--();
        reverse_iterator operator--(int);
        reverse_iterator& operator=(const reverse_iterator& rhv);
    private:
        explicit reverse_iterator(const pointer ptr);
    };

public:
    List();
    List(const size_type size);
    List(const int size, const T& initialValue);
    List(const List<T>& rhv);
    template <typename InputIterator>
    List(InputIterator first, InputIterator last);
    ~List();
    size_type size() const;
    bool empty() const;
    void resize(const size_type n);
    void resize(const size_type n, const T& initialValue);
    void push_back(const T& elem);
    void push_front(const T& elem);
    void pop_back();
    void pop_front();
    const List& operator=(const List<T>& rhv);
    bool operator==(const List<T>& rhv) const;
    bool operator!=(const List<T>& rhv) const;
    bool operator<(const List<T>& rhv) const;
    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
    const_reverse_iterator rbegin() const;
    reverse_iterator rbegin();
    const_reverse_iterator rend() const;
    reverse_iterator rend();
    iterator insert_after(iterator pos);
    iterator insert_after(iterator pos, const T& arg);
    void insert_after(iterator pos, const int n, const T& arg);
    template <typename InputIterator>
    void insert_after(iterator pos, InputIterator l, InputIterator r);
 
    iterator insert_before(iterator pos);
    iterator insert_before(iterator pos, const T& arg);
    iterator insert_before(iterator pos, const int n, const T& arg);
    template <typename InputIterator>
    void insert_before(iterator pos, InputIterator l, InputIterator r);

    iterator erase(iterator pos);
    iterator erase(iterator first, iterator last);
private:
    Node* begin_;
    Node* end_;

}; /// end of template class List

} /// end of namespace cd01

#include "sources/List.cpp"

#endif /// __T_SINGLELIST_HPP__

