#include "headers/Vector.hpp"
#include "headers/SingleList.hpp"
#include "headers/List.hpp"
#include "headers/Queue.hpp"
#include "headers/Stack.hpp"
#include <gtest/gtest.h>

TEST(VectorTest, DefaultConstSize)
{
    cd01::Vector<int> v;
    EXPECT_EQ(v.size(), 0);
}

TEST(VectorTest, SizeConstSize)
{
    cd01::Vector<int> v(5);
    EXPECT_EQ(v.size(), 5);
}

TEST(VectorTest, OutputStream)
{
    cd01::Vector<int> v1(5), v2(3, 3);
    std::stringstream s;
    std::string str;
    s << v1 << ' ' << v2 << std::endl;
    std::getline(s, str);
    EXPECT_STREQ(str.c_str(), "( 0 0 0 0 0 ) ( 3 3 3 )");
}

TEST(VectorTest, InputStream)
{
    cd01::Vector<int> v(5);
    std::stringstream s;
    s << "( 1 2 3 4 5 )";
    s >> v;
    for (size_t i = 0; i < 5; ++i) {
        EXPECT_EQ(v[i], i + 1);
    }
}

TEST(VectorTest, LessThanOperator)
{
    const cd01::Vector<int> v1(5), v2(40);
    const cd01::Vector<int> v3(5, 6), v4(5, 7);
    EXPECT_TRUE(v1 < v2);
    EXPECT_TRUE(v3 < v4);
}

TEST(VectorTest, EqualityOperator)
{
    const cd01::Vector<int> v1(5), v2(40);
    const cd01::Vector<int> v3(5), v4(5, 7);
    EXPECT_TRUE(v1 != v2);
    EXPECT_TRUE(v3 != v4);
    EXPECT_TRUE(v1 == v3);
}

TEST(VectorTest, CopyConstructor)
{
    const cd01::Vector<int> v1(5, 7);
    cd01::Vector<int> v2 = v1;
    cd01::Vector<int> v3(v1);
    EXPECT_TRUE(v1 == v2);
    EXPECT_TRUE(v1 == v3);
}

TEST(VectorTest, AssignmentOperator)
{
    const cd01::Vector<int> v1(5, 7);
    cd01::Vector<int> v2(4, 3);
    v2 = v1;
    EXPECT_TRUE(v1 == v2);
}

TEST(VectorTest, ReserveInt)
{
    cd01::Vector<int> v(2, 2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.reserve(10);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.reserve(5);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.reserve(15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }
}

void*
operator new(const size_t n)
{
    ///std::cout << "::operator new(" << n << ")" << std::endl;
    return ::malloc(n);
};

void*
operator new[](const size_t n)
{
    ///std::cout << "::operator new[](" << n << ")" << std::endl;
    return ::malloc(n);
};

struct A
{
    A() : data_(0) {
        ///std::cout << "A::A()" << std::endl;
    }
    A(const int data) : data_(data) {
        ///std::cout << "A::A()" << std::endl;
    }
    ~A() {
        ///std::cout << "A::~A()" << std::endl;
    }

    bool operator==(const A&) const { return true; }

    static void* operator new(const size_t n) {
        ///std::cout << "A::operator new(" << n << ")" << std::endl;
        return ::operator new(n);
    }
    static void* operator new[](const size_t n) {
        ///std::cout << "A::operator new[](" << n << ")" << std::endl;
        return ::operator new[](n);
    }

    int
    getData() const
    {
        return data_;
    }
private:
    int data_;
};

TEST(VectorTest, ReserveA)
{
    cd01::Vector<A> v(2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    
    v.reserve(10);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);

    v.reserve(5);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);

    v.reserve(15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 2);
}

TEST(VectorTest, ResizeInt)
{
    cd01::Vector<int> v(2, 2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.resize(1);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 1);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.resize(5, 5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], 5);
    }

    v.resize(5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], 5);
    }

    v.resize(15, 15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 15);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], 5);
    }
    for (size_t i = 5; i < 15; ++i) {
        EXPECT_EQ(v[i], 15);
    }
}

TEST(VectorTest, ResizeA)
{
    cd01::Vector<A> v(2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(1);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 1);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 15);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 5; i < 15; ++i) {
        EXPECT_EQ(v[i], A());
    }
}

TEST(VectorTest, ConstIterator)
{
    typedef cd01::Vector<int> V;
    typedef cd01::Vector<A> VA;
    typedef V::const_iterator VI;
    typedef VA::const_iterator VAI;

    const V v(2, 5); /// vector creation.
    VI it1 = v.begin(); /// copy constructor.
    VI it2; /// default constructor.
    it2 = it1; /// assignment operation.
    EXPECT_TRUE(*it2 == 5); /// equality check.

    const VA a(2, A(7));
    VAI it3 = a.begin();
    EXPECT_TRUE(it3->getData() == 7); /// calling a class function through a pointer.
    
    for ( ; it2 != v.end(); ++it2) { /// preikrement and inequality condition. 
        EXPECT_TRUE(*it2 == 5); /// equality check.
    }

    for (it2 = v.end() - 1; it2 != v.begin(); --it2) { /// predekriment and inequality condition.
        EXPECT_TRUE(*it2 == 5); /// equality check.
    }
}

TEST(VectorTest, Iterators)
{
    typedef cd01::Vector<int> V;
    typedef cd01::Vector<A> VA;
    typedef V::iterator VI;
    typedef VA::iterator VAI;

    V v(2, 5); /// vector creation.
    VI it1 = v.begin(); /// copy constructor.
    VI it2; /// default constructor.
    it2 = it1; /// assignment operation.
    EXPECT_TRUE(*it2 == 5); /// equality check.

    VA a(2, A(7));
    VAI it3 = a.begin();
    EXPECT_TRUE(it3->getData() == 7); /// calling a class function through a pointer.
    
    for ( ; it2 != v.end(); ++it2) { /// preikrement and inequality condition. 
        EXPECT_TRUE(*it2 == 5); /// equality check.
    }

    for (it2 = v.end() - 1; it2 != v.begin(); --it2) { /// predekriment and inequality condition.
        EXPECT_EQ(*it2, 5); /// equality check.
    }
 
    for ( ; it2 != v.end(); it2++) { /// postikrement and inequality condition. 
        EXPECT_TRUE(*it2 == 5); /// equality check.
    }

    for (it2 = v.end() - 1; it2 != v.begin(); it2--) { /// postdekriment and inequality condition.
        EXPECT_EQ(*it2, 5); /// equality check.
    }

}

TEST(VectorTest, Const_Reverse_Iterators)
{
    const typedef cd01::Vector<int> V;
    const typedef cd01::Vector<A> VA;
    typedef V::const_reverse_iterator VI;
    typedef VA::const_reverse_iterator VRI;

    const V v(2, 1);
    for (VI tmp = v.rbegin(); tmp != v.rend(); ++tmp) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (VI tmp = v.rend() - 1; tmp != v.rbegin() + 1; --tmp) {
        EXPECT_TRUE(*tmp == 1);
    }

    for (VI tmp = v.rbegin(); tmp != v.rend(); tmp++) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (VI tmp = v.rend() - 1; tmp != v.rbegin(); tmp--) {
        EXPECT_TRUE(*tmp == 1);
    }


    const cd01::Vector<A> a(2, A(7));
    VRI tmp = a.rbegin();
    EXPECT_TRUE(tmp->getData() == 7);
}

TEST(VectorTest, Reverse_Iterators)
{
    typedef cd01::Vector<int> V;
    typedef V::reverse_iterator VI;

    V v(2, 1);
    for (VI tmp = v.rbegin(); tmp != v.rend(); ++tmp) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (VI tmp = v.rend(); tmp != v.rbegin(); --tmp) {
        EXPECT_TRUE(*tmp == 1);
    }

    for (VI tmp = v.rbegin(); tmp != v.rend(); tmp++) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (VI tmp = v.rend(); tmp != v.rbegin(); tmp--) {
        EXPECT_TRUE(*tmp == 1);
    }


    typedef cd01::Vector<A> VA;
    typedef VA::reverse_iterator VRI;
    cd01::Vector<A> a(2, A(7));
    VRI tmp = a.rbegin();
    EXPECT_TRUE(tmp->getData() == 7);
}

TEST(VectorTest, Insert)
{
    typedef cd01::Vector<int> V;
    V v(5,5);

    V::iterator it = v.begin();
    it += 2;

    it = v.insert(it, 100);
    EXPECT_EQ(*it, 100);
    EXPECT_EQ(v.size(), 6);

    for (size_t i = 0; i < v.size(); ++i) {
        const int value = (i == 2) ? 100 : 5;
        EXPECT_EQ(v[i], value);
    }
}

TEST(VectorTest, Erase)
{
    typedef cd01::Vector<int> V;
    V v(5);

    int arg = 1;
    for (V::iterator it = v.begin(); it < v.end(); ++it) {
        *it = arg++;
    }
    
    V::iterator it = v.begin();
    it += 2;
    v.erase(it);
    EXPECT_EQ(v.size(), 4);

    for (size_t i = 0; i < v.size(); ++i) {
        const int value = (i >= 2) ? i + 2 : i + 1;
        EXPECT_EQ(v[i], value);
    }
}

TEST(VectorTest, InsertTwo)
{
    typedef cd01::Vector<int> V;
    V v(5,5);

    V::iterator it = v.begin() + 2;

    it = v.insert(it, 7, 5);
    EXPECT_EQ(v.size(), 12);
    EXPECT_TRUE(it == v.begin() + 2);

    for (size_t i = 0; i < v.size(); ++i) {
        EXPECT_EQ(v[i], 5);
    }
}

TEST(VectorTest, InsertThree)
{
    typedef cd01::Vector<int> V;
    
    V v1(10, 5);
    V v2(10, 2);

    V::iterator it1 = v1.begin();
    V::iterator it2 = v2.begin();

    v2.insert(it2 + 3, it1, it1 + 3);
    EXPECT_TRUE(v2.size() == 13);

    size_t quantity = 0;
    for (V::iterator i = v2.begin(); i != v2.end(); ++i) {
         if (*i == 5) {
            ++quantity;
         }
    }
    EXPECT_EQ(quantity, 3);
}

TEST(VectorTest, EraseTwo)
{
    typedef cd01::Vector<int> V;
    
    V v(10, 5);
    V::iterator it = v.begin();
    v.erase(it, it + 4);
    EXPECT_EQ(v.size(), 6);

    for (size_t i = 0; i != v.size(); ++i) {
        EXPECT_EQ(v[i], 5);
    }
}

TEST(VectorTest, RangeConstructor)
{
    typedef cd01::Vector<int> V;
    V v1(10, 2);

    V::iterator it = v1.begin();

    V v2(it, it + 5);

    EXPECT_EQ(v2.size(), 5);

    for (size_t i = 0; i != v2.size(); ++i) {
        EXPECT_EQ(v2[i], 2);
    }
}

/// SingleList

TEST(SingleListTest, DefaultConstSize)
{
    cd01::SingleList<int> l;
    EXPECT_EQ(l.size(), 0);
}

TEST(SingleListTest, SizeConstSize)
{
    cd01::SingleList<int> l(5);
    EXPECT_EQ(l.size(), 5);

    cd01::SingleList<int> y;
    EXPECT_EQ(y.empty(), 1);
}

TEST(SingleListTest, Erase)
{
    typedef cd01::SingleList<int> SL;
    typedef SL::iterator SLI;

    SL l(5, 1);
    SLI it = l.begin();
    l.erase(it);
    EXPECT_EQ(l.size(), 4);    

    for (SLI it = l.begin(); it != l.end(); ++it) {
        EXPECT_EQ(*it, 1);
    }

    l.erase(l.begin(), l.end());
    EXPECT_EQ(l.size(), 0);


    SL y(5, 1);
    SLI i = y.begin();
    for (int n = 0; n < 3; ++i, ++n);
    y.erase(i);
    EXPECT_EQ(y.size(), 4);
    

    SL x(10, 1);
    SLI tmp1 = x.begin();
    for (int i = 0; i < 3; ++tmp1, ++i);
    
    SLI tmp2 = x.begin();
    for (int i = 0; i < 8; ++tmp2, ++i);

    x.erase(tmp1, tmp2);
    EXPECT_EQ(x.size(), 5);

    for (SLI it = x.begin(); it != x.end(); ++it) {
        EXPECT_EQ(*it, 1);
    }
}

TEST(SingleListTest, Push_Back)
{
    typedef cd01::SingleList<int> SL;
    typedef SL::iterator SLI;

    SL l(5, 1);
    l.push_back(1);
    EXPECT_EQ(l.size(), 6);

    for (SLI it = l.begin(); it != l.end(); ++it) {
        EXPECT_EQ(*it, 1);
    }
}

TEST(SingleListTest, Push_Front)
{
    typedef cd01::SingleList<int> SL;
    typedef SL::iterator SLI;

    SL l(5, 1);
    l.push_front(2);
    SLI it = l.begin();
    EXPECT_EQ(l.size(), 6);

    for (; it != l.end(); ++it) {
        int value = (*it == 2 ? 2 : 1);
        EXPECT_EQ(*it, value);
    }
}

TEST(SingleListTest, Pop_Back)
{
    typedef cd01::SingleList<int> SL;
    typedef SL::iterator SLI;

    SL l(5, 1);
    l.pop_back();
    EXPECT_EQ(l.size(), 4);

    for (SLI it = l.begin(); it != l.end(); ++it) {
        EXPECT_EQ(*it, 1);
    }

    SL y(1, 3);
    y.pop_back();
    EXPECT_EQ(y.size(), 0); 
}

TEST(SingleListTest, Pop_Front)
{
    typedef cd01::SingleList<int> SL;

    SL l(5, 1);
    l.pop_front();
    EXPECT_EQ(l.size(), 4);
}

TEST(SingleListTest, InsertAfter)
{
    typedef cd01::SingleList<int> SL;
    typedef SL::iterator SLI;

    SL l(5, 1);
    SLI it = l.begin();
    
    l.insert_after(it, 3);
    EXPECT_EQ(l.size(), 6);

    for (; it != l.end(); ++it) {
        int value = (*it == 3 ? 3 : 1); 
        EXPECT_EQ(*it, value);
    }

    l.insert_after(l.begin(), 10, 3);
    EXPECT_EQ(l.size(), 16);
   
    SLI it2 = l.begin();
    for (; it2 != l.end(); ++it2) {
        int value = (*it2 == 3 ? 3 : 1); 
        EXPECT_EQ(*it2, value);
    }

    SL y(10);
    y.insert_after(y.begin(), l.begin(), l.end());
    EXPECT_EQ(y.size(), 26);
}

TEST(SingleListTest, InsertBefore)
{
    typedef cd01::SingleList<int> SL;
    typedef SL::iterator SLI;

    SL l(5, 1);
    SLI it = l.begin();
    l.insert_before(it, 2);
  

    EXPECT_EQ(l.size(), 6);
    l.insert_before(it, 3, 2);
    EXPECT_EQ(l.size(), 9);

    for (int i = 0; i < 3; ++i, ++it);

    for (SLI it = l.begin(); it != l.end(); ++it) {
        int value = (*it == 2) ? 2 : 1;
        EXPECT_EQ(*it, value);
    }    
}

TEST(SingleListTest, RangeConstructor)
{
    typedef cd01::SingleList<int> SL;
    typedef SL::iterator SLI;

    SL l(10, 1);
    SL y(l.begin(), l.end());

    for (SLI it1 = l.begin(), it2 = y.begin(); it1 != l.end(); ++it1, ++it2) {
        EXPECT_EQ(*it1, *it2);
    }
}

TEST(SingleListTest, EqualTest)
{
    typedef cd01::SingleList<int> SL;

    SL l(4, 3);
    SL y(8, 2);
    EXPECT_TRUE(l != y);
    l = y;
    EXPECT_EQ(l.size(), 8);
    EXPECT_TRUE(l == y);
}

/// List

TEST(ListTest, Erase)
{
    typedef cd01::List<int> L;
    typedef L::iterator LI;

    L l(5, 1);
    LI it = l.begin();
    l.erase(it);
    EXPECT_EQ(l.size(), 4);    

    for (LI it = l.begin(); it != l.end(); ++it) {
        EXPECT_EQ(*it, 1);
    }

    l.erase(l.begin(), l.end());
    EXPECT_EQ(l.size(), 0);

    L y(5, 1);
    LI i = y.begin();
    for (int n = 0; n < 3; ++i, ++n);
    y.erase(i);
    EXPECT_EQ(y.size(), 4);

    y.erase(y.end());
    EXPECT_EQ(y.size(), 3);
    
    for (LI it = y.begin(); it != y.end(); ++it) {
        EXPECT_EQ(*it, 1);
    }
}

TEST(ListTest, Push_Back)
{
    typedef cd01::List<int> L;
    typedef L::iterator LI;

    L l(5, 1);
    l.push_back(1);
    EXPECT_EQ(l.size(), 6);

    for (LI it = l.begin(); it != l.end(); ++it) {
        EXPECT_EQ(*it, 1);
    }
}


TEST(ListTest, Push_Front)
{
    typedef cd01::List<int> L;
    typedef L::iterator LI;

    L l(5, 1);
    l.push_front(2);
    LI it = l.begin();
    EXPECT_EQ(l.size(), 6);

    for (; it != l.end(); ++it) {
        int value = (*it == 2 ? 2 : 1);
        EXPECT_EQ(*it, value);
    }
}


TEST(ListTest, Pop_Back)
{
    typedef cd01::List<int> L;
    typedef L::iterator LI;

    L l(5, 1);
    l.pop_back();
    EXPECT_EQ(l.size(), 4);

    for (LI it = l.begin(); it != l.end(); ++it) {
        EXPECT_EQ(*it, 1);
    }

    L y(1, 3);
    y.pop_back();
    EXPECT_EQ(y.size(), 0); 
}


TEST(ListTest, Pop_Front)
{
    typedef cd01::SingleList<int> L;

    L l(5, 1);
    l.pop_front();
    EXPECT_EQ(l.size(), 4);
}


TEST(ListTest, InsertAfter)
{
    typedef cd01::List<int> L;
    typedef L::iterator LI;

    L l(5, 1);
    LI it = l.begin();
    
    l.insert_after(it, 3);
    EXPECT_EQ(l.size(), 6);

    for (; it != l.end(); ++it) {
        int value = (*it == 3 ? 3 : 1); 
        EXPECT_EQ(*it, value);
    }

    l.insert_after(l.begin(), 10, 3);
    EXPECT_EQ(l.size(), 16);
   
    LI it2 = l.begin();
    for (; it2 != l.end(); ++it2) {
        int value = (*it2 == 3 ? 3 : 1); 
        EXPECT_EQ(*it2, value);
    }

    L y(10);
    y.insert_after(y.begin(), l.begin(), l.end());
    EXPECT_EQ(y.size(), 26);
}


TEST(ListTest, InsertBefore)
{
    typedef cd01::List<int> L;
    typedef L::iterator LI;

    L l(5, 1);
    LI it = l.begin();
    l.insert_before(it, 2);
  

    EXPECT_EQ(l.size(), 6);
    l.insert_before(it, 3, 2);
    EXPECT_EQ(l.size(), 9);

    for (int i = 0; i < 3; ++i, ++it);

    for (LI it = l.begin(); it != l.end(); ++it) {
        int value = (*it == 2) ? 2 : 1;
        EXPECT_EQ(*it, value);
    }    
}


TEST(ListTest, RangeConstructor)
{
    typedef cd01::List<int> L;
    typedef L::iterator LI;

    L l(10, 1);
    L y(l.begin(), l.end());

    for (LI it1 = l.begin(), it2 = y.begin(); it1 != l.end(); ++it2, ++it1) {
        EXPECT_EQ(*it1, *it2);
    }
}


TEST(ListTest, EqualTest)
{
    typedef cd01::List<int> L;

    L l(4, 3);
    L y(8, 2);
    EXPECT_TRUE(l != y);
    l = y;
    EXPECT_EQ(l.size(), 8);
    EXPECT_TRUE(l == y);
}

TEST(ListTest, Reverse_Iterators)
{
    typedef cd01::List<int> L;
    typedef L::reverse_iterator LI;

    L l(2, 1);
    for (LI tmp = l.rbegin(); tmp != l.rend(); ++tmp) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (LI tmp = l.rend(); tmp != l.rbegin(); --tmp) {
        EXPECT_TRUE(*tmp == 1);
    }

    for (LI tmp = l.rbegin(); tmp != l.rend(); tmp++) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (LI tmp = l.rend(); tmp != l.rbegin(); tmp--) {
        EXPECT_TRUE(*tmp == 1);
    }


    typedef cd01::List<A> LA;
    typedef LA::reverse_iterator LRI;
    cd01::List<A> a(2, A(7));
    LRI tmp = a.rbegin();
    EXPECT_TRUE(*tmp == 7);
}

TEST(ListTest, Const_Reverse_iterator)
{
    const typedef cd01::List<int> L;
    const typedef cd01::List<A> LA;
    typedef L::const_reverse_iterator LI;
    typedef LA::const_reverse_iterator LRI;

    const L l(2, 1);
    for (LI tmp = l.rbegin(); tmp != l.rend(); ++tmp) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (LI tmp = l.rend(); tmp != l.rbegin(); --tmp) {
        EXPECT_TRUE(*tmp == 1);
    }

    for (LI tmp = l.rbegin(); tmp != l.rend(); tmp++) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (LI tmp = l.rend(); tmp != l.rbegin(); tmp--) {
        EXPECT_TRUE(*tmp == 1);
    }

    const cd01::List<A> a(2, A(7));
    LRI tmp = a.rbegin();
    EXPECT_TRUE(*tmp == 7);
}

TEST(QueueTest1, All)
{
    typedef cd01::Queue<int> Q;

    Q q;
    q.push(1);
    EXPECT_EQ(q.top(), 1);
    q.push(1);
    EXPECT_EQ(q.size(), 2);

    q.pop();
    EXPECT_EQ(q.size(), 1);
    q.pop();
    EXPECT_EQ(q.empty(), 1);

    q.push(1);
    q.push(5);
    EXPECT_EQ(q.size(), 2);
    
    Q y;
    y.push(1);
    y.push(5);
    EXPECT_EQ(y.size(), 2);

    EXPECT_TRUE(q == y);

    q.pop();
    EXPECT_TRUE(q < y);
}

TEST(QueueTest2, All)
{
    typedef cd01::Queue<int, cd01::List<int> > Q;

    Q q;
    q.push(1);
    EXPECT_EQ(q.top(), 1);
    q.push(1);
    EXPECT_EQ(q.size(), 2);

    q.pop();
    EXPECT_EQ(q.size(), 1);
    q.pop();
    EXPECT_EQ(q.empty(), 1);

    q.push(1);
    q.push(5);
    EXPECT_EQ(q.size(), 2);
    
    Q y;
    y.push(1);
    y.push(5);
    EXPECT_EQ(y.size(), 2);

    EXPECT_TRUE(q == y);

    q.pop();
    EXPECT_TRUE(q < y);
}

TEST(StackTest1, All)
{
    typedef cd01::Stack<int> S;

    S s;
    s.push(1);
    EXPECT_EQ(s.top(), 1);
    s.push(1);
    EXPECT_EQ(s.size(), 2);

    s.pop();
    EXPECT_EQ(s.size(), 1);
    s.pop();
    EXPECT_EQ(s.empty(), 1);

    s.push(1);
    s.push(5);
    EXPECT_EQ(s.size(), 2);
    
    S y;
    y.push(1);
    y.push(5);
    EXPECT_EQ(y.size(), 2);

    EXPECT_TRUE(s == y);

    s.pop();
    EXPECT_TRUE(s < y);
}

TEST(StackTest2, All)
{
    typedef cd01::Stack<int, cd01::List<int> > S;

    S s;
    s.push(1);
    EXPECT_EQ(s.top(), 1);
    s.push(1);
    EXPECT_EQ(s.size(), 2);

    s.pop();
    EXPECT_EQ(s.size(), 1);
    s.pop();
    EXPECT_EQ(s.empty(), 1);

    s.push(1);
    s.push(5);
    EXPECT_EQ(s.size(), 2);
    
    S y;
    y.push(1);
    y.push(5);
    EXPECT_EQ(y.size(), 2);

    EXPECT_TRUE(s == y);

    s.pop();
    EXPECT_TRUE(s < y);
}


int
main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

