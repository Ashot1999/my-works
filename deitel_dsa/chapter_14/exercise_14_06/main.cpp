#include <iostream>

template <typename T>
bool isEqualTo(const T& lhv, const T& rhv);

class Test
{
    friend bool operator==(const Test& lhv, const Test& rhv);
public:
    Test (const int value = 0) : data_(value) {}
private:
    int data_;
};

bool operator==(const Test& lhv, const Test& rhv) /// Only built-in objects can be compared, user-defined objects need operator overloading.
{
    return lhv.data_ == rhv.data_;
}

template <typename T>
bool
isEqualTo(const T& lhv, const T& rhv)
{
    return lhv == rhv;
}

int
main()
{
    int number1, number2;
    std::cin >> number1 >> number2;
    Test object1(number1);
    Test object2(number2);

    std::cout << "Test in two numbers: " << isEqualTo(number1, number2) << std::endl;
    std::cout << "Test in two objects: " << isEqualTo(object1, object2) << std::endl;
    return 0;
}

