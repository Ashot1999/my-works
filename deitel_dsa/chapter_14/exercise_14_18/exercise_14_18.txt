template <class T> Array <T> :: Array (int s);

This is creating a constructor for the template class Array with a front parameter int s.

In the implementation of this constructor, T will be counted as int.
