#include <iostream>
#include <unistd.h>

template <typename T>
void print(const T* array, const int arraySize);

template <typename T>
void bubbleSort(T* array, const int arraySize);

template <typename T>
void setElements(T* array, const int arraySize);

int
main()
{

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array elements: ";
    }
    const int ARRAY_SIZE = 10;
    int integerArray[ARRAY_SIZE] = {};

    setElements(integerArray, ARRAY_SIZE);
    std::cout << "Original integer array: ";
    print(integerArray, ARRAY_SIZE);
    bubbleSort(integerArray, ARRAY_SIZE);
    std::cout << "Sorting integer array: ";
    print(integerArray, ARRAY_SIZE);

    float floatArray[ARRAY_SIZE] = {};
    setElements(floatArray, ARRAY_SIZE);
    std::cout << "\nOriginal float array: ";
    print(floatArray, ARRAY_SIZE);
    bubbleSort(floatArray, ARRAY_SIZE);
    std::cout << "Sorting float array: ";
    print(floatArray, ARRAY_SIZE);   
    return 0;
}

template <typename T>
void
print(const T* array, const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

template <typename T>
void
bubbleSort(T* array, const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        for (int j = 0; j < arraySize - 1; ++j) {
            if (array[j] >= array[i]) {
                std::swap(array[j], array[i]);
            }
        }
    }
}

template <typename T>
void
setElements(T* array, const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}
