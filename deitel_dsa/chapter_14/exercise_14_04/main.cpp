#include <iostream>
#include <cassert>
#include <stdlib.h>
#include <unistd.h>
#include <string>

template <typename T>
int printArray(const T* array, const int arraySize, const int lowSubscript, const int highSubscript);

template <typename T>
void setArray(T* array, const int arraySize);

int
main()
{
    const int ARRAY_SIZE = 11;
    int integersArray[ARRAY_SIZE] = {};
    double doublesArray[ARRAY_SIZE] = {};
    char text[ARRAY_SIZE] = "Programmer";
    setArray(integersArray, ARRAY_SIZE);
    std::cout << "Original array: ";

    std::cout << printArray(integersArray, ARRAY_SIZE, 0, ARRAY_SIZE - 1) << " elements." << std::endl;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input low subscript: ";
    }
    int lowSubscript;
    std::cin >> lowSubscript;
    if (lowSubscript < 0 || lowSubscript >= ARRAY_SIZE) {
        std::cerr << "Error 1: The low subscript cannot be negative, exceed the size of the array, or be exactly to it\a" << std::endl;
        return 1;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input high subsctipt: ";
    }
    int highSubscript;
    std::cin >> highSubscript;
    if (highSubscript < 0 || highSubscript >= ARRAY_SIZE) {
        std::cerr << "Error 2: The high subscript cannot be negative, exceed the size of the array, or be exactly to it.\a" << std::endl;
        return 2;
    }
  
    std::cout << "Desired part of the array and number of elements (integers): ";
    std::cout << printArray(integersArray, ARRAY_SIZE, lowSubscript, highSubscript) << " elements." << std::endl;
    
    std::cout << "Desired part of the array and number of elements (doubles): ";
    std::cout << printArray(doublesArray, ARRAY_SIZE, lowSubscript, highSubscript) << " elements." << std::endl;

    std::cout << "Desired part of the array and number of elements (string): ";
    std::cout << printArray(text, ARRAY_SIZE, lowSubscript, highSubscript) << " elements." << std::endl;
    return 0;
}

template <typename T>
int
printArray(const T* array, const int arraySize, const int lowSubscript, const int highSubscript)
{
    assert(lowSubscript <= highSubscript && highSubscript < arraySize);
    for (int counter = lowSubscript; counter <= highSubscript; ++counter) {
        std::cout << array[counter] << " ";
    }
    std::cout << std::endl;
    return highSubscript - lowSubscript + 1;
}

template <typename T>
void
setArray(T* array, const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        array[i] = 1 + ::rand() % 50;
    }
}
