#include <iostream>
#include <vector>
#include <unistd.h>

template <typename T>
bool
palindrome(std::vector<T>& nums)
{
    typedef typename std::vector<T>::const_iterator const_iterator;
    
    for (const_iterator it1 = nums.begin(), it2 = nums.end() - 1; it1 < it2; ++it1, --it2) {
        if (*it1 != *it2) {
            return false;
        }
    }
    return true;
}

int
main()
{

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input amount of elements: ";
    }
    size_t amountOfElements;
    std::cin >> amountOfElements;
    
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input vector elements: ";
    }

    std::vector<int> v;
    for (size_t i = 0; i < amountOfElements; ++i) {
        int element;
        std::cin >> element;
        v.push_back(element);
    }

    if (palindrome(v)) {
        std::cout << "Is a palindrome." << std::endl;
        return 0;
    }
    
    std::cout << "Is a not palindrome." << std::endl;
    return 0;
}

