#include <iostream>
#include <unistd.h>
#include <vector>

void
bubbleSort(std::vector<int>& vect)
{
    for (size_t i = 0; i < vect.size(); ++i) {
        int counter = 0;
        for (size_t j = 0; j < vect.size() - i - 1; ++j) {
            if (vect[j] > vect[j + 1]) {
                std::swap(vect[j], vect[j + 1]);
                ++counter;
            }
        }
        if (0 == counter) {
            break;
        }
    }
}

void
print(const std::vector<int>& vect)
{
    for (size_t i = 0; i < vect.size(); ++i) {
        std::cout << vect[i] << " ";
    }
    std::cout << std::endl;
}

void
setVect(std::vector<int>& vect)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 elements of vector: ";
    }
    int number;
    for (int i = 0; i < 10; ++i) {
        std::cin >> number;
        vect.push_back(number);
    }
}

int
main()
{
    std::vector<int> vect;
    setVect(vect);
    print(vect);
    bubbleSort(vect);
    print(vect);
    return 0;
}

