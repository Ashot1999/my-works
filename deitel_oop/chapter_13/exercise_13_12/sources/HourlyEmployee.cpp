#include <iostream>
#include "headers/HourlyEmployee.hpp" 

HourlyEmployee::HourlyEmployee(const std::string& first, const std::string& last, const std::string& ssn,
                               const std::string& birthDate, const double hourlyWage, const double hoursWorked)
    : Employee(first, last, ssn, birthDate)
{
    setWage(hourlyWage);
    setHours(hoursWorked);
}

void
HourlyEmployee::setWage(const double hourlyWage)
{
    wage_ = (hourlyWage < 0.0 ? 0.0 : hourlyWage);
}

double
HourlyEmployee::getWage() const
{
    return wage_;
}

void
HourlyEmployee::setHours(const double hoursWorked)
{
    hours_ = (((hoursWorked >= 0.0) && (hoursWorked <= 168.0)) ? hoursWorked : 0.0);
}

double
HourlyEmployee::getHours() const
{
    return hours_;
}

double
HourlyEmployee::earnings() const
{
    if (getHours() <= 40) {
        return getWage() * getHours();
    } else {
        return 40 * getWage() + ((getHours() - 40) * getWage() * 1.5);
    }
}

void
HourlyEmployee::plusSalary(const size_t prize)
{
    wage_ += prize;
}

void
HourlyEmployee::print() const
{
    std::cout << "Hourly employee: ";
    Employee::print();
    std::cout << "Hourly wage: " << getWage() << "; hours worked: " << getHours() << std::endl;
}

void
HourlyEmployee::printBirthDate() const
{
    std::cout << getBirthDate().getDay() << '/' << getBirthDate().getMonth() << '/' << getBirthDate().getYear() << std::endl;
}

