#include <iostream>
#include "headers/SalariedEmployee.hpp"

SalariedEmployee::SalariedEmployee(const std::string& first, const std::string& last, 
                                   const std::string& ssn, const std::string& birthDate, const double weeklySalary)
    : Employee(first, last, ssn, birthDate)
{
    setWeeklySalary(weeklySalary);
}

void
SalariedEmployee::setWeeklySalary(const double weeklySalary)
{
    weeklySalary_ = (weeklySalary < 0.0) ? 0.0 : weeklySalary;
}

double
SalariedEmployee::getWeeklySalary() const
{
    return weeklySalary_;
}

double
SalariedEmployee::earnings() const
{
    return getWeeklySalary();
}

void
SalariedEmployee::plusSalary(const size_t prize)
{
    weeklySalary_ += prize;
}

void
SalariedEmployee::print() const
{
    std::cout << "Salaried employee: ";
    Employee::print();
    std::cout << "Weekly salary: " << getWeeklySalary() << std::endl;
}

void
SalariedEmployee::printBirthDate() const
{
    std::cout << getBirthDate().getDay() << '/' << getBirthDate().getMonth() << '/' << getBirthDate().getYear() << std::endl;
}

