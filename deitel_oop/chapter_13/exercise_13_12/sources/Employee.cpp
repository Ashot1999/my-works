#include <iostream>
#include "headers/Employee.hpp"
#include "headers/Date.hpp"

Employee::Employee(const std::string& first, const std::string& last, const std::string& ssn, const std::string& birthDate)
    : firstName_(first)
    , lastName_(last)
    , socialSecurityNumber_(ssn)
    , birthDate_(birthDate) {}

void
Employee::setFirstName(const std::string& first)
{
    firstName_ = first;
}

const std::string&
Employee::getFirstName() const
{
    return firstName_;
}

void
Employee::setLastName(const std::string& last)
{
    lastName_ = last;
}

const std::string&
Employee::getLastName() const
{
    return lastName_;
}

void
Employee::setSocialSecurityNumber(const std::string& ssn)
{
    socialSecurityNumber_ = ssn;
}

const std::string&
Employee::getSocialSecurityNumber() const
{
    return socialSecurityNumber_;
}

const Date&
Employee::getBirthDate() const
{
    return birthDate_;
}

void
Employee::print() const
{
    std::cout << getFirstName() << ' ' << getLastName() << "\nsocial security number: " << getSocialSecurityNumber() << std::endl;
}

void
Employee::printBirthDate() const
{
    std::cout << birthDate_.getDay() << '/' << birthDate_.getMonth() << '/' << birthDate_.getYear() << std::endl;

}

