#include <iostream>
#include "headers/BasePlusCommissionEmployee.hpp"

BasePlusCommissionEmployee::BasePlusCommissionEmployee( const std::string& first, const std::string& last, const std::string& ssn,
                                                        const std::string& birthDate, const double sales, const double rate, const double salary)
    : CommissionEmployee(first, last, ssn, birthDate, sales, rate)
{
    setBaseSalary(salary);
}

void
BasePlusCommissionEmployee::setBaseSalary(const double salary)
{
    baseSalary_ = ((salary < 0.0) ? 0.0 : salary);
}

double
BasePlusCommissionEmployee::getBaseSalary() const
{
    return baseSalary_;
}

double
BasePlusCommissionEmployee::earnings() const
{
    return getBaseSalary() + CommissionEmployee::earnings();
}

void
BasePlusCommissionEmployee::plusSalary(const size_t prize)
{
    baseSalary_ += prize;
}

void
BasePlusCommissionEmployee::print() const
{
    std::cout << "Base-salaried ";
    CommissionEmployee::print();
    std::cout << "Base salary: " << getBaseSalary() << std::endl;
}

void
BasePlusCommissionEmployee::printBirthDate() const
{
    std::cout << getBirthDate().getDay() << '/' << getBirthDate().getMonth() << '/' << getBirthDate().getYear() << std::endl;
}

