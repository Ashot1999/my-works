#ifndef __COMMISSION_HPP__
#define __COMMISSION_HPP__

#include <string>
#include "headers/Employee.hpp"

class CommissionEmployee : public Employee
{
public:
    CommissionEmployee(const std::string& firstName, const std::string& lastName, const std::string& ssn,
                       const std::string& birthDate, const double sales = 0.0, const double rate = 0.0);
    
    void setCommissionRate(const double rate);
    double getCommissionRate() const;
    void setGrossSales(const double sales);
    double getGrossSales() const;
    void plusSalary(const size_t prize);
    virtual double earnings() const;
    virtual void print() const;
    virtual void printBirthDate() const;
private:
    double grossSales_;
    double commissionRate_;
};

#endif /// __COMMISSION_HPP__

