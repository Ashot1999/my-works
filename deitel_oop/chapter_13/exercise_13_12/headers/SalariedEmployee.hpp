#ifndef __SALARIED_HPP__
#define __SALARIED_HPP__

#include "headers/Employee.hpp"

class SalariedEmployee : public Employee
{
public:
    SalariedEmployee(const std::string& firstName, const std::string& LastName,
                     const std::string& ssn, const std::string& birthDate, const double salary = 0.0);
    
    void setWeeklySalary(const double salary);
    double getWeeklySalary() const;
    void plusSalary(const size_t prize);
    virtual double earnings() const;
    virtual void print() const;
    virtual void printBirthDate() const;
private:
    double weeklySalary_;
};

#endif /// __SALARIED_HPP__

