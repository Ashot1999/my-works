#ifndef __HOURLY_HPP__
#define __HOURLY_HPP__

#include "headers/Employee.hpp"

class HourlyEmployee : public Employee
{
public:
    HourlyEmployee(const std::string& firstName, const std::string& lastName, const std::string& ssn, 
                   const std::string& birthDate, const double hourlyWage = 0.0, const double hoursWorked = 0.0);

    void setWage(const double wage);
    double getWage() const;
    void setHours(const double hours);
    double getHours() const;
    void plusSalary(const size_t prize);
    virtual double earnings() const;
    virtual void print() const;
    virtual void printBirthDate() const;
private:
    double wage_;
    double hours_;
};

#endif /// __HOURLY_HPP__

