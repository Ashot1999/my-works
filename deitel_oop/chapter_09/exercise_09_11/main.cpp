#include "headers/Rectangle.hpp"
#include <iostream>

int
main()
{
    Rectangle rectangle(4.1, 11.3);
    std::cout << "Rectangle perimeter: " << rectangle.perimeter() << std::endl;
    std::cout << "Reactangle area: " << rectangle.area() << std::endl;
    return 0;
}
