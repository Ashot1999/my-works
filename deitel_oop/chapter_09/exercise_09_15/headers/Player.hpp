#ifndef __PLAYER_HPP__
#define __PLAYER_HPP__

class Board;

class Player
{
public:
    Player(const char playerMark) : playerMark_(playerMark) {}
    int play(Board* board, const int row, const int column) const;
    int getMark() const;
private:
    const char playerMark_;
};

#endif /// __PLAYER_HPP__

