#ifndef __TICTACTOE_HPP__
#define __TICTACTOE_HPP__

class Player;
class Board;

class TicTacToe
{
public:
    int play(Board* board, Player* player1, Player* player2) const;
    void help() const;
private:
    bool isWin(Board* board, Player* player, const int playerNumber) const;
    int playLoop(Board* board, Player* player1, Player* player2) const;
  
};

#endif /// __TICTACTOE_HPP__

