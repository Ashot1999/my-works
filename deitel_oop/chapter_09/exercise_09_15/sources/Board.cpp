#include "headers/Board.hpp"
#include <iostream>
#include <cassert>
#include <stdlib.h>
#include <iostream>

Board::Board()
{
    constructBoard();
}

void
Board::printBoard(std::ostream& out) const
{
    for (int i = 0; i < BOARD_SIZE; ++i) {    
        for (int j = 0; j < BOARD_SIZE; ++j) {
            out << '|' << board_[i][j];
        } 
        out << '|' << std::endl;
    }
    out << std::endl;
}

void
Board::constructBoard()
{
    for (int i = 0; i < BOARD_SIZE; ++i) {
        for (int j = 0; j < BOARD_SIZE; ++j) {
            board_[i][j] = ' ';
        }
    }
}

int
Board::setMark(const int row, const int column, const char playerMark)
{
    assert('x' == playerMark || 'o' == playerMark || "Assert 1: You went off the board.\a\n"); 
    
    if (row >= BOARD_SIZE || column >= BOARD_SIZE) {
        return 1;
    }
 
    if (board_[row][column] != ' ') {
        return 2;
    }
    board_[row][column] = playerMark;
    printBoard(std::cout);
    return 0;
}

bool
Board::isWin(const int row, const int column, const char playerMark) const
{
    if (isRowWin(row, playerMark)) {
        return true;
    }
    
    if (isColumnWin(column, playerMark)) {
        return true;
    }

    if (isDiagonalWin(playerMark)) {
        return true;
    }
    return false;
}

bool
Board::isRowWin(const int row, const char playerMark) const
{
    for (int i = 0; i < BOARD_SIZE; ++i) {
        if (board_[row][i] != playerMark) {
            return false;
        }
    }
    return true;
}

bool
Board::isColumnWin(const int column, const char playerMark) const
{
    for (int i = 0; i < BOARD_SIZE; ++i) {
        if (board_[i][column] != playerMark) {
            return false;
        }
    }
    return true;
}

bool
Board::isDiagonalWin(const char playerMark) const
{
    return (isLeftDiagonalWin(playerMark) || isRightDiagonalWin(playerMark));
}

bool
Board::isLeftDiagonalWin(const char playerMark) const
{
    for (int i = 0; i < 3; ++i) {
        if (board_[i][i] != playerMark) {
            return false;
        }
    }
    return true;
}

bool
Board::isRightDiagonalWin(const char playerMark) const
{
    for (int i = 0; i < BOARD_SIZE; --i) {
        if (board_[i][BOARD_SIZE - 1 - i] != playerMark) {
            return false;
        }
    }
    return true;
}

