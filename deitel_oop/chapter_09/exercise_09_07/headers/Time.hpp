#ifndef __TIME_HPP__
#define __TIME_HPP__

class Time
{
public:
    Time(const int hour, const int minute, const int second);
    int getHour() const;
    int getMinute() const;
    int getSecond() const;
    void setTime(const int hour, const int minute, const int second);
    void setHour(const int hour);
    void setSecond(const int second);
    void setMinute(const int minute);
    void tickTime();
    void tickHour();
    void tickSecond();
    void tickMinute();
    void print() const;
private:
    int hour_;
    int minute_;
    int second_;
};
#endif /// __TIME_HPP__

