#include "headers/Time.hpp"
#include <iostream>
#include <iomanip>
#include <cassert>

Time::Time(const int hour, const int minute, const int second)
{
    setTime(hour, minute, second);
}

void
Time::setTime(const int hour, const int minute, const int second)
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
}

void
Time::print() const
{
    std::cout << std::setfill('0') << std::setw(2) << getHour()   << ":"
                                   << std::setw(2) << getMinute() << ":"
                                   << std::setw(2) << getSecond()
                                   << std::setfill(' ') << std::endl;
}

void
Time::setHour(const int hour)
{
    assert(hour < 24 && hour >= 0);
    hour_ = hour;
}

void
Time::setMinute(const int minute)
{
    assert(minute < 60 && minute >= 0);
    minute_ = minute;
}

void
Time::setSecond(const int second)
{
    assert(second < 60 && second >= 0);
    second_ = second;
}

void
Time::tickTime()
{
    tickSecond();
}

void
Time::tickSecond()
{
    ++second_;
    if (60 == second_) {
        second_ = 0;
        tickMinute();
    }
}

void
Time::tickMinute()
{
    ++minute_;
    if (60 == minute_) {
        minute_ = 0;
        tickHour();
    }
}

void
Time::tickHour()
{
    ++hour_;
    if (24 == hour_) {
        hour_ = 0;
    }
}

int
Time::getHour() const
{
    return hour_;
}

int
Time::getMinute() const
{
    return minute_;
}

int
Time::getSecond() const
{
    return second_;
}

