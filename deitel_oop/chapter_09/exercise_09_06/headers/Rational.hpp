#ifndef __RATIONAL_HPP__
#define __RATIONAL_HPP__

class Rational
{
public:
    Rational(const int numerator = 0, const int denominator = 1);
    Rational plus(const Rational& rhv) const;
    Rational minus(const Rational& rhv) const;
    Rational multiplication(const Rational& rhv) const;
    Rational division(const Rational& rhv) const;
    void printRational() const;
    void printFloatRational() const;
    void normalize();
private:
   int numerator_;
   int denominator_;
private:
    int gcd(int number1, int number2) const;
};
#endif /// __RATIONAL_HPP__

