#include "headers/Time.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input time: ";
    }

    int hour, minute, second;
    std::cin >> hour >> minute >> second;
    Time time1(hour, minute, second);
    time1.print();
    Time time2(hour, minute);
    time2.print();
    Time time3(hour);
    time3.print();
    Time time4;
    time4.print();
    return 0;
}

