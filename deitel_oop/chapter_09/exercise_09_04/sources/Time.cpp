#include "headers/Time.hpp"
#include <iostream>
#include <iomanip>
#include <cassert>

Time::Time(const int hour, const int minute, const int second)
{
    setTime(hour, minute, second);
}

Time::Time()
{
    const time_t currentTime = ::time(NULL);
    const int hour = ((static_cast<int>(currentTime) % 86400 / 3600) + 4) % 24;
    const int minute = static_cast<int>(currentTime) % 3600 / 60;
    const int second = static_cast<int>(currentTime) % 60;
    setTime(hour, minute, second);
}

void
Time::setTime(const int hour, const int minute, const int second)
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
}

void
Time::print()
{
    std::cout << std::setfill('0') << std::setw(2) << getHour()   << ":"
                                   << std::setw(2) << getMinute() << ":"
                                   << std::setw(2) << getSecond()
                                   << std::setfill(' ') << std::endl;
}

void
Time::setHour(const int hour)
{
    assert(hour < 24 && hour >= 0);
    hour_ = hour;
}

void
Time::setMinute(const int minute)
{
    assert(minute >= 0 && minute < 60);
    minute_ = minute;
}

void
Time::setSecond(const int second)
{
    assert(second >= 0 && second < 60);
    second_ = second;
}

int
Time::getHour()
{
    return hour_;
}

int
Time::getMinute()
{
    return minute_;
}

int
Time::getSecond()
{
    return second_;
}

