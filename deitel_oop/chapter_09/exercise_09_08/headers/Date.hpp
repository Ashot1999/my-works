#ifndef __DATE_HPP__
#define __DATE_HPP__

class Date
{
public:
    Date(const int month = 1, const int day = 1, const int year = 2000);
    void nextDay();
    void nextMonth();
    void nextYear();
    void print() const;
    bool isLeapYear() const;
private:
    int day_;
    int month_;
    int year_;
};
#endif /// __DATE_HPP__

