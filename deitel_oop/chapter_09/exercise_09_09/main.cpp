#include "headers/DateAndTime.hpp"
#include <iostream>

int
main()
{
    DateAndTime dateAndTime(2000, 1, 21, 23, 59, 55);

    for (int i = 0; i < 10; ++i) {
        dateAndTime.nextTime();
        dateAndTime.printTime();
        std::cout << "\n";
    }
    return 0;
}
