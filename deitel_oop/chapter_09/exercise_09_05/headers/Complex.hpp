#ifndef __COMPLEX_HPP__
#define __COMPLEX__HPP__

class Complex
{
public:
    Complex(const double realNumber = 0.0, const double imaginaryNumber = 0.0);
    Complex plus(const Complex& rhv) const;
    Complex minus(const Complex& rhv) const;
    double getRealPart() const;
    double getImaginaryPart() const;
    void print() const;
private:
    double realPart_;
    double imaginaryPart_;
};
#endif
/// __COMPLEX_HPP__

