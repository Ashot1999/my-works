#include "headers/Complex.hpp"
#include <iostream>
#include <cmath>

Complex::Complex(const double realPart, const double imaginaryPart)
{
    realPart_ = realPart;
    imaginaryPart_ = imaginaryPart;
}

Complex
Complex::plus(const Complex& rhv) const
{
    const Complex total(realPart_ + rhv.realPart_, imaginaryPart_ + rhv.imaginaryPart_);
    return total;
}

Complex
Complex::minus(const Complex& rhv) const
{
    const Complex total(realPart_ - rhv.realPart_, imaginaryPart_ - rhv.imaginaryPart_);
    return total;
}

double
Complex::getRealPart() const
{
    return realPart_;
}

double
Complex::getImaginaryPart() const
{
    return imaginaryPart_;
}

void
Complex::print() const
{
    std::cout << "(" << getRealPart() << " + " << getImaginaryPart() << "i)";
}

