#include "headers/Time.hpp"
#include <iostream>
#include <iomanip>
#include <cassert>
#include <stdlib.h>

int
Time::setTime(const int hour, const int minute, const int second)
{
    int errorCode = setHour(hour);
    if (errorCode) {
        return errorCode;
    }
    
    errorCode = setMinute(minute);
    if (errorCode) {
        return errorCode;
    }

    return setSecond(second);
}

void
Time::print() const
{
    std::cout << std::setfill('0') << std::setw(2) << getHour()   << ":"
                                   << std::setw(2) << getMinute() << ":"
                                   << std::setw(2) << getSecond()
                                   << std::setfill(' ') << std::endl;
}

int
Time::setHour(const int hour)
{
    if (hour < 0 || hour >= 24) {
        return 1;
    }
    hour_ = hour;
    return 0;
}

int
Time::setMinute(const int minute)
{
    if (minute < 0 || minute >= 60) {
        return 2;
    }
    minute_ = minute;
    return 0;
}

int
Time::setSecond(const int second)
{    
    if (second < 0 || second >= 60) {
        return 3;
    }
    second_ = second;
    return 0;
}

void
Time::tickTime()
{
    tickSecond();
}

void
Time::tickSecond()
{
    ++second_;
    if (60 == second_) {
        second_ = 0;
        tickMinute();
    }
}

void
Time::tickMinute()
{
    ++minute_;
    if (60 == minute_) {
        minute_ = 0;
        tickHour();
    }
}

void
Time::tickHour()
{
    ++hour_;
    if (24 == hour_) {
        hour_ = 0;
    }
}

int
Time::getHour() const
{
    return hour_;
}

int
Time::getMinute() const
{
    return minute_;
}

int
Time::getSecond() const
{
    return second_;
}

