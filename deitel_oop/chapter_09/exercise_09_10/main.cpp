#include "headers/Time.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input hour, minute, second: ";
    }
    int hour, minute, second;
    std::cin >> hour >> minute >> second;

    Time time1;
    switch (time1.setTime(hour, minute, second)) {
    case 1: std::cout << "Error 1: Invalide hour."  << std::endl;  return 1;
    case 2: std::cout << "Error 2: Invalide minute." << std::endl; return 2;
    case 3: std::cout << "Error 3: Invalide second." << std::endl; return 3; 
    }
    std::cout << "Time: ";
    time1.print();
    std::cout << "Incrementation.\n";

    for (int i = 0; i < 15; ++i) {
        time1.tickTime();
        time1.print();
    }
    return 0;
}

