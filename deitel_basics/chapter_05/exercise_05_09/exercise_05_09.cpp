#include <iostream>

int
main()
{
    int product = 1;
    for (int counter = 1; counter <= 15; counter += 2) {
        product *= counter;
    }
    std::cout << "Product of odd number between 1 and 15 is " << product << std::endl;
    return 0;
}
