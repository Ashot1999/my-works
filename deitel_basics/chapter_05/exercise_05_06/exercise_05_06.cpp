#include <iostream>

int
main()
{
    int sum = 0;
    int count = 0;
    std::cout << "Enter numbers: ";
    while (true) {
        int number;
        std::cin >> number;
        if (9999 == number) {
            break;           
        }
        sum += number;
        ++count;
    }
    if (0 == count) {
        std::cout << "Avarage for 0 numbers is not possible to calculate" << std::endl;
        return 0;
    }
    double summary = static_cast<double>(sum);
    double counter = static_cast<double>(count);
    std::cout << "Avarage is: " << summary / counter << std::endl;
    return 0;
}
