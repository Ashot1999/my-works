#include <iostream>
#include <iomanip>

int
main()
{
    for (int rate = 5; rate <=10; ++rate) {
        double percent = rate / 100.00;
        double principal = 24.00;
        std::cout << rate << '%' << std::endl;
        for (int year = 1626; year <= 2005; ++year) {
            principal += principal * percent;
            std::cout << "Year \t" << year << '\t' << principal << "$" << std::endl;
        }
        std::cout << std::endl;
    }
    return 0;
}
