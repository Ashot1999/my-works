#include <iostream>
#include <iomanip>

int
main()
{
    int counter = 1;
    long double piGreatest = 3.14159;
    long double epsilon = 0.000001;

    long double pi = 4.0;
    int denominator = 3;
    int sign = -1;
    for (int accuracy = 100; accuracy <= 100000; accuracy *= 10) {
        long double piGolden = static_cast<long double>(static_cast<int>(piGreatest * accuracy)) / accuracy;
        while (true) {
            pi += sign * 4.0 / denominator;
            denominator += 2;
            sign = -sign;
            ++counter;
            if (piGolden - pi <= epsilon && piGolden - pi >= -epsilon) {
                std::cout << pi << "\t" << counter << std::endl;
                break;
            }
        }
    }
    return 0;
}
