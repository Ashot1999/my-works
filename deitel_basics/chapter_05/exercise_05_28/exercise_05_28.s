	.text
	.file	"exercise_05_28.cpp"
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function __cxx_global_var_init
	.type	__cxx_global_var_init,@function
__cxx_global_var_init:                  # @__cxx_global_var_init
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movabsq	$_ZStL8__ioinit, %rdi
	callq	_ZNSt8ios_base4InitC1Ev
	movabsq	$_ZNSt8ios_base4InitD1Ev, %rax
	movq	%rax, %rdi
	movabsq	$_ZStL8__ioinit, %rsi
	movabsq	$__dso_handle, %rdx
	callq	__cxa_atexit
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	__cxx_global_var_init, .Lfunc_end0-__cxx_global_var_init
	.cfi_endproc
                                        # -- End function
	.text
	.globl	main                    # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movl	$0, -4(%rbp)
	movl	$1, -8(%rbp)
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	cmpl	$12, -8(%rbp)
	jg	.LBB1_30
# %bb.2:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-8(%rbp), %ecx
	addl	$-1, %ecx
	movl	%ecx, %edx
	subl	$11, %ecx
	movq	%rdx, -16(%rbp)         # 8-byte Spill
	ja	.LBB1_15
# %bb.31:                               #   in Loop: Header=BB1_1 Depth=1
	movq	-16(%rbp), %rax         # 8-byte Reload
	movq	.LJTI1_0(,%rax,8), %rcx
	jmpq	*%rcx
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_4:                                #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.2, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_5:                                #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.3, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_6:                                #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.4, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_7:                                #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.5, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_8:                                #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.6, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_9:                                #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.7, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_10:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.8, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_11:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.9, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_12:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.10, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_13:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.11, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_15
.LBB1_14:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.12, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_15:                               #   in Loop: Header=BB1_1 Depth=1
	movl	$_ZSt4cout, %edi
	movl	$.L.str.13, %esi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %ecx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	movq	%rcx, -24(%rbp)         # 8-byte Spill
	callq	_ZNSolsEPFRSoS_E
	movl	$.L.str.14, %esi
	movq	%rax, %rdi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movq	-24(%rbp), %rsi         # 8-byte Reload
	callq	_ZNSolsEPFRSoS_E
	movl	-8(%rbp), %edx
	addl	$-1, %edx
	movl	%edx, %ecx
	subl	$11, %edx
	movq	%rcx, -32(%rbp)         # 8-byte Spill
	ja	.LBB1_28
# %bb.32:                               #   in Loop: Header=BB1_1 Depth=1
	movq	-32(%rbp), %rax         # 8-byte Reload
	movq	.LJTI1_1(,%rax,8), %rcx
	jmpq	*%rcx
.LBB1_16:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.15, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_17:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.16, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_18:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.17, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_19:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.18, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_20:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.19, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_21:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.20, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_22:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.21, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_23:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.22, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_24:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.23, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_25:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.24, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_26:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.25, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_27:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.26, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_28:                               #   in Loop: Header=BB1_1 Depth=1
	jmp	.LBB1_29
.LBB1_29:                               #   in Loop: Header=BB1_1 Depth=1
	movl	-8(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -8(%rbp)
	jmp	.LBB1_1
.LBB1_30:
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_3
	.quad	.LBB1_4
	.quad	.LBB1_5
	.quad	.LBB1_6
	.quad	.LBB1_7
	.quad	.LBB1_8
	.quad	.LBB1_9
	.quad	.LBB1_10
	.quad	.LBB1_11
	.quad	.LBB1_12
	.quad	.LBB1_13
	.quad	.LBB1_14
.LJTI1_1:
	.quad	.LBB1_27
	.quad	.LBB1_26
	.quad	.LBB1_25
	.quad	.LBB1_24
	.quad	.LBB1_23
	.quad	.LBB1_22
	.quad	.LBB1_21
	.quad	.LBB1_20
	.quad	.LBB1_19
	.quad	.LBB1_18
	.quad	.LBB1_17
	.quad	.LBB1_16
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_exercise_05_28.cpp
	.type	_GLOBAL__sub_I_exercise_05_28.cpp,@function
_GLOBAL__sub_I_exercise_05_28.cpp:      # @_GLOBAL__sub_I_exercise_05_28.cpp
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	__cxx_global_var_init
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end2:
	.size	_GLOBAL__sub_I_exercise_05_28.cpp, .Lfunc_end2-_GLOBAL__sub_I_exercise_05_28.cpp
	.cfi_endproc
                                        # -- End function
	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"On the "
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"first"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"second"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"third"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"fourth"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"fifth"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"sixth"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"seventh"
	.size	.L.str.7, 8

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"eighth"
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"ninth"
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"tenth"
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"eleventh"
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"twelfth"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"day of Christmas"
	.size	.L.str.13, 17

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"my true love sent to me:"
	.size	.L.str.14, 25

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"12 Drummers Drumming\n"
	.size	.L.str.15, 22

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Eleven Pipers Piping\n"
	.size	.L.str.16, 22

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Ten Lords a Leaping\n"
	.size	.L.str.17, 21

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Nine Ladies Dancing\n"
	.size	.L.str.18, 21

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Eight Maids a Milking\n"
	.size	.L.str.19, 23

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Seven Swans a Swimming\n"
	.size	.L.str.20, 24

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Six Geese a Laying\n"
	.size	.L.str.21, 20

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Five Golden Rings\n"
	.size	.L.str.22, 19

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Four Calling Birds\n"
	.size	.L.str.23, 20

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Three French Hens\n"
	.size	.L.str.24, 19

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Two Turtle Doves\nand "
	.size	.L.str.25, 22

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"a Partridge in a Pear Tree\n\n"
	.size	.L.str.26, 29

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_exercise_05_28.cpp
	.ident	"clang version 10.0.0-4ubuntu1 "
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym __cxx_global_var_init
	.addrsig_sym __cxa_atexit
	.addrsig_sym _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	.addrsig_sym _ZNSolsEPFRSoS_E
	.addrsig_sym _ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.addrsig_sym _GLOBAL__sub_I_exercise_05_28.cpp
	.addrsig_sym _ZStL8__ioinit
	.addrsig_sym __dso_handle
	.addrsig_sym _ZSt4cout
