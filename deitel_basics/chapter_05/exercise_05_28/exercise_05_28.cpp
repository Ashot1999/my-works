#include <iostream>

int
main()
{
    for (int day = 1; day <= 12; ++day) {
        std::cout << "On the ";
        switch (day) {
        case 1: std::cout << "first"; break;
        case 2: std::cout << "second"; break;
        case 3: std::cout << "third"; break;
        case 4: std::cout << "fourth"; break;
        case 5: std::cout << "fifth"; break;
        case 6: std::cout << "sixth"; break;
        case 7: std::cout << "seventh"; break;
        case 8: std::cout << "eighth"; break; 
        case 9: std::cout << "ninth"; break;
        case 10: std::cout << "tenth"; break;
        case 11: std::cout << "eleventh"; break;
        case 12: std::cout << "twelfth"; 
        }
        std::cout << "day of Christmas" << std::endl << "my true love sent to me:" << std::endl;
        
        switch (day) {
        case 12: std::cout << "12 Drummers Drumming\n";
        case 11: std::cout << "Eleven Pipers Piping\n";
        case 10: std::cout << "Ten Lords a Leaping\n";
        case 9: std::cout << "Nine Ladies Dancing\n";
        case 8: std::cout << "Eight Maids a Milking\n";
        case 7: std::cout << "Seven Swans a Swimming\n";
        case 6: std::cout << "Six Geese a Laying\n";
        case 5: std::cout << "Five Golden Rings\n";
        case 4: std::cout << "Four Calling Birds\n";
        case 3: std::cout << "Three French Hens\n";
        case 2: std::cout << "Two Turtle Doves\nand ";
        case 1: std::cout << "a Partridge in a Pear Tree\n\n";
        }
    }
    return 0;
}
