#include <iostream>

int
main()
{
    std::cout << "Enter numbers (first must be quantity over 0): ";
    int quantity;
    std::cin >> quantity;
    if (quantity < 0) {
        std::cerr << "Error 1: quantity must be over 0." << std::endl;
        return 1;
    }
    int sum = 0;
    for (int counter = 0; counter < quantity; ++counter) {
        int number;
        std::cin >> number;
        sum += number;
    }
    std::cout << "Sum: " << sum << std::endl;
    return 0;
}
