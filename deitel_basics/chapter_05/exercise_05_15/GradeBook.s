	.text
	.file	"GradeBook.cpp"
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function __cxx_global_var_init
	.type	__cxx_global_var_init,@function
__cxx_global_var_init:                  # @__cxx_global_var_init
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movabsq	$_ZStL8__ioinit, %rdi
	callq	_ZNSt8ios_base4InitC1Ev
	movabsq	$_ZNSt8ios_base4InitD1Ev, %rax
	movq	%rax, %rdi
	movabsq	$_ZStL8__ioinit, %rsi
	movabsq	$__dso_handle, %rdx
	callq	__cxa_atexit
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	__cxx_global_var_init, .Lfunc_end0-__cxx_global_var_init
	.cfi_endproc
                                        # -- End function
	.text
	.globl	_ZN9GradeBookC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_ # -- Begin function _ZN9GradeBookC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_
	.p2align	4, 0x90
	.type	_ZN9GradeBookC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_,@function
_ZN9GradeBookC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_: # @_ZN9GradeBookC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, %rdi
	movq	%rsi, -96(%rbp)         # 8-byte Spill
	movq	%rdx, -104(%rbp)        # 8-byte Spill
	movq	%rax, -112(%rbp)        # 8-byte Spill
	movq	%rcx, -120(%rbp)        # 8-byte Spill
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev
	movq	-112(%rbp), %rax        # 8-byte Reload
	addq	$32, %rax
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)        # 8-byte Spill
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev
.Ltmp0:
	leaq	-40(%rbp), %rdi
	movq	-96(%rbp), %rsi         # 8-byte Reload
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_
.Ltmp1:
	jmp	.LBB1_1
.LBB1_1:
.Ltmp2:
	leaq	-40(%rbp), %rsi
	movq	-120(%rbp), %rdi        # 8-byte Reload
	callq	_ZN9GradeBook13setCourseNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp3:
	jmp	.LBB1_2
.LBB1_2:
	leaq	-40(%rbp), %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
.Ltmp5:
	leaq	-88(%rbp), %rdi
	movq	-104(%rbp), %rsi        # 8-byte Reload
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_
.Ltmp6:
	jmp	.LBB1_3
.LBB1_3:
.Ltmp8:
	leaq	-88(%rbp), %rsi
	movq	-120(%rbp), %rdi        # 8-byte Reload
	callq	_ZN9GradeBook14setTeacherNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp9:
	jmp	.LBB1_4
.LBB1_4:
	leaq	-88(%rbp), %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
	addq	$128, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB1_5:
	.cfi_def_cfa %rbp, 16
.Ltmp7:
                                        # kill: def $edx killed $edx killed $rdx
	movq	%rax, -48(%rbp)
	movl	%edx, -52(%rbp)
	jmp	.LBB1_8
.LBB1_6:
.Ltmp4:
                                        # kill: def $edx killed $edx killed $rdx
	movq	%rax, -48(%rbp)
	movl	%edx, -52(%rbp)
	leaq	-40(%rbp), %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
	jmp	.LBB1_8
.LBB1_7:
.Ltmp10:
                                        # kill: def $edx killed $edx killed $rdx
	movq	%rax, -48(%rbp)
	movl	%edx, -52(%rbp)
	leaq	-88(%rbp), %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
.LBB1_8:
	movq	-128(%rbp), %rdi        # 8-byte Reload
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
	movq	-112(%rbp), %rdi        # 8-byte Reload
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
# %bb.9:
	movq	-48(%rbp), %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN9GradeBookC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_, .Lfunc_end1-_ZN9GradeBookC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	255                     # @TType Encoding = omit
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end0-.Lcst_begin0
.Lcst_begin0:
	.uleb128 .Ltmp0-.Lfunc_begin0   # >> Call Site 1 <<
	.uleb128 .Ltmp1-.Ltmp0          #   Call between .Ltmp0 and .Ltmp1
	.uleb128 .Ltmp7-.Lfunc_begin0   #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp2-.Lfunc_begin0   # >> Call Site 2 <<
	.uleb128 .Ltmp3-.Ltmp2          #   Call between .Ltmp2 and .Ltmp3
	.uleb128 .Ltmp4-.Lfunc_begin0   #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp5-.Lfunc_begin0   # >> Call Site 3 <<
	.uleb128 .Ltmp6-.Ltmp5          #   Call between .Ltmp5 and .Ltmp6
	.uleb128 .Ltmp7-.Lfunc_begin0   #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp8-.Lfunc_begin0   # >> Call Site 4 <<
	.uleb128 .Ltmp9-.Ltmp8          #   Call between .Ltmp8 and .Ltmp9
	.uleb128 .Ltmp10-.Lfunc_begin0  #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp9-.Lfunc_begin0   # >> Call Site 5 <<
	.uleb128 .Lfunc_end1-.Ltmp9     #   Call between .Ltmp9 and .Lfunc_end1
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end0:
	.p2align	2
                                        # -- End function
	.text
	.globl	_ZN9GradeBook13setCourseNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE # -- Begin function _ZN9GradeBook13setCourseNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align	4, 0x90
	.type	_ZN9GradeBook13setCourseNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,@function
_ZN9GradeBook13setCourseNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE: # @_ZN9GradeBook13setCourseNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end2:
	.size	_ZN9GradeBook13setCourseNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .Lfunc_end2-_ZN9GradeBook13setCourseNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
                                        # -- End function
	.globl	_ZN9GradeBook14setTeacherNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE # -- Begin function _ZN9GradeBook14setTeacherNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align	4, 0x90
	.type	_ZN9GradeBook14setTeacherNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,@function
_ZN9GradeBook14setTeacherNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE: # @_ZN9GradeBook14setTeacherNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$32, %rax
	movq	%rax, %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end3:
	.size	_ZN9GradeBook14setTeacherNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .Lfunc_end3-_ZN9GradeBook14setTeacherNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3               # -- Begin function _ZN9GradeBook11inputGradesEv
.LCPI4_0:
	.quad	4607182418800017408     # double 1
.LCPI4_1:
	.quad	4611686018427387904     # double 2
.LCPI4_2:
	.quad	4613937818241073152     # double 3
.LCPI4_3:
	.quad	4616189618054758400     # double 4
	.text
	.globl	_ZN9GradeBook11inputGradesEv
	.p2align	4, 0x90
	.type	_ZN9GradeBook11inputGradesEv,@function
_ZN9GradeBook11inputGradesEv:           # @_ZN9GradeBook11inputGradesEv
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movq	%rdi, -16(%rbp)
	movl	$0, -20(%rbp)
	movl	$0, -24(%rbp)
	movl	$0, -28(%rbp)
	movl	$0, -32(%rbp)
	xorps	%xmm0, %xmm0
	movsd	%xmm0, -40(%rbp)
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movabsq	$_ZSt3cin, %rdi
	callq	_ZNSi3getEv
	movl	%eax, -44(%rbp)
	cmpl	$-1, %eax
	je	.LBB4_10
# %bb.2:                                #   in Loop: Header=BB4_1 Depth=1
	movl	-44(%rbp), %eax
	addl	$-9, %eax
	movl	%eax, %ecx
	subl	$91, %eax
	movq	%rcx, -56(%rbp)         # 8-byte Spill
	ja	.LBB4_8
# %bb.14:                               #   in Loop: Header=BB4_1 Depth=1
	movq	-56(%rbp), %rax         # 8-byte Reload
	movq	.LJTI4_0(,%rax,8), %rcx
	jmpq	*%rcx
.LBB4_3:                                #   in Loop: Header=BB4_1 Depth=1
	movsd	.LCPI4_3(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	-40(%rbp), %xmm0
	movsd	%xmm0, -40(%rbp)
	movl	-20(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -20(%rbp)
	jmp	.LBB4_9
.LBB4_4:                                #   in Loop: Header=BB4_1 Depth=1
	movsd	.LCPI4_2(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	-40(%rbp), %xmm0
	movsd	%xmm0, -40(%rbp)
	movl	-24(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -24(%rbp)
	jmp	.LBB4_9
.LBB4_5:                                #   in Loop: Header=BB4_1 Depth=1
	movsd	.LCPI4_1(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	-40(%rbp), %xmm0
	movsd	%xmm0, -40(%rbp)
	movl	-28(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -28(%rbp)
	jmp	.LBB4_9
.LBB4_6:                                #   in Loop: Header=BB4_1 Depth=1
	movsd	.LCPI4_0(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	-40(%rbp), %xmm0
	movsd	%xmm0, -40(%rbp)
	movl	-32(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -32(%rbp)
	jmp	.LBB4_9
.LBB4_7:                                #   in Loop: Header=BB4_1 Depth=1
	jmp	.LBB4_9
.LBB4_8:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$1, -4(%rbp)
	jmp	.LBB4_13
.LBB4_9:                                #   in Loop: Header=BB4_1 Depth=1
	jmp	.LBB4_1
.LBB4_10:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.2, %rsi
	movq	%rax, -64(%rbp)         # 8-byte Spill
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-20(%rbp), %esi
	movq	%rax, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.3, %rsi
	movq	%rax, -72(%rbp)         # 8-byte Spill
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-24(%rbp), %esi
	movq	%rax, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.4, %rsi
	movq	%rax, -80(%rbp)         # 8-byte Spill
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-28(%rbp), %esi
	movq	%rax, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.5, %rsi
	movq	%rax, -88(%rbp)         # 8-byte Spill
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-32(%rbp), %esi
	movq	%rax, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	xorl	%ecx, %ecx
	movl	-20(%rbp), %edx
	addl	-24(%rbp), %edx
	addl	-28(%rbp), %edx
	addl	-32(%rbp), %edx
	movl	%edx, -48(%rbp)
	cmpl	-48(%rbp), %ecx
	jne	.LBB4_12
# %bb.11:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.6, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$2, -4(%rbp)
	jmp	.LBB4_13
.LBB4_12:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.7, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movsd	-40(%rbp), %xmm0        # xmm0 = mem[0],zero
	cvtsi2sdl	-48(%rbp), %xmm1
	divsd	%xmm1, %xmm0
	movq	%rax, %rdi
	callq	_ZNSolsEd
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$0, -4(%rbp)
.LBB4_13:
	movl	-4(%rbp), %eax
	addq	$96, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end4:
	.size	_ZN9GradeBook11inputGradesEv, .Lfunc_end4-_ZN9GradeBook11inputGradesEv
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_7
	.quad	.LBB4_7
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_7
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_3
	.quad	.LBB4_4
	.quad	.LBB4_5
	.quad	.LBB4_6
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_8
	.quad	.LBB4_3
	.quad	.LBB4_4
	.quad	.LBB4_5
	.quad	.LBB4_6
                                        # -- End function
	.text
	.globl	_ZN9GradeBook15printCourseNameEv # -- Begin function _ZN9GradeBook15printCourseNameEv
	.p2align	4, 0x90
	.type	_ZN9GradeBook15printCourseNameEv,@function
_ZN9GradeBook15printCourseNameEv:       # @_ZN9GradeBook15printCourseNameEv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	$_ZSt4cout, %edi
	movl	$.L.str.8, %esi
	movq	%rax, -96(%rbp)         # 8-byte Spill
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	leaq	-40(%rbp), %rcx
	movq	%rcx, %rdi
	movq	-96(%rbp), %rsi         # 8-byte Reload
	movq	%rax, -104(%rbp)        # 8-byte Spill
	movq	%rcx, -112(%rbp)        # 8-byte Spill
	callq	_ZN9GradeBook13getCourseNameB5cxx11Ev
.Ltmp11:
	movq	-104(%rbp), %rdi        # 8-byte Reload
	movq	-112(%rbp), %rsi        # 8-byte Reload
	callq	_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE
.Ltmp12:
	movq	%rax, -120(%rbp)        # 8-byte Spill
	jmp	.LBB5_1
.LBB5_1:
.Ltmp13:
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	-120(%rbp), %rdi        # 8-byte Reload
	callq	_ZNSolsEPFRSoS_E
.Ltmp14:
	jmp	.LBB5_2
.LBB5_2:
	leaq	-40(%rbp), %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
	movl	$_ZSt4cout, %edi
	movl	$.L.str.9, %esi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	leaq	-88(%rbp), %rcx
	movq	%rcx, %rdi
	movq	-96(%rbp), %rsi         # 8-byte Reload
	movq	%rax, -128(%rbp)        # 8-byte Spill
	movq	%rcx, -136(%rbp)        # 8-byte Spill
	callq	_ZN9GradeBook14getTeacherNameB5cxx11Ev
.Ltmp16:
	movq	-128(%rbp), %rdi        # 8-byte Reload
	movq	-136(%rbp), %rsi        # 8-byte Reload
	callq	_ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE
.Ltmp17:
	movq	%rax, -144(%rbp)        # 8-byte Spill
	jmp	.LBB5_3
.LBB5_3:
.Ltmp18:
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	-144(%rbp), %rdi        # 8-byte Reload
	callq	_ZNSolsEPFRSoS_E
.Ltmp19:
	jmp	.LBB5_4
.LBB5_4:
	leaq	-88(%rbp), %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
	addq	$144, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB5_5:
	.cfi_def_cfa %rbp, 16
.Ltmp15:
                                        # kill: def $edx killed $edx killed $rdx
	movq	%rax, -48(%rbp)
	movl	%edx, -52(%rbp)
	leaq	-40(%rbp), %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
	jmp	.LBB5_7
.LBB5_6:
.Ltmp20:
                                        # kill: def $edx killed $edx killed $rdx
	movq	%rax, -48(%rbp)
	movl	%edx, -52(%rbp)
	leaq	-88(%rbp), %rdi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev
.LBB5_7:
	movq	-48(%rbp), %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_ZN9GradeBook15printCourseNameEv, .Lfunc_end5-_ZN9GradeBook15printCourseNameEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	255                     # @TType Encoding = omit
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end1-.Lcst_begin1
.Lcst_begin1:
	.uleb128 .Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.uleb128 .Ltmp11-.Lfunc_begin1  #   Call between .Lfunc_begin1 and .Ltmp11
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp11-.Lfunc_begin1  # >> Call Site 2 <<
	.uleb128 .Ltmp14-.Ltmp11        #   Call between .Ltmp11 and .Ltmp14
	.uleb128 .Ltmp15-.Lfunc_begin1  #     jumps to .Ltmp15
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp14-.Lfunc_begin1  # >> Call Site 3 <<
	.uleb128 .Ltmp16-.Ltmp14        #   Call between .Ltmp14 and .Ltmp16
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp16-.Lfunc_begin1  # >> Call Site 4 <<
	.uleb128 .Ltmp19-.Ltmp16        #   Call between .Ltmp16 and .Ltmp19
	.uleb128 .Ltmp20-.Lfunc_begin1  #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp19-.Lfunc_begin1  # >> Call Site 5 <<
	.uleb128 .Lfunc_end5-.Ltmp19    #   Call between .Ltmp19 and .Lfunc_end5
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end1:
	.p2align	2
                                        # -- End function
	.text
	.globl	_ZN9GradeBook13getCourseNameB5cxx11Ev # -- Begin function _ZN9GradeBook13getCourseNameB5cxx11Ev
	.p2align	4, 0x90
	.type	_ZN9GradeBook13getCourseNameB5cxx11Ev,@function
_ZN9GradeBook13getCourseNameB5cxx11Ev:  # @_ZN9GradeBook13getCourseNameB5cxx11Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, %rax
	movq	%rdi, %rcx
	movq	%rcx, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rax, -24(%rbp)         # 8-byte Spill
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_
	movq	-24(%rbp), %rax         # 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end6:
	.size	_ZN9GradeBook13getCourseNameB5cxx11Ev, .Lfunc_end6-_ZN9GradeBook13getCourseNameB5cxx11Ev
	.cfi_endproc
                                        # -- End function
	.globl	_ZN9GradeBook14getTeacherNameB5cxx11Ev # -- Begin function _ZN9GradeBook14getTeacherNameB5cxx11Ev
	.p2align	4, 0x90
	.type	_ZN9GradeBook14getTeacherNameB5cxx11Ev,@function
_ZN9GradeBook14getTeacherNameB5cxx11Ev: # @_ZN9GradeBook14getTeacherNameB5cxx11Ev
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, %rax
	movq	%rdi, %rcx
	movq	%rcx, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rcx
	addq	$32, %rcx
	movq	%rcx, %rsi
	movq	%rax, -24(%rbp)         # 8-byte Spill
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1ERKS4_
	movq	-24(%rbp), %rax         # 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end7:
	.size	_ZN9GradeBook14getTeacherNameB5cxx11Ev, .Lfunc_end7-_ZN9GradeBook14getTeacherNameB5cxx11Ev
	.cfi_endproc
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_GradeBook.cpp
	.type	_GLOBAL__sub_I_GradeBook.cpp,@function
_GLOBAL__sub_I_GradeBook.cpp:           # @_GLOBAL__sub_I_GradeBook.cpp
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	__cxx_global_var_init
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end8:
	.size	_GLOBAL__sub_I_GradeBook.cpp, .Lfunc_end8-_GLOBAL__sub_I_GradeBook.cpp
	.cfi_endproc
                                        # -- End function
	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Input grade (A, B, C, D):\n"
	.size	.L.str, 27

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Error 1: invalid grade\n"
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A = "
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"B = "
	.size	.L.str.3, 5

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"C = "
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"D = "
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Error 2: The number of grades cannot be zero"
	.size	.L.str.6, 45

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Average: "
	.size	.L.str.7, 10

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Welcome to the "
	.size	.L.str.8, 16

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Teacher of this course is "
	.size	.L.str.9, 27

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_GradeBook.cpp
	.globl	_ZN9GradeBookC1ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_
	.type	_ZN9GradeBookC1ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_,@function
.set _ZN9GradeBookC1ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_, _ZN9GradeBookC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_
	.ident	"clang version 10.0.0-4ubuntu1 "
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym __cxx_global_var_init
	.addrsig_sym __cxa_atexit
	.addrsig_sym _ZN9GradeBook13setCourseNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.addrsig_sym __gxx_personality_v0
	.addrsig_sym _ZN9GradeBook14setTeacherNameENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.addrsig_sym _ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSERKS4_
	.addrsig_sym _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	.addrsig_sym _ZNSi3getEv
	.addrsig_sym _ZNSolsEPFRSoS_E
	.addrsig_sym _ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.addrsig_sym _ZNSolsEi
	.addrsig_sym _ZNSolsEd
	.addrsig_sym _ZStlsIcSt11char_traitsIcESaIcEERSt13basic_ostreamIT_T0_ES7_RKNSt7__cxx1112basic_stringIS4_S5_T1_EE
	.addrsig_sym _ZN9GradeBook13getCourseNameB5cxx11Ev
	.addrsig_sym _ZN9GradeBook14getTeacherNameB5cxx11Ev
	.addrsig_sym _GLOBAL__sub_I_GradeBook.cpp
	.addrsig_sym _Unwind_Resume
	.addrsig_sym _ZStL8__ioinit
	.addrsig_sym __dso_handle
	.addrsig_sym _ZSt4cout
	.addrsig_sym _ZSt3cin
