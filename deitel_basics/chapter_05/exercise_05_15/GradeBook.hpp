#include <cstring>

class GradeBook
{
public:
    GradeBook(std::string courseName, std::string teacherName);
    void setCourseName(std::string courseName);
    void setTeacherName(std::string teacherName);
    int inputGrades();
    std::string getCourseName();
    std::string getTeacherName();
    void printCourseName();
private:
    std::string courseName_;
    std::string teacherName_;
    int aCount_;
    int bCount_;
    int cCount_;
    int dCount_;
    int fCount_;
};
