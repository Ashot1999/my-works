#include <iostream>

int
main()
{
    std::cout << "Worker type(1 - manager, 2 - hourly workers, 3 - commission workers, 4 - pieceworkers): ";
    int workerType;
    std::cin >> workerType;
    std::cout << std::endl;

    switch (workerType) {
    case 1: {
        std::cout << "Enter salary per hour: ";
        int managerSalaryPerHour;
        std::cin >> managerSalaryPerHour;
        if (managerSalaryPerHour < 0) {
            std::cerr << "Error 1: invalid number" << std::endl;
            return 1;
        }
        std::cout << "Enter hours: ";
        int managerWorkedHours;
        std::cin >> managerWorkedHours;
        if (managerWorkedHours < 0) {
            std::cerr << "Error 2: invalid number" << std::endl;
            return 2;
        }
        int managerSalary = managerSalaryPerHour * managerWorkedHours;
        std::cout << "Your salary: " << managerSalary << std::endl;
        break;
    }
    case 2: {
        std::cout << "Enter worked hours: ";
        int hourlyWorkerWorkedHours;
        std::cin >> hourlyWorkerWorkedHours;
        if (hourlyWorkerWorkedHours < 0) {
            std::cerr << "Error 3: invalid number" << std::endl;
            return 3;
        }
        if (hourlyWorkerWorkedHours < 40) {
            std::cout << "You have not worked enough yet to get salary" << std::endl;
            break;
        }
        std::cout << "Enter salary per hour: ";
        int hourlyWorkerSalaryPerHour;
        std::cin >> hourlyWorkerSalaryPerHour;
        if (hourlyWorkerSalaryPerHour < 0) {
            std::cerr << "Error 4: invalid number" << std::endl;
            return 4;
        }
        int hourlyWorkerSalary = 40 * hourlyWorkerSalaryPerHour;
        if (hourlyWorkerWorkedHours > 40) {
            hourlyWorkerSalary += (hourlyWorkerWorkedHours - 40) * hourlyWorkerSalaryPerHour * 1.5;
        }
        std::cout << "Your salary: " << hourlyWorkerSalary << std::endl;
        break;
    }
    case 3: {
        std::cout << "Enter sales: ";
        double commisionWorkerSales;
        std::cin >> commisionWorkerSales;
        if ( commisionWorkerSales < 0) {
            std::cerr << "Error 5: invalid number" << std::endl;
            return 5;
        }
        double fixedSalary = 250.00;
        double rate = 5.7 / 100;
        double commisionWorkerSalary = fixedSalary + commisionWorkerSales * rate;
        std::cout << "Your salary: " << commisionWorkerSalary << std::endl;
        break;
    }
    case 4: {
        std::cout << "Enter your product sales: ";
        double pieceWorkerSales;
        std::cin >> pieceWorkerSales;
        if (pieceWorkerSales < 0) {
            std::cerr << "Error 6: invalid number" << std::endl;
            return 6;
        }
        std::cout << "Enter your rate: ";
        int pieceWorkerRate;
        std::cin >> pieceWorkerRate;
        if (pieceWorkerRate < 0) {
            std::cerr << "Error 7: invalid number" << std::endl;
            return 7;
        }
        double pieceWorkerSalary = pieceWorkerSales * pieceWorkerRate / 100;
        std::cout << "Your salary: " << pieceWorkerSalary << std::endl;
        break;
    }
    }
    return 0;
}
