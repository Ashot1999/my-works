	.text
	.file	"exercise_05_21.cpp"
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function __cxx_global_var_init
	.type	__cxx_global_var_init,@function
__cxx_global_var_init:                  # @__cxx_global_var_init
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movabsq	$_ZStL8__ioinit, %rdi
	callq	_ZNSt8ios_base4InitC1Ev
	movabsq	$_ZNSt8ios_base4InitD1Ev, %rax
	movq	%rax, %rdi
	movabsq	$_ZStL8__ioinit, %rsi
	movabsq	$__dso_handle, %rdx
	callq	__cxa_atexit
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	__cxx_global_var_init, .Lfunc_end0-__cxx_global_var_init
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3               # -- Begin function main
.LCPI1_0:
	.quad	4636737291354636288     # double 100
.LCPI1_1:
	.quad	4588375386756118217     # double 0.057000000000000002
.LCPI1_2:
	.quad	4643000109586448384     # double 250
.LCPI1_3:
	.quad	4609434218613702656     # double 1.5
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	movl	$0, -4(%rbp)
	movl	$_ZSt4cout, %eax
	movl	$.L.str, %esi
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)         # 8-byte Spill
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$_ZSt3cin, %edi
	leaq	-8(%rbp), %rsi
	movq	%rax, -104(%rbp)        # 8-byte Spill
	callq	_ZNSirsERi
	movl	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %esi
	movq	-96(%rbp), %rdi         # 8-byte Reload
	movq	%rax, -112(%rbp)        # 8-byte Spill
	callq	_ZNSolsEPFRSoS_E
	movl	-8(%rbp), %ecx
	addl	$-1, %ecx
	movl	%ecx, %edx
	subl	$3, %ecx
	movq	%rdx, -120(%rbp)        # 8-byte Spill
	ja	.LBB1_23
# %bb.25:
	movq	-120(%rbp), %rax        # 8-byte Reload
	movq	.LJTI1_0(,%rax,8), %rcx
	jmpq	*%rcx
.LBB1_1:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-12(%rbp), %rsi
	movq	%rax, -128(%rbp)        # 8-byte Spill
	callq	_ZNSirsERi
	cmpl	$0, -12(%rbp)
	jge	.LBB1_3
# %bb.2:
	movabsq	$_ZSt4cerr, %rdi
	movabsq	$.L.str.2, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$1, -4(%rbp)
	jmp	.LBB1_24
.LBB1_3:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.3, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-16(%rbp), %rsi
	movq	%rax, -136(%rbp)        # 8-byte Spill
	callq	_ZNSirsERi
	cmpl	$0, -16(%rbp)
	jge	.LBB1_5
# %bb.4:
	movabsq	$_ZSt4cerr, %rdi
	movabsq	$.L.str.4, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$2, -4(%rbp)
	jmp	.LBB1_24
.LBB1_5:
	movl	-12(%rbp), %eax
	imull	-16(%rbp), %eax
	movl	%eax, -20(%rbp)
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.5, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-20(%rbp), %esi
	movq	%rax, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	jmp	.LBB1_23
.LBB1_6:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.6, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-24(%rbp), %rsi
	movq	%rax, -144(%rbp)        # 8-byte Spill
	callq	_ZNSirsERi
	cmpl	$0, -24(%rbp)
	jge	.LBB1_8
# %bb.7:
	movabsq	$_ZSt4cerr, %rdi
	movabsq	$.L.str.7, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$3, -4(%rbp)
	jmp	.LBB1_24
.LBB1_8:
	cmpl	$40, -24(%rbp)
	jge	.LBB1_10
# %bb.9:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.8, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	jmp	.LBB1_23
.LBB1_10:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-28(%rbp), %rsi
	movq	%rax, -152(%rbp)        # 8-byte Spill
	callq	_ZNSirsERi
	cmpl	$0, -28(%rbp)
	jge	.LBB1_12
# %bb.11:
	movabsq	$_ZSt4cerr, %rdi
	movabsq	$.L.str.9, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$4, -4(%rbp)
	jmp	.LBB1_24
.LBB1_12:
	imull	$40, -28(%rbp), %eax
	movl	%eax, -32(%rbp)
	cmpl	$40, -24(%rbp)
	jle	.LBB1_14
# %bb.13:
	movsd	.LCPI1_3(%rip), %xmm0   # xmm0 = mem[0],zero
	movl	-24(%rbp), %eax
	subl	$40, %eax
	imull	-28(%rbp), %eax
	cvtsi2sd	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	cvtsi2sdl	-32(%rbp), %xmm0
	addsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	movl	%eax, -32(%rbp)
.LBB1_14:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.5, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-32(%rbp), %esi
	movq	%rax, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	jmp	.LBB1_23
.LBB1_15:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.10, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-40(%rbp), %rsi
	movq	%rax, -160(%rbp)        # 8-byte Spill
	callq	_ZNSirsERd
	xorps	%xmm0, %xmm0
	ucomisd	-40(%rbp), %xmm0
	jbe	.LBB1_17
# %bb.16:
	movabsq	$_ZSt4cerr, %rdi
	movabsq	$.L.str.11, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$5, -4(%rbp)
	jmp	.LBB1_24
.LBB1_17:
	movsd	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI1_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm1, -48(%rbp)
	movsd	%xmm0, -56(%rbp)
	movsd	-48(%rbp), %xmm0        # xmm0 = mem[0],zero
	movsd	-40(%rbp), %xmm1        # xmm1 = mem[0],zero
	mulsd	-56(%rbp), %xmm1
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -64(%rbp)
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.5, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movsd	-64(%rbp), %xmm0        # xmm0 = mem[0],zero
	movq	%rax, %rdi
	callq	_ZNSolsEd
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	jmp	.LBB1_23
.LBB1_18:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.12, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-72(%rbp), %rsi
	movq	%rax, -168(%rbp)        # 8-byte Spill
	callq	_ZNSirsERd
	xorps	%xmm0, %xmm0
	ucomisd	-72(%rbp), %xmm0
	jbe	.LBB1_20
# %bb.19:
	movabsq	$_ZSt4cerr, %rdi
	movabsq	$.L.str.13, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$6, -4(%rbp)
	jmp	.LBB1_24
.LBB1_20:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.14, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-76(%rbp), %rsi
	movq	%rax, -176(%rbp)        # 8-byte Spill
	callq	_ZNSirsERi
	cmpl	$0, -76(%rbp)
	jge	.LBB1_22
# %bb.21:
	movabsq	$_ZSt4cerr, %rdi
	movabsq	$.L.str.15, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$7, -4(%rbp)
	jmp	.LBB1_24
.LBB1_22:
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	-72(%rbp), %xmm1        # xmm1 = mem[0],zero
	cvtsi2sdl	-76(%rbp), %xmm2
	mulsd	%xmm2, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, -88(%rbp)
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.5, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movsd	-88(%rbp), %xmm0        # xmm0 = mem[0],zero
	movq	%rax, %rdi
	callq	_ZNSolsEd
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
.LBB1_23:
	movl	$0, -4(%rbp)
.LBB1_24:
	movl	-4(%rbp), %eax
	addq	$176, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_1
	.quad	.LBB1_6
	.quad	.LBB1_15
	.quad	.LBB1_18
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_exercise_05_21.cpp
	.type	_GLOBAL__sub_I_exercise_05_21.cpp,@function
_GLOBAL__sub_I_exercise_05_21.cpp:      # @_GLOBAL__sub_I_exercise_05_21.cpp
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	__cxx_global_var_init
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end2:
	.size	_GLOBAL__sub_I_exercise_05_21.cpp, .Lfunc_end2-_GLOBAL__sub_I_exercise_05_21.cpp
	.cfi_endproc
                                        # -- End function
	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Worker type(1 - manager, 2 - hourly workers, 3 - commission workers, 4 - pieceworkers): "
	.size	.L.str, 89

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Enter salary per hour: "
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Error 1: invalid number"
	.size	.L.str.2, 24

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Enter hours: "
	.size	.L.str.3, 14

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Error 2: invalid number"
	.size	.L.str.4, 24

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Your salary: "
	.size	.L.str.5, 14

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Enter worked hours: "
	.size	.L.str.6, 21

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Error 3: invalid number"
	.size	.L.str.7, 24

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"You have not worked enough yet to get salary"
	.size	.L.str.8, 45

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Error 4: invalid number"
	.size	.L.str.9, 24

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Enter sales: "
	.size	.L.str.10, 14

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Error 5: invalid number"
	.size	.L.str.11, 24

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Enter your product sales: "
	.size	.L.str.12, 27

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Error 6: invalid number"
	.size	.L.str.13, 24

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Enter your rate: "
	.size	.L.str.14, 18

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Error 7: invalid number"
	.size	.L.str.15, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_exercise_05_21.cpp
	.ident	"clang version 10.0.0-4ubuntu1 "
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym __cxx_global_var_init
	.addrsig_sym __cxa_atexit
	.addrsig_sym _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	.addrsig_sym _ZNSirsERi
	.addrsig_sym _ZNSolsEPFRSoS_E
	.addrsig_sym _ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.addrsig_sym _ZNSolsEi
	.addrsig_sym _ZNSirsERd
	.addrsig_sym _ZNSolsEd
	.addrsig_sym _GLOBAL__sub_I_exercise_05_21.cpp
	.addrsig_sym _ZStL8__ioinit
	.addrsig_sym __dso_handle
	.addrsig_sym _ZSt4cout
	.addrsig_sym _ZSt3cin
	.addrsig_sym _ZSt4cerr
