#include <iostream>
#include <cmath>
#include <iomanip>

int
main()
{
    int rate = 5;
    int startAmount = 1000;
    std::cout << std::setprecision(2) << std::endl;
    for (int counter = 1; counter <= 10; ++counter) {
        startAmount += startAmount * rate / 100;
        int cents = startAmount % 100;
        std::cout << counter 
                  << ":\t" 
                  << startAmount  
                  << " $\t" 
                  << cents  
                  << " cents" 
                  << std::endl; 
    }
    return 0;
}


