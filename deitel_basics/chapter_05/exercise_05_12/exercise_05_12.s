	.text
	.file	"exercise_05_12.cpp"
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function __cxx_global_var_init
	.type	__cxx_global_var_init,@function
__cxx_global_var_init:                  # @__cxx_global_var_init
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movabsq	$_ZStL8__ioinit, %rdi
	callq	_ZNSt8ios_base4InitC1Ev
	movabsq	$_ZNSt8ios_base4InitD1Ev, %rax
	movq	%rax, %rdi
	movabsq	$_ZStL8__ioinit, %rsi
	movabsq	$__dso_handle, %rdx
	callq	__cxa_atexit
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	__cxx_global_var_init, .Lfunc_end0-__cxx_global_var_init
	.cfi_endproc
                                        # -- End function
	.text
	.globl	main                    # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movl	$0, -4(%rbp)
	movl	$1, -8(%rbp)
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
	cmpl	$10, -8(%rbp)
	jg	.LBB1_11
# %bb.2:                                #   in Loop: Header=BB1_1 Depth=1
	movl	$1, -12(%rbp)
.LBB1_3:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$10, -12(%rbp)
	jg	.LBB1_9
# %bb.4:                                #   in Loop: Header=BB1_3 Depth=2
	movl	-12(%rbp), %eax
	cmpl	-8(%rbp), %eax
	jg	.LBB1_6
# %bb.5:                                #   in Loop: Header=BB1_3 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_7
.LBB1_6:                                #   in Loop: Header=BB1_3 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_7:                                #   in Loop: Header=BB1_3 Depth=2
	jmp	.LBB1_8
.LBB1_8:                                #   in Loop: Header=BB1_3 Depth=2
	movl	-12(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -12(%rbp)
	jmp	.LBB1_3
.LBB1_9:                                #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
# %bb.10:                               #   in Loop: Header=BB1_1 Depth=1
	movl	-8(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -8(%rbp)
	jmp	.LBB1_1
.LBB1_11:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$10, -16(%rbp)
.LBB1_12:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_14 Depth 2
	cmpl	$1, -16(%rbp)
	jl	.LBB1_22
# %bb.13:                               #   in Loop: Header=BB1_12 Depth=1
	movl	$1, -20(%rbp)
.LBB1_14:                               #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$10, -20(%rbp)
	jg	.LBB1_20
# %bb.15:                               #   in Loop: Header=BB1_14 Depth=2
	movl	-20(%rbp), %eax
	cmpl	-16(%rbp), %eax
	jle	.LBB1_17
# %bb.16:                               #   in Loop: Header=BB1_14 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_18
.LBB1_17:                               #   in Loop: Header=BB1_14 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_18:                               #   in Loop: Header=BB1_14 Depth=2
	jmp	.LBB1_19
.LBB1_19:                               #   in Loop: Header=BB1_14 Depth=2
	movl	-20(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -20(%rbp)
	jmp	.LBB1_14
.LBB1_20:                               #   in Loop: Header=BB1_12 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
# %bb.21:                               #   in Loop: Header=BB1_12 Depth=1
	movl	-16(%rbp), %eax
	addl	$-1, %eax
	movl	%eax, -16(%rbp)
	jmp	.LBB1_12
.LBB1_22:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$1, -24(%rbp)
.LBB1_23:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_25 Depth 2
	cmpl	$10, -24(%rbp)
	jg	.LBB1_33
# %bb.24:                               #   in Loop: Header=BB1_23 Depth=1
	movl	$1, -28(%rbp)
.LBB1_25:                               #   Parent Loop BB1_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$10, -28(%rbp)
	jg	.LBB1_31
# %bb.26:                               #   in Loop: Header=BB1_25 Depth=2
	movl	-28(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jl	.LBB1_28
# %bb.27:                               #   in Loop: Header=BB1_25 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_29
.LBB1_28:                               #   in Loop: Header=BB1_25 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_29:                               #   in Loop: Header=BB1_25 Depth=2
	jmp	.LBB1_30
.LBB1_30:                               #   in Loop: Header=BB1_25 Depth=2
	movl	-28(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -28(%rbp)
	jmp	.LBB1_25
.LBB1_31:                               #   in Loop: Header=BB1_23 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
# %bb.32:                               #   in Loop: Header=BB1_23 Depth=1
	movl	-24(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -24(%rbp)
	jmp	.LBB1_23
.LBB1_33:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$1, -32(%rbp)
.LBB1_34:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_36 Depth 2
	cmpl	$10, -32(%rbp)
	jg	.LBB1_44
# %bb.35:                               #   in Loop: Header=BB1_34 Depth=1
	movl	$10, -36(%rbp)
.LBB1_36:                               #   Parent Loop BB1_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$1, -36(%rbp)
	jl	.LBB1_42
# %bb.37:                               #   in Loop: Header=BB1_36 Depth=2
	movl	-36(%rbp), %eax
	cmpl	-32(%rbp), %eax
	jg	.LBB1_39
# %bb.38:                               #   in Loop: Header=BB1_36 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_40
.LBB1_39:                               #   in Loop: Header=BB1_36 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_40:                               #   in Loop: Header=BB1_36 Depth=2
	jmp	.LBB1_41
.LBB1_41:                               #   in Loop: Header=BB1_36 Depth=2
	movl	-36(%rbp), %eax
	addl	$-1, %eax
	movl	%eax, -36(%rbp)
	jmp	.LBB1_36
.LBB1_42:                               #   in Loop: Header=BB1_34 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
# %bb.43:                               #   in Loop: Header=BB1_34 Depth=1
	movl	-32(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -32(%rbp)
	jmp	.LBB1_34
.LBB1_44:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$1, -40(%rbp)
.LBB1_45:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_47 Depth 2
                                        #     Child Loop BB1_54 Depth 2
                                        #     Child Loop BB1_61 Depth 2
                                        #     Child Loop BB1_68 Depth 2
	cmpl	$10, -40(%rbp)
	jg	.LBB1_76
# %bb.46:                               #   in Loop: Header=BB1_45 Depth=1
	movl	$1, -44(%rbp)
.LBB1_47:                               #   Parent Loop BB1_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$10, -44(%rbp)
	jg	.LBB1_53
# %bb.48:                               #   in Loop: Header=BB1_47 Depth=2
	movl	-44(%rbp), %eax
	cmpl	-40(%rbp), %eax
	jg	.LBB1_50
# %bb.49:                               #   in Loop: Header=BB1_47 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_51
.LBB1_50:                               #   in Loop: Header=BB1_47 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_51:                               #   in Loop: Header=BB1_47 Depth=2
	jmp	.LBB1_52
.LBB1_52:                               #   in Loop: Header=BB1_47 Depth=2
	movl	-44(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -44(%rbp)
	jmp	.LBB1_47
.LBB1_53:                               #   in Loop: Header=BB1_45 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.2, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$10, -48(%rbp)
.LBB1_54:                               #   Parent Loop BB1_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$1, -48(%rbp)
	jl	.LBB1_60
# %bb.55:                               #   in Loop: Header=BB1_54 Depth=2
	movl	-48(%rbp), %eax
	cmpl	-40(%rbp), %eax
	jl	.LBB1_57
# %bb.56:                               #   in Loop: Header=BB1_54 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_58
.LBB1_57:                               #   in Loop: Header=BB1_54 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_58:                               #   in Loop: Header=BB1_54 Depth=2
	jmp	.LBB1_59
.LBB1_59:                               #   in Loop: Header=BB1_54 Depth=2
	movl	-48(%rbp), %eax
	addl	$-1, %eax
	movl	%eax, -48(%rbp)
	jmp	.LBB1_54
.LBB1_60:                               #   in Loop: Header=BB1_45 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.2, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$1, -52(%rbp)
.LBB1_61:                               #   Parent Loop BB1_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$10, -52(%rbp)
	jg	.LBB1_67
# %bb.62:                               #   in Loop: Header=BB1_61 Depth=2
	movl	-52(%rbp), %eax
	cmpl	-40(%rbp), %eax
	jl	.LBB1_64
# %bb.63:                               #   in Loop: Header=BB1_61 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_65
.LBB1_64:                               #   in Loop: Header=BB1_61 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_65:                               #   in Loop: Header=BB1_61 Depth=2
	jmp	.LBB1_66
.LBB1_66:                               #   in Loop: Header=BB1_61 Depth=2
	movl	-52(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -52(%rbp)
	jmp	.LBB1_61
.LBB1_67:                               #   in Loop: Header=BB1_45 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.2, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$10, -56(%rbp)
.LBB1_68:                               #   Parent Loop BB1_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$1, -56(%rbp)
	jl	.LBB1_74
# %bb.69:                               #   in Loop: Header=BB1_68 Depth=2
	movl	-56(%rbp), %eax
	cmpl	-40(%rbp), %eax
	jg	.LBB1_71
# %bb.70:                               #   in Loop: Header=BB1_68 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	jmp	.LBB1_72
.LBB1_71:                               #   in Loop: Header=BB1_68 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
.LBB1_72:                               #   in Loop: Header=BB1_68 Depth=2
	jmp	.LBB1_73
.LBB1_73:                               #   in Loop: Header=BB1_68 Depth=2
	movl	-56(%rbp), %eax
	addl	$-1, %eax
	movl	%eax, -56(%rbp)
	jmp	.LBB1_68
.LBB1_74:                               #   in Loop: Header=BB1_45 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
# %bb.75:                               #   in Loop: Header=BB1_45 Depth=1
	movl	-40(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -40(%rbp)
	jmp	.LBB1_45
.LBB1_76:
	xorl	%eax, %eax
	addq	$64, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_exercise_05_12.cpp
	.type	_GLOBAL__sub_I_exercise_05_12.cpp,@function
_GLOBAL__sub_I_exercise_05_12.cpp:      # @_GLOBAL__sub_I_exercise_05_12.cpp
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	__cxx_global_var_init
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end2:
	.size	_GLOBAL__sub_I_exercise_05_12.cpp, .Lfunc_end2-_GLOBAL__sub_I_exercise_05_12.cpp
	.cfi_endproc
                                        # -- End function
	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"*"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" "
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"   "
	.size	.L.str.2, 4

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_exercise_05_12.cpp
	.ident	"clang version 10.0.0-4ubuntu1 "
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym __cxx_global_var_init
	.addrsig_sym __cxa_atexit
	.addrsig_sym _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	.addrsig_sym _ZNSolsEPFRSoS_E
	.addrsig_sym _ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.addrsig_sym _GLOBAL__sub_I_exercise_05_12.cpp
	.addrsig_sym _ZStL8__ioinit
	.addrsig_sym __dso_handle
	.addrsig_sym _ZSt4cout
