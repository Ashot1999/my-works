#include <iostream>

int
main()
{
    for (int rate = 5; rate <= 10; ++rate) {    
        std::cout << "Enter start-ammount: " << std::endl;
        int ammount;
        std::cin >> ammount;
        if (ammount < 0) {
            std::cout << "Error 1: ammount can not be negative" << std::endl;
            return 1;
        }

        std::cout << "Enter years: " << std::endl;
        int years;
        std::cin >> years;
        if (years < 0) {
            std::cout << "Error 2: years can not be negative" << std::endl;
            return 2;
        }
        
        double percent = rate / 100.00;
        for (int counter = 1; counter <= years; ++counter) {
            ammount += ammount * percent;
            std::cout << "Year " << counter << ": " << ammount << std::endl;
        }
        
        std::cout << std::endl;
    }
    return 0;
}
