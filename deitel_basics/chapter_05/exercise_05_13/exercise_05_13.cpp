#include <iostream>

int
main()
{
    std::cout << "Enter quantity: ";
    int quantity;
    std::cin >> quantity;

    if (quantity <= 0) {
        std::cerr << "Error 1: quantity must be over 0" << std::endl;
        return 1;
    }

    for (int counter = 1; counter <= quantity; ++counter) {
        std::cout << "*";
    }
    std::cout << std::endl;
    return 0;
}
