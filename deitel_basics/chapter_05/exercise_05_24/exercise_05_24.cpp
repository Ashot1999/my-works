#include <iostream>
#include <cmath>

int
main()
{
    std::cout << "Input an odd number from 1 - 19: ";
    int size;
    std::cin >> size;
    if (size % 2 == 0 || size < 1 || size > 19) {
        std::cerr << "Error 1: invalid mumber" << std::endl;
        return 1;
    }
    int halfSize = size / 2;
    for (int row = -halfSize; row <= halfSize; ++row) {
        for (int column = -halfSize; column <= halfSize; ++column) {
            std::cout << (std::abs(row) + std::abs(column) <= halfSize ? "*" : " ");
        }
        std::cout << std::endl;
    }
    return 0;
}

