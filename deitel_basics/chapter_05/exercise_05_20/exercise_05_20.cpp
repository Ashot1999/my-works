#include <iostream>
#include <cmath>

int
main()
{
    int limitForSide1 = static_cast<int>(500 / std::sqrt(2)); /// limit is about 353, (353^2) * 2 is about 500.So it's enough for side1 reaching                                                                      this limit.
    for (int side1 = 3; side1 <= limitForSide1; ++side1) {
        int side1Square = side1 * side1;
        for (int side2 = 4; side2 <= 500; ++side2 ) {
            int side2Square = side2 * side2;
            for (int hypotenuse = 5; hypotenuse <= 500; ++hypotenuse) {
                int hypotenuseSquare = hypotenuse * hypotenuse;
                if (side1Square + side2Square == hypotenuseSquare) {
                    std::cout << side1 << '\t' << side2 << '\t' << hypotenuse << std::endl;
                }
            }
        }
    }
    return 0;
}
