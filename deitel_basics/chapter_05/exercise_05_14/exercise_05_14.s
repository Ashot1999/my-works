	.text
	.file	"exercise_05_14.cpp"
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function __cxx_global_var_init
	.type	__cxx_global_var_init,@function
__cxx_global_var_init:                  # @__cxx_global_var_init
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movabsq	$_ZStL8__ioinit, %rdi
	callq	_ZNSt8ios_base4InitC1Ev
	movabsq	$_ZNSt8ios_base4InitD1Ev, %rax
	movq	%rax, %rdi
	movabsq	$_ZStL8__ioinit, %rsi
	movabsq	$__dso_handle, %rdx
	callq	__cxa_atexit
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	__cxx_global_var_init, .Lfunc_end0-__cxx_global_var_init
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3               # -- Begin function main
.LCPI1_0:
	.quad	4619420950787396731     # double 6.8700000000000001
.LCPI1_1:
	.quad	4616741309009111286     # double 4.4900000000000002
.LCPI1_2:
	.quad	4621807858589903094     # double 9.9800000000000004
.LCPI1_3:
	.quad	4616752568008179712     # double 4.5
.LCPI1_4:
	.quad	4613892782244799447     # double 2.98
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movl	$0, -4(%rbp)
	xorps	%xmm0, %xmm0
	movss	%xmm0, -8(%rbp)
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-12(%rbp), %rsi
	movq	%rax, -24(%rbp)         # 8-byte Spill
	callq	_ZNSirsERi
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	movq	%rax, -32(%rbp)         # 8-byte Spill
	callq	_ZNSolsEPFRSoS_E
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	cmpl	-12(%rbp), %ecx
	jne	.LBB1_3
# %bb.2:
	jmp	.LBB1_15
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$0, -12(%rbp)
	jle	.LBB1_5
# %bb.4:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$5, -12(%rbp)
	jle	.LBB1_6
.LBB1_5:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$1, -4(%rbp)
	jmp	.LBB1_16
.LBB1_6:                                #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.2, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-16(%rbp), %rsi
	movq	%rax, -40(%rbp)         # 8-byte Spill
	callq	_ZNSirsERi
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	movq	%rax, -48(%rbp)         # 8-byte Spill
	callq	_ZNSolsEPFRSoS_E
	cmpl	$0, -16(%rbp)
	jge	.LBB1_8
# %bb.7:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.3, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$2, -4(%rbp)
	jmp	.LBB1_16
.LBB1_8:                                #   in Loop: Header=BB1_1 Depth=1
	movl	-12(%rbp), %eax
	addl	$-1, %eax
	movl	%eax, %ecx
	subl	$4, %eax
	movq	%rcx, -56(%rbp)         # 8-byte Spill
	ja	.LBB1_14
# %bb.17:                               #   in Loop: Header=BB1_1 Depth=1
	movq	-56(%rbp), %rax         # 8-byte Reload
	movq	.LJTI1_0(,%rax,8), %rcx
	jmpq	*%rcx
.LBB1_9:                                #   in Loop: Header=BB1_1 Depth=1
	movsd	.LCPI1_4(%rip), %xmm0   # xmm0 = mem[0],zero
	cvtsi2sdl	-16(%rbp), %xmm1
	mulsd	%xmm1, %xmm0
	movss	-8(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, -8(%rbp)
	jmp	.LBB1_14
.LBB1_10:                               #   in Loop: Header=BB1_1 Depth=1
	movsd	.LCPI1_3(%rip), %xmm0   # xmm0 = mem[0],zero
	cvtsi2sdl	-16(%rbp), %xmm1
	mulsd	%xmm1, %xmm0
	movss	-8(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, -8(%rbp)
	jmp	.LBB1_14
.LBB1_11:                               #   in Loop: Header=BB1_1 Depth=1
	movsd	.LCPI1_2(%rip), %xmm0   # xmm0 = mem[0],zero
	cvtsi2sdl	-16(%rbp), %xmm1
	mulsd	%xmm1, %xmm0
	movss	-8(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, -8(%rbp)
	jmp	.LBB1_14
.LBB1_12:                               #   in Loop: Header=BB1_1 Depth=1
	movsd	.LCPI1_1(%rip), %xmm0   # xmm0 = mem[0],zero
	cvtsi2sdl	-16(%rbp), %xmm1
	mulsd	%xmm1, %xmm0
	movss	-8(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, -8(%rbp)
	jmp	.LBB1_14
.LBB1_13:                               #   in Loop: Header=BB1_1 Depth=1
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	cvtsi2sdl	-16(%rbp), %xmm1
	mulsd	%xmm1, %xmm0
	movss	-8(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, -8(%rbp)
.LBB1_14:                               #   in Loop: Header=BB1_1 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.4, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movss	-8(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movq	%rax, %rdi
	callq	_ZNSolsEf
	movq	%rax, %rdi
	movabsq	$.L.str.5, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	jmp	.LBB1_1
.LBB1_15:
	movl	$0, -4(%rbp)
.LBB1_16:
	movl	-4(%rbp), %eax
	addq	$64, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_9
	.quad	.LBB1_10
	.quad	.LBB1_11
	.quad	.LBB1_12
	.quad	.LBB1_13
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_exercise_05_14.cpp
	.type	_GLOBAL__sub_I_exercise_05_14.cpp,@function
_GLOBAL__sub_I_exercise_05_14.cpp:      # @_GLOBAL__sub_I_exercise_05_14.cpp
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	__cxx_global_var_init
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end2:
	.size	_GLOBAL__sub_I_exercise_05_14.cpp, .Lfunc_end2-_GLOBAL__sub_I_exercise_05_14.cpp
	.cfi_endproc
                                        # -- End function
	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Enter the product number (1-5 products, -1 to exit): "
	.size	.L.str, 54

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Error 1: product number can not be negative or larger 5"
	.size	.L.str.1, 56

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Enter sold items quantity: "
	.size	.L.str.2, 28

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Error 2: quantity can not be negative"
	.size	.L.str.3, 38

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Total ammount: "
	.size	.L.str.4, 16

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" $"
	.size	.L.str.5, 3

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_exercise_05_14.cpp
	.ident	"clang version 10.0.0-4ubuntu1 "
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym __cxx_global_var_init
	.addrsig_sym __cxa_atexit
	.addrsig_sym _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	.addrsig_sym _ZNSirsERi
	.addrsig_sym _ZNSolsEPFRSoS_E
	.addrsig_sym _ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.addrsig_sym _ZNSolsEf
	.addrsig_sym _GLOBAL__sub_I_exercise_05_14.cpp
	.addrsig_sym _ZStL8__ioinit
	.addrsig_sym __dso_handle
	.addrsig_sym _ZSt4cout
	.addrsig_sym _ZSt3cin
