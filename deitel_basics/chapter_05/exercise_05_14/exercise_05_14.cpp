#include <iostream>

int
main()
{
    float totalAmmount = 0;
    while (true) {
        std::cout << "Enter the product number (1-5 products, -1 to exit): ";
        int productNumber;
        std::cin >> productNumber;
        std::cout << std::endl;
        if (-1 == productNumber) {
            break;
        }
        
        if (productNumber <= 0 || productNumber > 5) {
            std::cout << "Error 1: product number can not be negative or larger 5" << std::endl; 
            return 1;
        }

        std::cout << "Enter sold items quantity: ";
        int quantityPerDay;
        std::cin >> quantityPerDay;
        if (quantityPerDay < 0) {
            std::cout << "Error 2: quantity can not be negative";
            return 2;
        }
        std::cout << std::endl;
        
        switch (productNumber) {
        case 1: totalAmmount += 2.98 * quantityPerDay; break;
        case 2: totalAmmount += 4.50 * quantityPerDay; break;
        case 3: totalAmmount += 9.98 * quantityPerDay; break;
        case 4: totalAmmount += 4.49 * quantityPerDay; break;
        case 5: totalAmmount += 6.87 * quantityPerDay; break;
        }
        std::cout << "Total ammount: " << totalAmmount << " $" << std::endl;
    }

    return 0;
}
