#include <iostream>

int
main()
{
    int product = 1;
    for (int counter = 1; counter <= 5; ++counter) {
        std::cout << counter << "! = " << counter * product << std::endl;
        product *= counter;
    }
    return 0;
}

