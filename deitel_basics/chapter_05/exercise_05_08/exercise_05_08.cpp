#include <iostream>
#include<bits/stdc++.h>

int
main()
{
    std::cout << "Enter quantity: " << std::endl;
    int quantity;
    std::cin >> quantity;
    if (quantity <= 0) {
        std::cerr << "Error 1: quantity must be over 0" << std::endl;
        return 1;
    }
    int smallest = INT_MAX;
    std::cout << "Input numbers: ";
    for (int counter = 0; counter < quantity; counter++) {
        int number;
        std::cin >> number;
        if (number < smallest) {
            smallest = number;
        }
    }

    std::cout << std::endl << "The smallest number is " << smallest << std::endl;
    return 0;
}

