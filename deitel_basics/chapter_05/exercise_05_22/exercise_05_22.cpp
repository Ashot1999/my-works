#include <iostream>

int
main()
{
    ///a)
    std::cout << "Input 2 numbers: ";
    int x, y;
    std::cin >> x >> y;
    if  ((!(x < 5) && !(y >= 7)) == (!(x < 5 || y >= 7))) {
        std::cout << "!( x < 5 ) && !( y >= 7 ) and (!(x < 5 || y >= 7)) are equivalent" << std::endl;
    } else {
        std::cout << "!( x < 5 ) && !( y >= 7 ) and (!(x < 5 || y >= 7)) are not equivalent" << std::endl;
    }
    std::cout << std::endl;

    ///b)
    std::cout << "Input 3 numbers: ";
    int a, b, g;
    std::cin >> a >> b >> g;
    if (!( a == b ) || !( g != 5 ) == (!(a == b && g != 5))) {
        std::cout << "!( a == b ) || !( g != 5 ) and (!(a == b && g != 5)) are equivalent" << std::endl;
    } else {
        std::cout << "!( a == b ) || !( g != 5 ) and (!(a == b && g != 5)) are not equivalent" << std::endl;
    }
    std::cout << std::endl;

    ///c
    std::cout << "Input 2 numbers: ";
    int q, w;
    std::cin >> q >> y;
    if ((!(q <= 8 && w > 4 ) == (q > 8 || w <= 4))) {
        std::cout << "!( q <= 8 ) && ( w > 4 ) ) are equivalent (q > 8 || w <= 4)" << std::endl;
    } else {
        std::cout << "!(( q <= 8 ) && ( w > 4 )) and are not equivalent (q > 8 || w <= 4)" << std::endl;
    }
    std::cout << std::endl;

    ///d
    std::cout << "Input 2 numbers: ";
    int i, j;
    std::cin >> i >> j;
    if (!(i > 4 || j <= 6) == (i <= 4 && j > 6)) {
        std::cout << "!(i > 4 || j <= 6) and (i <= 4 && j > 6) are equivalent" << std::endl;
    } else {
        std::cout << "(!(i > 4 || j <= 6) and (i <= 4 && j > 6)) are not equal" << std::endl;
    } 
    return 0;
}
