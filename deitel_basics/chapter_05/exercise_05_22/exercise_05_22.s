	.text
	.file	"exercise_05_22.cpp"
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function __cxx_global_var_init
	.type	__cxx_global_var_init,@function
__cxx_global_var_init:                  # @__cxx_global_var_init
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movabsq	$_ZStL8__ioinit, %rdi
	callq	_ZNSt8ios_base4InitC1Ev
	movabsq	$_ZNSt8ios_base4InitD1Ev, %rax
	movq	%rax, %rdi
	movabsq	$_ZStL8__ioinit, %rsi
	movabsq	$__dso_handle, %rdx
	callq	__cxa_atexit
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	__cxx_global_var_init, .Lfunc_end0-__cxx_global_var_init
	.cfi_endproc
                                        # -- End function
	.text
	.globl	main                    # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$160, %rsp
	movl	$0, -4(%rbp)
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-8(%rbp), %rsi
	movq	%rax, -48(%rbp)         # 8-byte Spill
	callq	_ZNSirsERi
	movq	%rax, %rdi
	leaq	-12(%rbp), %rsi
	callq	_ZNSirsERi
	xorl	%ecx, %ecx
                                        # kill: def $cl killed $cl killed $ecx
	cmpl	$5, -8(%rbp)
	movb	%cl, -49(%rbp)          # 1-byte Spill
	jl	.LBB1_2
# %bb.1:
	cmpl	$7, -12(%rbp)
	setge	%al
	xorb	$-1, %al
	movb	%al, -49(%rbp)          # 1-byte Spill
.LBB1_2:
	movb	-49(%rbp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ecx
	cmpl	$5, -8(%rbp)
	movb	$1, %al
	movl	%ecx, -56(%rbp)         # 4-byte Spill
	movb	%al, -57(%rbp)          # 1-byte Spill
	jl	.LBB1_4
# %bb.3:
	cmpl	$7, -12(%rbp)
	setge	%al
	movb	%al, -57(%rbp)          # 1-byte Spill
.LBB1_4:
	movb	-57(%rbp), %al          # 1-byte Reload
	xorb	$-1, %al
	andb	$1, %al
	movzbl	%al, %ecx
	movl	-56(%rbp), %edx         # 4-byte Reload
	cmpl	%ecx, %edx
	jne	.LBB1_6
# %bb.5:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.1, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	jmp	.LBB1_7
.LBB1_6:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.2, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
.LBB1_7:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.3, %rsi
	movq	%rax, -72(%rbp)         # 8-byte Spill
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-16(%rbp), %rsi
	movq	%rax, -80(%rbp)         # 8-byte Spill
	callq	_ZNSirsERi
	movq	%rax, %rdi
	leaq	-20(%rbp), %rsi
	callq	_ZNSirsERi
	movq	%rax, %rdi
	leaq	-24(%rbp), %rsi
	callq	_ZNSirsERi
	movl	-16(%rbp), %ecx
	cmpl	-20(%rbp), %ecx
	jne	.LBB1_11
# %bb.8:
	xorl	%eax, %eax
                                        # kill: def $al killed $al killed $eax
	cmpl	$5, -24(%rbp)
	setne	%cl
	xorb	$-1, %cl
	andb	$1, %cl
	movzbl	%cl, %edx
	movl	-16(%rbp), %esi
	cmpl	-20(%rbp), %esi
	movl	%edx, -84(%rbp)         # 4-byte Spill
	movb	%al, -85(%rbp)          # 1-byte Spill
	jne	.LBB1_10
# %bb.9:
	cmpl	$5, -24(%rbp)
	setne	%al
	movb	%al, -85(%rbp)          # 1-byte Spill
.LBB1_10:
	movb	-85(%rbp), %al          # 1-byte Reload
	xorb	$-1, %al
	andb	$1, %al
	movzbl	%al, %ecx
	movl	-84(%rbp), %edx         # 4-byte Reload
	cmpl	%ecx, %edx
	jne	.LBB1_12
.LBB1_11:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.4, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	jmp	.LBB1_13
.LBB1_12:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.5, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
.LBB1_13:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	movq	%rax, -96(%rbp)         # 8-byte Spill
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-28(%rbp), %rsi
	movq	%rax, -104(%rbp)        # 8-byte Spill
	callq	_ZNSirsERi
	movq	%rax, %rdi
	leaq	-12(%rbp), %rsi
	callq	_ZNSirsERi
	xorl	%ecx, %ecx
                                        # kill: def $cl killed $cl killed $ecx
	cmpl	$8, -28(%rbp)
	movb	%cl, -105(%rbp)         # 1-byte Spill
	jg	.LBB1_15
# %bb.14:
	cmpl	$4, -32(%rbp)
	setg	%al
	movb	%al, -105(%rbp)         # 1-byte Spill
.LBB1_15:
	movb	-105(%rbp), %al         # 1-byte Reload
	xorb	$-1, %al
	andb	$1, %al
	movzbl	%al, %ecx
	cmpl	$8, -28(%rbp)
	movb	$1, %al
	movl	%ecx, -112(%rbp)        # 4-byte Spill
	movb	%al, -113(%rbp)         # 1-byte Spill
	jg	.LBB1_17
# %bb.16:
	cmpl	$4, -32(%rbp)
	setle	%al
	movb	%al, -113(%rbp)         # 1-byte Spill
.LBB1_17:
	movb	-113(%rbp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ecx
	movl	-112(%rbp), %edx        # 4-byte Reload
	cmpl	%ecx, %edx
	jne	.LBB1_19
# %bb.18:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.6, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	jmp	.LBB1_20
.LBB1_19:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.7, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
.LBB1_20:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str, %rsi
	movq	%rax, -128(%rbp)        # 8-byte Spill
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-36(%rbp), %rsi
	movq	%rax, -136(%rbp)        # 8-byte Spill
	callq	_ZNSirsERi
	movq	%rax, %rdi
	leaq	-40(%rbp), %rsi
	callq	_ZNSirsERi
	cmpl	$4, -36(%rbp)
	movb	$1, %cl
	movb	%cl, -137(%rbp)         # 1-byte Spill
	jg	.LBB1_22
# %bb.21:
	cmpl	$6, -40(%rbp)
	setle	%al
	movb	%al, -137(%rbp)         # 1-byte Spill
.LBB1_22:
	movb	-137(%rbp), %al         # 1-byte Reload
	xorl	%ecx, %ecx
                                        # kill: def $cl killed $cl killed $ecx
	xorb	$-1, %al
	andb	$1, %al
	movzbl	%al, %edx
	cmpl	$4, -36(%rbp)
	movl	%edx, -144(%rbp)        # 4-byte Spill
	movb	%cl, -145(%rbp)         # 1-byte Spill
	jg	.LBB1_24
# %bb.23:
	cmpl	$6, -40(%rbp)
	setg	%al
	movb	%al, -145(%rbp)         # 1-byte Spill
.LBB1_24:
	movb	-145(%rbp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ecx
	movl	-144(%rbp), %edx        # 4-byte Reload
	cmpl	%ecx, %edx
	jne	.LBB1_26
# %bb.25:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.8, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	jmp	.LBB1_27
.LBB1_26:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.9, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
.LBB1_27:
	xorl	%eax, %eax
	addq	$160, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_exercise_05_22.cpp
	.type	_GLOBAL__sub_I_exercise_05_22.cpp,@function
_GLOBAL__sub_I_exercise_05_22.cpp:      # @_GLOBAL__sub_I_exercise_05_22.cpp
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	__cxx_global_var_init
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end2:
	.size	_GLOBAL__sub_I_exercise_05_22.cpp, .Lfunc_end2-_GLOBAL__sub_I_exercise_05_22.cpp
	.cfi_endproc
                                        # -- End function
	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Input 2 numbers: "
	.size	.L.str, 18

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"!( x < 5 ) && !( y >= 7 ) and (!(x < 5 || y >= 7)) are equivalent"
	.size	.L.str.1, 66

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"!( x < 5 ) && !( y >= 7 ) and (!(x < 5 || y >= 7)) are not equivalent"
	.size	.L.str.2, 70

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Input 3 numbers: "
	.size	.L.str.3, 18

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"!( a == b ) || !( g != 5 ) and (!(a == b && g != 5)) are equivalent"
	.size	.L.str.4, 68

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"!( a == b ) || !( g != 5 ) and (!(a == b && g != 5)) are not equivalent"
	.size	.L.str.5, 72

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"!( q <= 8 ) && ( w > 4 ) ) are equivalent (q > 8 || w <= 4)"
	.size	.L.str.6, 60

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"!(( q <= 8 ) && ( w > 4 )) and are not equivalent (q > 8 || w <= 4)"
	.size	.L.str.7, 68

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"!(i > 4 || j <= 6) and (i <= 4 && j > 6) are equivalent"
	.size	.L.str.8, 56

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"(!(i > 4 || j <= 6) and (i <= 4 && j > 6)) are not equal"
	.size	.L.str.9, 57

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_exercise_05_22.cpp
	.ident	"clang version 10.0.0-4ubuntu1 "
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym __cxx_global_var_init
	.addrsig_sym __cxa_atexit
	.addrsig_sym _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	.addrsig_sym _ZNSirsERi
	.addrsig_sym _ZNSolsEPFRSoS_E
	.addrsig_sym _ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.addrsig_sym _GLOBAL__sub_I_exercise_05_22.cpp
	.addrsig_sym _ZStL8__ioinit
	.addrsig_sym __dso_handle
	.addrsig_sym _ZSt4cout
	.addrsig_sym _ZSt3cin
