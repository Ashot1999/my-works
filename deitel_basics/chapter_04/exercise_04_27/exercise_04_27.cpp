#include <iostream>

int
main()
{
    std::cout << "Enter binary number: ";
    int binaryNumber;
    std::cin >> binaryNumber;

    int decimal = 0;
    int place = 1;

    for (int i = binaryNumber; i >= 1; i /= 10) {
        int currentLast = i % 10;
        if (currentLast != 0) {
            if (currentLast != 1) {
                std::cerr << "Error 1: number is not binary" << std::endl;
                return 1;
            }
        }
        decimal += currentLast * place;
        place *= 2;
    }
    std::cout << "Binary: " << binaryNumber << "\n" << "Decimal: " << decimal << "\n";
    
    return 0;
}
