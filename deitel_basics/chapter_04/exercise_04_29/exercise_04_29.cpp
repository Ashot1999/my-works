#include <iostream>

int
main()
{
    int number = 1;
    while (true) {
        std::cout << number * 2 << " ";
        number *= 2; 
    }
    return 0;
}

///Programm starts printing powers of 2 until -2^31, and then we see 0-es. As powers 0f 2 converting to binary number we can se number 1 then 0-es. After -2^31 number 1 goes out  from 4 bites memory and programm starts printing 0-es.
