#include <iostream>

int
main()
{
    while (true) {
        std::cout << "Enter sales in dollars (-1 to end): ";
        float sales;
        std::cin >> sales;
        if (-1 == static_cast<int>(sales)) {
            return 0;
        }
        if (sales < 0) {
            std::cerr << "Error 1: sales cannot be negative" << std::endl;
            return 0;
        }
        float salary = 200 + (sales * 9 / 100);
        std::cout << "Salary: " << salary << std::endl;
    }
    return 0;
}
