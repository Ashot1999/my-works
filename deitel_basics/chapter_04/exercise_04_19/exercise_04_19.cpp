#include <iostream>
#include <climits>

int
main()
{
    int counter = 0;
    int largest1 = INT_MIN;
    int largest2 = INT_MIN;

    std::cout << "Enter 10 numbers: " << std::endl;
    while (counter < 10) {
        int number;
        std::cin >> number;
        if (number > largest1) {
            largest2 = largest1;
            largest1 = number;
        } else if (number > largest2) {
            largest2 = number; 
        }       
        ++counter;
    }
    std::cout << "The largest: " << largest1 << std::endl;
    std::cout << "The second largest: " << largest2 << std::endl;
    return 0;
}

