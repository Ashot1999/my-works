#include <iostream>

int
main()
{
    std::cout << "Enter 3 positive numbers: ";
    int number1, number2, number3;
    std::cin >> number1 >> number2 >> number3;

    if (number1 <= 0 || number2 <= 0 || number3 <= 0) {
        std::cerr << "Error 1: all numbers must be positive" << std::endl;
        return 1;
    }
    
    int number1Square = number1 * number1;
    int number2Square = number2 * number2;
    int number3Square = number3 * number3;

    if (number1Square + number2Square == number3Square) {
        std::cout << "These 3 numbers can be sides of right triangle" << std::endl;
        return 0;
    }
    if (number1Square + number3Square == number2Square) {
        std::cout << "These 3 numbers can be sides of right triangle" << std::endl;
        return 0;
    }
    if (number3Square + number2Square == number1Square) {
        std::cout << "These 3 numbers can be sides of right triangle" << std::endl;
        return 0;
    }
    
    std::cout << "These numbers cannot make up the sides of a right triangle" << std::endl;
    
    return 0;
}
