#include <iostream>

int
main()
{
    float totalMiles = 0;
    float totalGallon = 0;

    while (true) {
        std::cout << "Enter the miles used (-1 to quit): ";
        float miles;
        std::cin >> miles;
        if (static_cast<int>(miles) == -1) {
            std::cout << "Exiting..." << std::endl;
            return 0;
        }

        if (miles < 0) {
            std::cout << "Error 1: The number of miles cannot be negative." << std::endl;
            return 1;
        }

        std::cout << "Enter gallons: ";
        float gallon;
        std::cin >> gallon;
        
        if (gallon < 0) {
            std::cout << "Error 2: The number of gallon cannot be negative." << std::endl;
            return 2;
        }
        
        totalMiles += miles;
        totalGallon += gallon;
        std::cout << "MPG this tankful: " << miles / gallon << std::endl;
        std::cout << "Total MPG: " << totalMiles / totalGallon << std::endl;
    }
    return 0;
}
