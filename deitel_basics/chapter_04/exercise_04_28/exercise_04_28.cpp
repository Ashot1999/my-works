#include <iostream>

int
main()
{
    for (int i = 1; i <= 8; ++i) {
        if (i % 2 == 0) {
            std::cout << " ";
        }
        for (int j = 1; j <= 8; ++j) {
                std::cout << "* ";
        }     
        std::cout << std::endl;
    }
    return 0;
}
