#include <iostream>

int
main()
{
    std::cout << "Enter number between 10000 - 99999: ";
    int number;
    std::cin >> number;
    
    if (number < 10000) {
        std::cerr << "Error 1: number must be between 10000 - 99999" << std::endl;
        return 1;
    }
    if (number > 99999) {
        std::cerr << "Error 1: number must be between 10000 - 99999" << std::endl;
        return 1;
    }

    if (number / 10000 == number % 10) {
        if (number / 1000 % 10 == number / 10 % 10) {
            std::cout << "Number is palindrome!" << std::endl;
            return 0;
        }
    }
    std::cout << "Number is not palindrome!" << std::endl;
    return 0;
}
