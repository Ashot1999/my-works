#include <iostream>

int
main()
{
    std::cout << "Enter not negative number: ";
    int number;
    std::cin >> number;

    if (number < 0) {
        std::cerr << "Error 1: number is negative" << std::endl;
        return 1;
    }

    int i = number;
    int numberFactorial = 1;
    while (i >= 1) {
        numberFactorial *= i;
        --i;
    }
    std::cout << number << "! = " << numberFactorial << std::endl;
    return 0;
}
