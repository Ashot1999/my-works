#include <iostream>

int
main()
{
    std::cout << "Input accuracy: ";
    int accuracy;
    std::cin >> accuracy;
    
    if (accuracy <= 0) {
        std::cerr << "Error 1: accuracy must be positive" << std::endl;
        return 1;
    }   
    
    std::cout << "Input exponent: ";
    double x;
    std::cin >> x;

    int number = 1;
    int numberFactorial = 1;
    double e = 1.0;
    double exponent = 1.0;
    while (number < accuracy) {
        exponent *= x;
        numberFactorial *= number;
        e += exponent / numberFactorial;
        ++number;
    }
    std::cout << "e ^ " << x << " is " << e << std::endl;
    return 0;
}
