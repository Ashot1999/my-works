#include <iostream>

int
main()
{
    int accuracy;
    std::cout << "Input the accuracy: ";
    std::cin >> accuracy;
    if (accuracy <= 0) {
        std::cerr << "Error 1: accuracy must be positive" << std::endl;
        return 1;
    }   
    int number = 1;
    int numberFactorial = 1;
    double e = 1.0;

    while (number < accuracy) {
        numberFactorial *= number;
        e += 1.0 / numberFactorial;
        ++number;
    }

    std::cout << "e = " << e << std::endl;
    return 0;
}
