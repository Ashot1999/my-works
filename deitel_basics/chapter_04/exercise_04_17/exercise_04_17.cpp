#include <iostream>
#include <climits>

int
main()
{
    int counter = 0;
    int largest = INT_MIN;

    std::cout << "Enter 10 numbers: " << std::endl;
    while (counter < 10) {
        int number;
        std::cin >> number;
        ++counter;
        if (number > largest) {
            largest = number;
        }
    }

    std::cout << "The largest: " << largest << std::endl;
    return 0;

}
