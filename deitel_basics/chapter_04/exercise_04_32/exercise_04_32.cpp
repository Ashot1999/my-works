#include <iostream>

int
main()
{
    std::cout << "Enter 3 positive numbers: ";
    int         number1,   number2,   number3;
    std::cin >> number1 >> number2 >> number3;

    if (number1 <= 0) {
        std::cerr << "Error 1: all numbers must be positive" << std::endl;
        return 1;
    }
    if (number2 <= 0) {
        std::cerr << "Error 1: all numbers must be positive" << std::endl;
        return 1;
    }
    if (number3 <= 0) {
        std::cerr << "Error 1: all numbers must be positive" << std::endl;
        return 1;
    }

    if (number1 + number2 > number3) {
        if (number1 + number3 > number2) {
            if (number2 + number3 > number1) {
                std::cout << "This 3 numbers can be sides of a triangle" << std::endl;
                return 0;
            }
        }
    }
    std::cout << "This 3 numbers cannot be sides of a triangle" << std::endl;
    return 0;
}
