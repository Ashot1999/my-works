#include <iostream>                                            

class Account                                                 
{
public:
    Account(int accountNumber, float balance, float incomeToBalance, float outcomeFromBalance, float creditLimit);                 
    void setAccountNumber(int accountNumber);
    void setBalance(float balance);
    void setOutcomeFromBalance(float outcomeFromBalance);
    void setIncomeToBalance(float incomeToBalance);
    void setCreditLimit(float creditLimit);
    int getAccountNumber();
    float getBalance();
    float getOutcomeFromBalance();
    float getIncomeToBalance();
    float getCreditLimit();
private:
    int accountNumber_;
    float balance_;
    float incomeToBalance_;
    float outcomeFromBalance_;
    float creditLimit_;
};
