#include <iostream>
#include "Account.hpp"

int
main()
{
    while (true) {
        int accountNumber;
        std::cout << "Enter account number (-1 to end): ";
        std::cin >> accountNumber;
 
        if (accountNumber == -1) {
            return 0;
        }

        float balance;
        std::cout << "Enter beginning balance: ";
        std::cin >> balance;
        float outcomeFromBalance;
        std::cout << "Enter total charges: ";
        std::cin >> outcomeFromBalance;
        float incomeToBalance;
        std::cout << "Enter total credits: ";
        std::cin >> incomeToBalance;
        float creditLimit;
        std::cout << "Enter credit limit: ";
        std::cin >> creditLimit;
 
        Account user1(accountNumber, balance + incomeToBalance - outcomeFromBalance, incomeToBalance, outcomeFromBalance, creditLimit);
 
        std::cout     << "New balance is "
                      << user1.getBalance()
                      << std::endl;

        if (user1.getBalance() > user1.getCreditLimit()) {
            std::cout << "Account: "
                      << user1.getAccountNumber()
                      << std::endl;
            std::cout << "Credit limit: "                 
                      << user1.getCreditLimit()
                      << std::endl;
            std::cout << "Balance: "
                      << user1.getBalance()
                      << std::endl;
            std::cout << "Credit Limit Exceeded."
                      << std::endl;
        }
    }

    return 0;
}


