#include <iostream>
#include "Account.hpp"
 
Account::Account(int accountNumber, float balance, float incomeToBalance, float outcomeFromBalance, float creditLimit)
{
    setAccountNumber(accountNumber);
    setBalance(balance);
    setOutcomeFromBalance(outcomeFromBalance);
    setIncomeToBalance(incomeToBalance);
    setCreditLimit(creditLimit);
}
  
void
Account::setAccountNumber(int accountNumber)
{
    accountNumber_ = accountNumber;
}
  
void
Account::setBalance(float balance)
{
    balance_ = balance;
}
  
void
Account::setOutcomeFromBalance(float outcomeFromBalance)
{
    outcomeFromBalance_ = outcomeFromBalance;
}
 
void
Account::setIncomeToBalance(float incomeToBalance)
{
   incomeToBalance_ = incomeToBalance;
}
 
void
Account::setCreditLimit(float creditLimit)
{
    creditLimit_ = creditLimit;
}
 
int
Account::getAccountNumber()
{
    return accountNumber_;
}
 
float
Account::getBalance()
{
    return balance_;
}

float
Account::getOutcomeFromBalance()
{
    return outcomeFromBalance_;
}

float
Account::getIncomeToBalance()
{
    return incomeToBalance_;
}

float
Account::getCreditLimit()
{
    return creditLimit_;
}

