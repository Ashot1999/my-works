#include <iostream>

int
main()
{
    double pi = 3.14;
    std::cout << "Input radius: ";
    double radius;
    std::cin >> radius;

    if (radius <= 0) {
        std::cout << "Error 1: Radius must be over 0" << std::endl;
        return 1;
    }

    std::cout << "The diameter: " << 2 * radius << std::endl;
    std::cout << "The circumference: " << pi * 2 * radius << std::endl;
    std::cout << "The area: " << pi * radius * radius << std::endl;

    return 0;
}

