#include <iostream>

int
main()
{
    while (true) {
        float hours;  
        std::cout << "Enter hours worked (-1 to end): " << std::endl;
        std::cin >> hours;

        if (static_cast<int>(hours) == -1) {
            std::cout << "Exiting..." << std::endl;
            return 0;
        }

        if (hours < 0) {
            std::cout << "Error 1: Worked hours cannot be negative." << std::endl;
            return 1;
        }

        float salaryPerHour;
        std::cout << "Enter hourly rate of the worker ($00.00): " << std::endl;
        std::cin >> salaryPerHour;

        if (salaryPerHour < 0) {
            std::cout << "Error 2: Hourly rate cannot be negative." << std::endl;
            return 2;
        }
        
        float salary = hours * salaryPerHour;
        if (hours > 40) {
            float extraHours = hours - 40;
            salary += extraHours * salaryPerHour * 0.5;
        }
       
        std::cout << "Salary is " << salary << "$" << std::endl;      
    }

    return 0;
}

