#include <iostream>

int
main()
{
    std::cout << "Enter four digit number: ";
    int number;
    std::cin >> number;

    int digit1 = (number / 1000 + 3) % 10;
    int digit2 = (number % 1000 / 100 + 3) % 10;
    int digit3 = (number % 100 / 10 + 3) % 10;
    int digit4 = (number % 10 + 3) % 10;

    std::cout << "Decrypting result: " << digit3 << digit4 << digit1 << digit2 << std::endl;

    return 0;
}
