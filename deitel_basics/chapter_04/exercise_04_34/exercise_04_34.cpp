#include <iostream>

int
main()
{
    std::cout << "Enter four digit number: ";
    int number;
    std::cin >> number;

    if (number < 0) {
        std::cerr << "Error 1: number is negative" << std::endl;
        return 1;
    }

    if (number < 1000) {
        std::cerr << "Error 2: number must be between 1000 - 9999" << std::endl;
        return 2;
    }

    if (number > 9999) {
        std::cerr << "Error 3: number must be between 1000 - 9999" << std::endl;
        return 3;
    }

    int digit1 = (number / 1000 + 7) % 10;
    int digit2 = (number  % 1000 / 100 + 7) % 10;
    int digit3 = (number % 100 / 10 + 7) % 10;
    int digit4 = (number % 10 + 7) % 10;

    std::cout << "Encrypting result: " << digit1 << digit2 << digit3 << digit4 << std::endl;

    return 0;
}
