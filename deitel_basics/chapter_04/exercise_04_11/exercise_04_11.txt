a) if ( age >= 65 );
      cout << "Age is greater than or equal to 65" << endl;
   else
      cout << "Age is less than 65 << endl";


      Right version.

   if ( age >= 65 ) 
      cout << "Age is greater than or equal to 65" << endl;
   else
      cout << "Age is less than 65" << endl;

b) if ( age >= 65 )
      cout « "Age is greater than or equal to 65" « endl;
   else
      cout « "Age is less than 65 « endl";

      Right version.

   if ( age >= 65 )
      cout « "Age is greater than or equal to 65" « endl;
   else
      cout « "Age is less than 65" « endl;

c) int x = 1, total;
   while ( x <= 10 )
   {
      total += x;
      x++;
   }

      Right version.

    int x = 1;
    int total = 0;
    while (x <= 10)
    {
        total += x;
        x++;
    }

d) While ( x <= 100 )
   total += x;
   x++;

     Right version.
   
   int total = 0;
   int x = 0; 
   while ( x <= 100 ) {
   total += x;
   x++;
   }

e) while ( y > 0 )
   {
       cout << y << endl;
       y++;
   }

      Right version.

  int y = 20;
  while (y > 0)
  {
    cout << y << std::endl;
    y--;
  }

