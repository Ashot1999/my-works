#include <iostream>

int
main()
{
    std::cout << "Enter side: ";
    int side;
    std::cin >> side;
    for (int row = 1; row <= side; ++row) {
        for (int column = 1; column <= side; ++column) {
            if (1 == row) {
                std::cout << "*";
            } else if (row == side) {
                std::cout << "*";
            } else if (1 == column) {
                std::cout << "*";
            } else if (column == side) {
                std::cout << "*";
            } else {
                std::cout << " ";           
            }
        }
        std::cout << std::endl;
    }
    return 0;
}
