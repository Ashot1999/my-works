#include <iostream>

int
main()
{
    int radius;
    std::cout << "Input radius: ";
    std::cin >> radius;
    if (radius <= 0) {
        std::cout << "Error 1: This number is invalid" << std::endl;
        return 1;
    }
    std::cout << "The diameter is: " << 2 * radius << std::endl;
    std::cout << "The circumference is: " << 3.14159 * 2 * radius << std::endl;
    std::cout << "The area of a circleis: " << 3.14159 * radius * radius << std::endl;
    return 0;
}
