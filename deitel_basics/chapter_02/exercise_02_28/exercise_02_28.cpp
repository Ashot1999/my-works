#include <iostream>

int
main()
{
    std::cout << "Enter a five-digit number: ";
    int number;
    std::cin >> number;

    if (number < 10000) {
        std::cout << "Error 2: invalid number." << std::endl;
        return 2;
    }

    if (number > 99999) {
        std::cout << "Error 3: invalid number." << std::endl;
        return 3;
    }
    std::cout << number / 10000 % 10 << "   " << number / 1000 % 10  << "   " << number / 100 % 10  << "   " << number / 10 % 10  << "   " << number  % 10 << std::endl;   
    return 0;
}
