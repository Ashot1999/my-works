#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Enter 2 numbers:" << std::endl;
    std::cin >> number1 >> number2;
    if (0 == number2) {
        std::cout << "Error 1: The second number cannot be 0." << std::endl;
        return 1;
    }
    if (number1 % number2 == 0) {
	std::cout << "The first number is multiple of the second" << std::endl;
	return 0;
    }
    std::cout << "The first number is not multiple of the second" << std::endl;
    return 0;
}
