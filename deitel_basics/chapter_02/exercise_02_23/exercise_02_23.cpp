#include <iostream>

int
main()
{
    int number1, number2, number3, number4, number5;
    std::cout << "Enter 5 numbers: " << std::endl;
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5;
    int largest = number1;
    int smallest = number1;
    if (number2 > largest) {
        largest = number2;
    }
    if (number3 > largest) {
        largest = number3;
    }
    if (number4 > largest) {
        largest = number4;
    }
    if (number5 > largest) {
        largest = number5;
    }
    if (number2 < smallest) {
        smallest = number2;
    }
    if (number3 < smallest) {
        smallest = number3;
    }
    if (number4 < smallest) {
        smallest = number4;
    }
    if (number5 < smallest) {
        smallest = number5;
    }
    std::cout << "Largest is: " << largest << std::endl  <<  "Smallest is: " << smallest << std::endl;
    return 0;
}
