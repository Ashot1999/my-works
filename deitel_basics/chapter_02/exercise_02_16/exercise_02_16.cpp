#include <iostream>

int
main()
{
    int number1;	
    int number2;
    std::cout << "Enter the first number: ";
    std::cin >> number1;
    std::cout << "Enter the second number: ";
    std::cin >> number2;
    std::cout << "Sum: " << number1 + number2 << std::endl;
    std::cout << "Product: " << number1 * number2 << std::endl;
    std::cout << "Difference: " << number1 - number2 << std::endl;
    if (0 == number2) {
        std::cout << "Error 1: The second number cannot be 0." << std::endl;
        return 1;
    } 
    std::cout << "Quotient: " << number1 / number2 << std::endl;
    return 0; 
}    
