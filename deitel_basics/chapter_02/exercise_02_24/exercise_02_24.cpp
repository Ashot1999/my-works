#include <iostream>

int
main()
{
    int number1;
    std::cout << "Enter a number: " << std::endl;
    std::cin >> number1;
    if (number1 % 2 == 0) {
        std::cout << "The number is even." << std::endl;
        return 0;
    }
    std::cout << "The number is odd" << std::endl;
    return 0;
}
