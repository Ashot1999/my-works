#include <iostream>

int 
main()
{
    int number1, number2, number3;
    std:: cout << "Input 3 different numbers " << std::endl;
    std::cin >> number1 >> number2 >> number3;
    std::cout << "The sum is: " << number1 + number2 + number3 << std::endl;
    std::cout << "The average is: " << (number1 + number2 + number3) / 3 << std::endl;
    std::cout << "The product is: " << number1 * number2 * number3 << std::endl;
    int smallest = number1;
    int largest = number1;
    if (number2 > largest) {
        largest = number2;
    }
    if (number3 > largest) {
        largest = number3;
    }
    if (number2 < smallest) {
        smallest = number2;
    }
    if (number3 < smallest) {
        smallest = number3;
    }
    std::cout << "The largest number is: " << largest << std::endl << "The smallest number is: " << smallest << std::endl;  
    return 0;
}

