#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Enter the first number: ";
    std::cin >> number1;
    std::cout << "Enter the second number: ";
    std::cin >> number2;
    if (number1 == number2) {
        std::cout << "These numbers are equal" << std::endl;
        return 0;
    }
    if (number1 > number2) {
        std::cout << " The first number is larger." << std::endl;
        return 0;
    }
    std::cout << "The second number is larger." << std::endl;
    return 0;
}
