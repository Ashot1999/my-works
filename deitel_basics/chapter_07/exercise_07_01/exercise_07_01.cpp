#include <iostream>
#include <iomanip>

int
main()
{
    const int arraySize = 7;
    int frequency [arraySize] = {0};
    ::srand(::time(0));

    std::cout << "Face" << std::setw(13) << "Frequency" << std::endl;
    for (int roll = 1; roll <= 10; ++roll) {
        ++frequency[1 + rand() % 6];
    }

    for (int face = 1; face < arraySize; ++face) {
        std::cout << std::setw(4) << face << std::setw(13) << frequency[face] << std::endl;
    }

    return 0;
}
