#include <iostream>

void
printMenu()
{
    std::cout << "Please type 1 for Business class and 2 for Economy class: " << std::endl;
}

int
chooseClass()
{
    printMenu();
    int choice;
    std::cin >> choice;
    if (choice != 1 && choice != 2) {
        std::cerr << "Error 1: choose 1 or 2" << std::endl;
        return 1;
    }
    return choice;
}

void
emptySeats(const int array[], const int size)
{
    for (int i = 0; i < size; ++i) {
        if (array[i] != 0) {
            std::cout << "Seat " << array[i] << " is available!" << std::endl;
        }
    }
}

void
selectYourBusinessClassSeat(const int array[], const int size)
{
    businessClassSeats(array, size);
    emptySeats(array, size);
}

void
selectYourEconomyClassSeat(const int array[], const int size)
{
    economyClassSeats(array, size);
    emptySeats(array, size);
}


void
reserveSeat(const int array[], const int size)
{
    std::cout << "Reserve your seat from empty seats" << std::endl;
    std::cout << "Do you want to see empty seats? (y or n) ";
    char selection;
    std::cin >> selection;
    if (selection == y) {
        emptySeats(array, size);
    } else if (selection == n) {
        continue;
    } else {
        std::cerr << "Invalid character!" << std::endl;
    }
    const int yourChoice = chooseClass();
    if (yourChoice == 2) {
        selectYourEconomyClassSeat(array, size);
        std::cout << "Your seat is " << yourSeat << std::endl;
    }
    if (yourChoice == 1) {
        selectYourBusinessClassSeat(array, size);
        std::cout << "Your seat is " << yourSeat << std::endl;
    }
}

void
businessClassSeats(int array[], const int size)
{
    for (int i = 0; i < SEATS; ++i) {
        array[i] = i + 1;   
    }
}

void
economyClassSeats(int array[], const int size)
{
    for (int i = 0; i < SEATS; ++i) {
        array[i] = i + 6;
    }
}



int
main()
{
    const int SEATS = 5;
    const int businessClass[SEATS] = {}
    businessClassSeats(businessClass, SEATS);
    const int economyClass[SIZE] = {};
    economyClassSeats(economyClass, SEATS);

}
