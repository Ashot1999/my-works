#include <iostream>

void someFunction(int [], int, int);

int
main()
{
    const int arraySize = 10;
    int a[ arraySize ] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    cout << "The values in the array are:" << endl;
    someFunction( a, 0, arraySize );
    std::cout << std::endl;
    return 0;
}
void
someFunction(int b[], int current, int size)
{
    if ( current < size ) {
        someFunction( b, current + 1, size );
        cout << b[ current ] << " ";
    } 
}

Answer: By using recusrsion this funtion prints members of an array in opposite order.
