a. To refer to a particular location or element within an array, we specify the name of the array and the value of the particular element.
   False. We specify the name of array and index of particular member.

b. An array declaration reserves space for the array.
   True.

c. To indicate that 100 locations should be reserved for integer array p , the programmer writes the declaration p[ 100 ];
   False. We must specialize what type of array it is. for example: double p[100];

d. A for statement must be used to initialize the elements of a 15-element array to zero.
   False. Not only for statement can be uzes to initialize the elements to zero. Another way: int p[15] = {0};

e. Nested for statements must be used to total the elements of a two-dimensional array.
   True. 
