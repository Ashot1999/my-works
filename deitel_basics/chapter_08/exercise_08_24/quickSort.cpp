#include <iostream>

void
printArray(int array[], int size)
{
    for (int i = 0; i < size; i++) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

int
partition (int array[], int start, int end)
{
    int pivotIndex = start;
    while (start < end) {
        while (array[pivotIndex] <= array[end]) {
            --end;
        }
        std::swap(array[pivotIndex], array[end]);
        pivotIndex = end;

        while (array[pivotIndex] >= array[start]) {
            ++start;
        }
        std::swap(array[pivotIndex], array[start]);
        pivotIndex = start;
    }
    return pivotIndex;
}

void
quickSort(int array[], int start, int end)
{
    if (start < end) {
        int pi = partition(array, start, end);
        quickSort(array, start, pi - 1);
        quickSort(array, pi + 1, end);
    }
}
 

int
main()
{
    int array[] = {6, 15, 2, 37, 50, 22, 1, 10, 4, 89, 118, 200, 300};
    int n = sizeof(array) / sizeof(array[0]);
    quickSort(array, 0, n - 1);
    std::cout << "Sorted array: \n";
    printArray(array, n);
    return 0;
}
