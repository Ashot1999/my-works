#include "DeckOfCards.hpp"
#include <iostream>

int
main()
{
    DeckOfCards deckOfCards;
    deckOfCards.shuffle();
    deckOfCards.deal();
    return 0;
}
