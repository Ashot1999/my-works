enum Suit { HEARTS, DIAMONDS, CLUBS, SPADES, SUIT_COUNT };
enum Face { ACE, DEUCE, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, FACE_COUNT };

///static const char* SUIT[] = {"Hearts", "Diamonds", "Clubs", "Spades"};
///static const char* FACE[] = {"Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};

struct Card
{
    Card(const Suit s = SUIT_COUNT, const Face f = FACE_COUNT) {
        suit = s;
        face = f;
    }
    Card(const int row, const int column) {
        suit = static_cast<Suit>(row);
        face = static_cast<Face>(column);
    }
    Suit suit;
    Face face;
};

class Hand
{
public:
    static const int COUNT = 5;

public:
    void deal(const int index, const Card& card);
    bool checkPair();
    bool checkTwoPairs();
    bool checkThreeOfAKind();
    bool checkFourOfAKind();
    bool checkFlush();
    bool checkStraight();

private:
    Card cards_[COUNT];

};

class DeckOfCards 
{
    static const int COUNT = SUIT_COUNT * FACE_COUNT;
public:
    DeckOfCards();
    void shuffle();
    void deal();

private:
    Card deck_[COUNT];
    Hand hand_;

};

