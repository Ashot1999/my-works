#include "DeckOfCards.hpp"
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>

void
Hand::deal(const int index, const Card& card)
{
    cards_[index] = card;
}

DeckOfCards::DeckOfCards()
{
    ::srand(::time(0));
}

void
DeckOfCards::shuffle()
{
    int counter = 0;
    for (int index = 0; index < COUNT; ++index) {
        do {
            counter = ::rand() % COUNT;
        } while (deck_[counter].suit != SUIT_COUNT && deck_[counter].face != FACE_COUNT);
        deck_[counter] = Card(index / SUIT_COUNT, index % FACE_COUNT);
    }
}

void
DeckOfCards::deal()
{
    int counter = 0;
    for (int index = 0; index < Hand::COUNT; ++index) {
        hand_.deal(index, deck_[counter++]);
    }
}

bool
checkPair(Hand::COUNT, )
{

}












