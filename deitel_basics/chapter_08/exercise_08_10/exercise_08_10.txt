long int value1, value2;
value1 = 200000;

a)
long int* iPtr;

b)
iPtr = &value1;

c)
std::cout << *iPtr;

d)
value2 = *iPtr;

e)
std::cout << value2;
    or
std::cout << *iPtr;

f)
std::cout << &value1;
std::cout << iPtr;

g)
std::cout << iPtr;
std::cout << &value1;
In both cases outputs are the same.
