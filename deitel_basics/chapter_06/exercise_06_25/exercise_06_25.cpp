#include <iostream>
#include <cassert>

int
integerPart(const int number1, const int number2)
{
    assert(number2 != 0);
    const int integerPart = number1 / number2;
    return integerPart;
}

int
remainder(const int number1, const int number2)
{
    assert(number2 != 0);
    const int remainder = number1 % number2;
    return remainder;
}

void
seperatingIntoParts(int number)
{
    assert(number >= 1 && number <= 32767);
    int numberPart = 10000;
    while (numberPart > number) {
        numberPart = integerPart(numberPart, 10);
    }
    
    while (numberPart != 0) {
        const double separateDigit = remainder(integerPart(number, numberPart), 10);
        if (separateDigit < 10 && separateDigit >= 0) {
            const int result = integerPart(number, numberPart);
            std::cout << remainder(result, 10) << " ";
        }
        numberPart = integerPart(numberPart, 10);
    }
}

int
main()
{
    std::cout << "Input number (1 - 32767): ";
    int number;
    std::cin >> number;
    if (number > 32767 || number < 1) {
        std::cerr << "Error 1: Entered number is out of range" << std::endl;
        return 1;
    }

    std::cout << "Start number: " << number << std::endl;
    std::cout << "Divided into parts: ";
    seperatingIntoParts(number);
    std::cout << std::endl;
    return 0;
}

