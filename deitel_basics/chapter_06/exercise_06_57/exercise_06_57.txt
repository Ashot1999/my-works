C++ provides the unary scope resolution operator (::) to access a global variable when a local variable of the same name is in scope. The unary scope resolution operator cannot be used to access a
local variable of the same name in an outer block . A global variable can be accessed directly without the unary scope resolution operator if the name of the global variable is not the same as that 
of a local variable in scope.
