#include <iostream>

int
gcd(int number1, int number2)
{
    while (number1 != 0 && number2 != 0) {
        if (number1 > number2) {
            number1 %= number2;
        } else {
            number2 %= number1;
        }
    }
    const int gcd = number1 + number2;
    return gcd;

}

int
main()
{
    std::cout << "Enter two numbers: ";
    int number1, number2;
    std::cin >> number1 >> number2;

    std::cout << "GCD = " << gcd(number1, number2);
    
    return 0;
}
