#include <iostream>
#include <iomanip>

double
celsius(const int t)
{
    const double fahrenheit = (static_cast<double>(t) - 32) * 5 / 9;
    return fahrenheit;
}

double
fahrenheit(const int t)
{
    const double celsius = static_cast<double>(t) * 9 / 5 + 32;
    return celsius;
}

int
main()
{
    for (int t = 0; t <= 100; ++t) {
        std::cout << t << " celsius = " << fahrenheit(t) << " fahrenheit" << std::endl;
    }

    std::cout << std::endl;

    for (int t = 32; t <= 212; ++t) {
        std::cout << t << " fahrenheit = " << celsius(t) << " celsius" << std::endl;
    }
    std::cout << std::endl;
    return 0;

}

