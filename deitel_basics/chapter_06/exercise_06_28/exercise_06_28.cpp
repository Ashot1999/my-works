#include <iostream>

double
minimum(const double number1, const double number2, const double number3)
{
    double minimum = number1;
    if (minimum > number2) {
        minimum = number2;
    }
    if (minimum > number3) {
        minimum = number3;
    }
    return minimum;
}

int
main()
{
    std::cout << "Input 3 numbers: ";
    double number1, number2, number3;
    std::cin >> number1 >> number2 >> number3;

    std::cout << "The smallest number: " << minimum(number1, number2, number3);
    std::cout << std::endl;
}
