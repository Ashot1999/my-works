#include <iostream>
#include <cassert>

void
printSquare(const int side)
{
    assert(side > 0);
    for (int counter1 = 0; counter1 < side; ++counter1) {
        for (int counter2 = 0; counter2 < side; ++counter2) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }

}

int
main()
{
    std::cout << "Enter side of square: ";
    int side;
    std::cin >> side;
    if (side <= 0) {
        std::cerr << "Error 1: side can not be negative number or 0" << std::endl;
        return 1;
    }
    
    printSquare(side);
    return 0;
}
