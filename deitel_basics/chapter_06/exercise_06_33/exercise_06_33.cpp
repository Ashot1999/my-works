#include <iostream>
#include <cassert>
#include <cmath>

int
qualityPoints(const int avarage)
{
    assert(avarage >= 0 && avarage <= 100);
    if (100 == avarage) {
        return 4;
    }
    if (60 > avarage) {
        return 0;
    }
    const int grade = avarage / 10 - 5;
    return grade;
}

int
main()
{
    std::cout << "Enter avarage (0 - 99): ";
    int avarage;
    std::cin >> avarage;

    std::cout << "Your grade is " << qualityPoints(avarage) << std::endl;
    return 0;
}
