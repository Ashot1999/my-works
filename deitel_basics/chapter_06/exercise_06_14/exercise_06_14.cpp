#include <iostream>
#include <cmath>

double
roundToInteger(const double number)
{
    const double resultForRoundToInteger = ::floor(number + .5);
    return resultForRoundToInteger;
}

double
roundToTens(const double number)
{
    const double resultForRoundToTenths = ::floor(number * 10 + .5) / 10;
    return resultForRoundToTenths;
}

double
roundToHundreds(const double number)
{
    const double resultForRoundToHundreds = ::floor(number * 100 + .5) / 100;
    return resultForRoundToHundreds;
}

double
roundToThousands(const double number)
{
    const double resultForRoundToThousands = ::floor(number * 1000 + .5) / 1000;
    return resultForRoundToThousands;
}

int
main()
{
    std::cout << "Enter number: ";
    double number;
    std::cin >> number;
    std::cout << "Number: " << number << std::endl;
    std::cout << "Rounded to integer: " << roundToInteger(number) << std::endl;
    std::cout << "Rounded to tenths: " << roundToTens(number) << std::endl;
    std::cout << "Rounded to hundreds: " << roundToHundreds(number) << std::endl;
    std::cout << "Rounded to thousands: " << roundToThousands(number) << std::endl;

    return 0;
}
