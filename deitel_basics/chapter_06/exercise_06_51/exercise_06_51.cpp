#include <iostream>

int
multiple(int number1, int number2)
{
    if (1 == number1) {
        return number2;
    }
    if (1 == number2) {
        return number1;
    }
    if (0 > number2) {
        number2 *= -1;
        number1 *= -1;
    }
    return number1 + multiple(number1, number2 - 1);
}

int
main()
{
    std::cout << "Enter 2 numbers: ";
    int number1, number2;
    std::cin >> number1 >> number2;
    std::cout << number1 << " * " << number2 << " = " << multiple(number1, number2) << std::endl;
    return 0;
}
