#include <iostream>

int
flip()
{
    const int headsOrTails = ::rand() % 2;
    return headsOrTails;
}

int
main()
{
    ::srand(::time(0));
    int heads = 0;
    int tails = 0;
    for (int i = 1; i <= 100; ++i) {
        const int headsOrTails = flip();
        if (0 == headsOrTails) {
            std::cout << "tails" << std::endl;
            ++tails;
        } else {
            std::cout << "heads" << std::endl;
            ++heads;
        }
    }

    std::cout << "Total heads: " << heads << std::endl;
    std::cout << "Total tails: " << tails << std::endl;
    return 0;
}
