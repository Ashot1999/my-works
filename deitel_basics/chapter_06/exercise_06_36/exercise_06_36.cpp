#include <iostream>

void
responseToRightAnswer()
{
    const int answerVersion = 1 + ::rand() % 4;
    switch (answerVersion) {
    case 1:
        std::cout << "Very good!" << std::endl;
        break;
    case 2:
        std::cout << "Excellent!" << std::endl;
        break;
    case 3:
        std::cout << "Nice work!" << std::endl;
        break;
    case 4: 
        std::cout << "Keep up the good work!!" << std::endl;
        break;
    }
}

void
responseToWrongAnswer()
{
    const int version = 1 + ::rand() % 4;
    switch (version) {
    case 1:
        std::cout << "No. Please try again." << std::endl;
        break;
    case 2:
        std::cout << "Wrong. Try once more." << std::endl;
        break;
    case 3:
        std::cout << "Don't give up!" << std::endl;
        break;
    case 4:
        std::cout << "No.Keep trying." << std::endl;
        break;
    }
}


void
askQuestion()
{
    for (int i = 1; i <= 10; ++i) {
        const int number1 = ::rand() % 10;
        const int number2 = ::rand() % 10;
        while (true) {
            int answer;
            std::cout << number1 << " * " << number2 << " = ";
            std::cin >> answer;
            if (number1 * number2 == answer) {
                responseToRightAnswer();
                break;
            }
            responseToWrongAnswer();
        }
    }
}



int
main()
{
    ::srand(::time(0));
    askQuestion();
    return 0;
}
