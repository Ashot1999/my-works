#include <iostream>
#include <cmath>

bool
isPrime(const int number)
{
    const int limit = std::sqrt(number);
    for (int counter = 2; counter <= limit; ++counter) {
        if (number % counter == 0) {
            return false;
        }
    }
    return true;
}

int
main()
{
    for (int number = 2; number <= 10000; ++number) {
        if (isPrime(number)) {
            std::cout << "Prime: " << number << std::endl;
        }
    }

    return 0;
}

