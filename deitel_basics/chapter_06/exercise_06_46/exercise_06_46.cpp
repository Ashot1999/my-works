#include <iostream>

int
main()
{
    static int counter = 1;
    std::cout << counter << std::endl;
    ++counter;
    if (10 == counter) {
        return 0;
    }
    main();
}
