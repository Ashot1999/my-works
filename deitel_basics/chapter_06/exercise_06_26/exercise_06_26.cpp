#include <iostream>
#include <cassert>

int
time(const int hour, const int minute, const int second)
{
    assert(hour >= 0 && hour <= 12 && minute >= 0 && minute < 60 && second >= 0 && second < 60);
    const int secondInHour = hour * 3600;
    const int secondInMinute = minute * 60;
    const int totalSecond = secondInHour + secondInMinute + second;
    return totalSecond;
}

void
inputTime(int& hour, int& minute, int& second)
{
    std::cout << "Enter hour: ";
    std::cin >> hour;
    if (hour < 0 || hour > 12) {
        std::cerr << "Error 1: entered hour is not valid" << std::endl;
        ::exit(1);
    }

    std::cout << "Enter minute: ";
    std::cin >> minute;
    if (minute < 0 || minute >= 60) {
        std::cerr << "Error 2: entered minute is not valid" << std::endl;
        ::exit(2);
    }

    std::cout << "Enter second: ";
    std::cin >> second;
    if (second < 0 || second >= 60) {
        std::cerr << "Error 3: entered second is not valid" << std::endl;
        ::exit(3);
    }
}

int
main()
{
    int hour1, minute1, second1;
    int hour2, minute2, second2;
    inputTime(hour1, minute1, second1);
    inputTime(hour2, minute2, second2);
    
    const int time1 = time(hour1, minute1, second1);
    const int time2 = time(hour2, minute2, second2);
    const int result = (time1 > time2) ? time1 - time2 : time2 - time1;
    std::cout << "Passed time: " << result << std::endl;
    return 0;
}

