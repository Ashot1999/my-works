#include <iostream>
#include <iomanip>
#include <cassert>

bool
multiple (const int integer1, const int integer2)
{
    assert(integer2 != 0);
    return integer1 % integer2 == 0;
}

int
main()
{
    ::srand(::time(0));

    for (int counter = 1; counter <= 10; ++counter) {
        const int integer2 = 1 + ::rand() % 20;
        if (0 == integer2) {
            std::cerr << "Error 1: The number by which we are dividing cannot be zero.\a" << std::endl;
            return 1;
        }
        const int integer1 = 1 + ::rand() % 20;
        const bool multipleResult = multiple(integer1, integer2);
        if (multipleResult) {
            std::cout << std::setw(2) << integer1 << " is multiple of " << integer2 << std::endl;
        } else {
            std::cout << std::setw(2) << integer1 << " is not multiple of " << integer2 << std::endl;
        }
    }
    return 0;
}
