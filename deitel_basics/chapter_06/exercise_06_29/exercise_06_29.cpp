#include <iostream>

bool
isPerfect(const int number)
{
    int result = 0;
    for (int counter = 1; counter <= number / 2; ++counter) {
        if (0 == number % counter) {
            result += counter;
        }
    }
    return number == result;
}

void
printPerfectNumber(const int number)
{
    std::cout << "Perfect number: " << number << std::endl;
    std::cout << "Divisors: ";
    for (int i = 1; i <= number / 2; ++i) {
        if (0 == number % i) {
            std::cout << i << " ";
        }
    }
    std::cout << std::endl;
}

int
main()
{
    for (int number = 1; number <= 10000; ++number) {
        const bool isPerfectNumber = isPerfect(number);
    
        if (isPerfectNumber) {
            printPerfectNumber(number);
        }
    }
    return 0;
}
