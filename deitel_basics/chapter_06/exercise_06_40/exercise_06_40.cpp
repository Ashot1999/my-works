#include <iostream>

int
power(const int base, const int exponent)
{
    if (0 == exponent) {
         return 1;
    } else if (0 == exponent % 2) {
        return power(base * base, exponent / 2);
    } else {
        return base * power(base, exponent - 1);
    }
}

int
main()
{
    std::cout << "Input base: ";
    int base;
    std::cin >> base;
    if (0 > base) {
        std::cerr << "Error 1: Enter positive base.\n";
        return 1;
    }
    std::cout << "Input exponent: ";
    int exponent;
    std::cin >> exponent;
    if (0 > exponent) {
        std::cerr << "Error 2: enter positive exponent.\n";
        return 2;
    }
    std::cout << base << " ^ " << exponent << " = " << power(base, exponent) << std::endl;
    return 0;
}
