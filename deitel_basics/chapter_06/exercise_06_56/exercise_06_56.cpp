#include <iostream>

int
tripleCallByValue(int count)
{
    count *= 3;
    return count;
}

int
tripleByReference(int& count)
{
    count *= 3;
    return count;
}

int
main()
{
    std::cout << "Enter number: ";
    int count;
    std::cin >> count;

    std::cout << "Number after calling function by value: "     <<  tripleCallByValue(count) << ". The velue of enetered number became: " << count << std::endl;

    std::cout << "Number after calling function by reference: " <<  tripleByReference(count) << ". The value of enetered number became: " << count << std::endl;

    return 0;
}
