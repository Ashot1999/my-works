#include <iostream>
 
int
main()
{
    ::srand(time(0));
    int randomNumber = 1 + ::rand() % 2;
    std::cout << "Random number from 1 to 2: " << randomNumber << std::endl;
    
    randomNumber = 1 + ::rand() % 100;
    std::cout << "Random number from 1 to 100: " << randomNumber << std::endl;
    
    randomNumber = ::rand() % 10;
    std::cout << "Random number from 0 to 9: " << randomNumber << std::endl;
  
    randomNumber = 1000 + ::rand() % 113;
    std::cout << "Random number from 1000 to 1112: " << randomNumber << std::endl;
  
    randomNumber =  -1 + ::rand() % 3;
    std::cout << "Random number from -1 to 1: " << randomNumber << std::endl;
  
    randomNumber = -3 + ::rand() % 15;
    std::cout << "Random number: from -3 to 11: " << randomNumber << std::endl;
    
    return 0;
 }

