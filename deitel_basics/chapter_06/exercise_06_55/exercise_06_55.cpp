#include <iostream>
#include <cassert>

inline double
circleArea(const double radius)
{
    assert(radius > 0);
    const double pi = 3.14;
    const double circleArea = pi * radius * radius;
    return circleArea;
}

int
main()
{
    std::cout << "Input radius: ";
    double radius;
    std::cin >> radius;

    if (radius <= 0) {
        std::cerr << "Error 1: radius must be positive:" << std::endl;
        return 1;
    }

    std::cout << "Area of circle with radius " << radius << ": " << circleArea(radius) << std::endl;
    return 0;
}
