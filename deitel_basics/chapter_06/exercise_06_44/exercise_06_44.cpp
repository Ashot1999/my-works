#include <iostream>

unsigned long int
factorial(const unsigned int number)
{
    std::cout << number << "! = " << number << " * " << number - 1 << "!" << std::endl;
    if (number <= 1) {
        return 1;
    }
    return number * factorial(number - 1);
}

int
main()
{   
    for (unsigned int number = 1; number <= 10; ++number) {
        std::cout << factorial(number) << std::endl;
    }
    return 0;
}
