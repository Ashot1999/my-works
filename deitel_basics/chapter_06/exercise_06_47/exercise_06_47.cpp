#include <iostream>
#include <cassert>
#include <iomanip>
#include <cmath>
#include <time.h>

void
responseToRightAnswer()
{
    const int answerVersion = 1 + ::rand() % 4;
    switch (answerVersion) {
    case 1: std::cout << "Very good!"              << std::endl; break;
    case 2: std::cout << "Excellent!"              << std::endl; break;
    case 3: std::cout << "Nice work!"              << std::endl; break;
    case 4: std::cout << "Keep up the good work!!" << std::endl; break;
    }
}

void
responseToWrongAnswer()
{
    const int version = 1 + ::rand() % 4;
    switch (version) {
    case 1: std::cout << "No. Please try again." << std::endl; break;
    case 2: std::cout << "Wrong. Try once more." << std::endl; break;
    case 3: std::cout << "Don't give up!"        << std::endl; break;
    case 4: std::cout << "No.Keep trying."       << std::endl; break;
    }
}

int
chooseLevel(const int level)
{
    assert(level <= 3 && level >= 1 && "Assert 1: You only have three options to choose from.\a");
    const int range = ::pow(10, level);
    const int number = ::rand() % range;
    return number;
}

char
getOperationSymbol(int operationNumber)
{
    switch (operationNumber) {
    case 1: return '+';
    case 2: return '*';
    case 3: return '-';
    case 4: return '/';
    }
    assert(0);
}

int
doOperation(int operationNumber, const int number1, const int number2)
{
    switch (operationNumber) {
    case 1: return number1 + number2;
    case 2: return number1 * number2;
    case 3: return number1 - number2;
    case 4: return number1 / number2;
    }
    assert(0);
}

void
askQuestion()
{
    int rightAnswersQuality = 0;
    int wrongAnswersQuality = 0;
    int answersQuality = 0;
    
    std::cout << "Select complexity level (1, 2, 3): ";
    int level;
    std::cin >> level;
    if (1 > level || 3 < level) {
        std::cerr << "Error 1: chosen level is not valid" << std::endl;
        ::exit(1);
    }
    
    std::cout << "Select operation (1 for + , 2 for *, 3 for -, 4 for /, 5 for mixed(+, *, -, /): ";
    int operationNumber;
    std::cin >> operationNumber;
    if (operationNumber < 1 || operationNumber > 5) {
        std::cerr << "Error 2: invalid number" << std::endl;
        ::exit(2);
    }
    int operationNumberCopy = operationNumber;
    for (int i = 1; i <= 10; ++i) {
        if (5 == operationNumber) {
            operationNumberCopy = 1 + ::rand() % 4;
        }
        const int number1 = chooseLevel(level);
        const int number2 = chooseLevel(level);
        while (true) {
            const int rightAnswer = doOperation(operationNumberCopy, number1, number2);
            std::cout << number1 << " " << getOperationSymbol(operationNumberCopy) << " " << number2 << " = ";
            int answer;
            std::cin >> answer;
            ++answersQuality;
            if (rightAnswer == answer) {
                ++rightAnswersQuality;
                responseToRightAnswer();
                break;
            }
            ++wrongAnswersQuality;
            responseToWrongAnswer();
        }
    }
    
    if (answersQuality * 75 / 100 > static_cast<double>(rightAnswersQuality)) {
        std::cout << "/n Please ask your instructor for extra help" << std::endl;
    }
}

int
main()
{
    ::srand(::time(0));
    askQuestion();
    return 0;
}

