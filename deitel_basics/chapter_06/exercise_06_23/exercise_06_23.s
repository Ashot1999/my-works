	.text
	.file	"exercise_06_23.cpp"
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function __cxx_global_var_init
	.type	__cxx_global_var_init,@function
__cxx_global_var_init:                  # @__cxx_global_var_init
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movabsq	$_ZStL8__ioinit, %rdi
	callq	_ZNSt8ios_base4InitC1Ev
	movabsq	$_ZNSt8ios_base4InitD1Ev, %rax
	movq	%rax, %rdi
	movabsq	$_ZStL8__ioinit, %rsi
	movabsq	$__dso_handle, %rdx
	callq	__cxa_atexit
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	__cxx_global_var_init, .Lfunc_end0-__cxx_global_var_init
	.cfi_endproc
                                        # -- End function
	.text
	.globl	_Z11printSquareic       # -- Begin function _Z11printSquareic
	.p2align	4, 0x90
	.type	_Z11printSquareic,@function
_Z11printSquareic:                      # @_Z11printSquareic
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
                                        # kill: def $sil killed $sil killed $esi
	movl	%edi, -4(%rbp)
	movb	%sil, -5(%rbp)
	cmpl	$0, -4(%rbp)
	jle	.LBB1_2
# %bb.1:
	jmp	.LBB1_3
.LBB1_2:
	movabsq	$.L.str, %rdi
	movabsq	$.L.str.1, %rsi
	movl	$7, %edx
	movabsq	$.L__PRETTY_FUNCTION__._Z11printSquareic, %rcx
	callq	__assert_fail
.LBB1_3:
	movl	$1, -12(%rbp)
.LBB1_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
	movl	-12(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jg	.LBB1_11
# %bb.5:                                #   in Loop: Header=BB1_4 Depth=1
	movl	$1, -16(%rbp)
.LBB1_6:                                #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-16(%rbp), %eax
	cmpl	-4(%rbp), %eax
	jg	.LBB1_9
# %bb.7:                                #   in Loop: Header=BB1_6 Depth=2
	movabsq	$_ZSt4cout, %rdi
	movsbl	-5(%rbp), %esi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
# %bb.8:                                #   in Loop: Header=BB1_6 Depth=2
	movl	-16(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -16(%rbp)
	jmp	.LBB1_6
.LBB1_9:                                #   in Loop: Header=BB1_4 Depth=1
	movabsq	$_ZSt4cout, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
# %bb.10:                               #   in Loop: Header=BB1_4 Depth=1
	movl	-12(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -12(%rbp)
	jmp	.LBB1_4
.LBB1_11:
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	_Z11printSquareic, .Lfunc_end1-_Z11printSquareic
	.cfi_endproc
                                        # -- End function
	.globl	main                    # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movl	$0, -4(%rbp)
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.2, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-8(%rbp), %rsi
	movq	%rax, -24(%rbp)         # 8-byte Spill
	callq	_ZNSirsERi
	cmpl	$0, -8(%rbp)
	jg	.LBB2_2
# %bb.1:
	movabsq	$_ZSt4cerr, %rdi
	movabsq	$.L.str.3, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	movabsq	$_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_, %rsi
	callq	_ZNSolsEPFRSoS_E
	movl	$1, -4(%rbp)
	jmp	.LBB2_3
.LBB2_2:
	movabsq	$_ZSt4cout, %rdi
	movabsq	$.L.str.4, %rsi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movabsq	$_ZSt3cin, %rdi
	leaq	-9(%rbp), %rsi
	movq	%rax, -32(%rbp)         # 8-byte Spill
	callq	_ZStrsIcSt11char_traitsIcEERSt13basic_istreamIT_T0_ES6_RS3_
	movl	-8(%rbp), %edi
	movsbl	-9(%rbp), %esi
	movq	%rax, -40(%rbp)         # 8-byte Spill
	callq	_Z11printSquareic
	movl	$0, -4(%rbp)
.LBB2_3:
	movl	-4(%rbp), %eax
	addq	$48, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_exercise_06_23.cpp
	.type	_GLOBAL__sub_I_exercise_06_23.cpp,@function
_GLOBAL__sub_I_exercise_06_23.cpp:      # @_GLOBAL__sub_I_exercise_06_23.cpp
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	__cxx_global_var_init
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end3:
	.size	_GLOBAL__sub_I_exercise_06_23.cpp, .Lfunc_end3-_GLOBAL__sub_I_exercise_06_23.cpp
	.cfi_endproc
                                        # -- End function
	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"side > 0"
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"exercise_06_23.cpp"
	.size	.L.str.1, 19

	.type	.L__PRETTY_FUNCTION__._Z11printSquareic,@object # @__PRETTY_FUNCTION__._Z11printSquareic
.L__PRETTY_FUNCTION__._Z11printSquareic:
	.asciz	"void printSquare(const int, const char)"
	.size	.L__PRETTY_FUNCTION__._Z11printSquareic, 40

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Enter side: "
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Error 1: Side cannot be negative or zero.\007"
	.size	.L.str.3, 43

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Enter symbol: "
	.size	.L.str.4, 15

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_exercise_06_23.cpp
	.ident	"clang version 10.0.0-4ubuntu1 "
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym __cxx_global_var_init
	.addrsig_sym __cxa_atexit
	.addrsig_sym _Z11printSquareic
	.addrsig_sym __assert_fail
	.addrsig_sym _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_c
	.addrsig_sym _ZNSolsEPFRSoS_E
	.addrsig_sym _ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	.addrsig_sym _ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	.addrsig_sym _ZNSirsERi
	.addrsig_sym _ZStrsIcSt11char_traitsIcEERSt13basic_istreamIT_T0_ES6_RS3_
	.addrsig_sym _GLOBAL__sub_I_exercise_06_23.cpp
	.addrsig_sym _ZStL8__ioinit
	.addrsig_sym __dso_handle
	.addrsig_sym _ZSt4cout
	.addrsig_sym _ZSt3cin
	.addrsig_sym _ZSt4cerr
