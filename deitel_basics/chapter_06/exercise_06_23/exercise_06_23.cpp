#include <iostream>
#include <cassert>

void
printSquare(const int side, const char fillCharacter)
{
    assert(side > 0);
    for (int length = 1; length <= side; ++length) {
        for (int width = 1; width <= side; ++width) {
            std::cout << fillCharacter;
        }
        std::cout << std::endl;
    }
}

int
main()
{
    std::cout << "Enter side: ";
    int side;
    std::cin >> side;
    if (side <= 0) {
        std::cerr << "Error 1: Side cannot be negative or zero.\a" << std::endl;
        return 1;
    }

    std::cout << "Enter symbol: ";
    char fillCharacter;
    std::cin >> fillCharacter;
    printSquare(side, fillCharacter);

    return 0;
}

