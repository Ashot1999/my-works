#include <iostream>
#include <cmath>
#include <unistd.h>
#include <iomanip>

double
calculateCharges(const double hours)
{
    if (hours >= 19) {
        return 10.0;
    }

    double charge = 2.0;

    if (hours > 3) {
        charge += std::ceil(hours - 3.0) * 0.5; 
    }

    return charge;
}

int
main()
{    
    std::cout << "Car \t" << "Hours \t" << "Charge" << std::endl;
    double amount = 0.0;
    double time = 0.0;
    
    for (int counter = 1; counter <= 3; ++counter) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter the time: ";
        }
        double parkedHours;
        std::cin >> parkedHours;
        if (parkedHours < 0 || parkedHours > 24) {
            std::cerr << "Error 1: invalid time" << std::endl;
            return 1;
        }

        const double charge = calculateCharges(parkedHours);
                std::cout << std::fixed << counter << std::setw(12) << std::setprecision(1) 
                  << parkedHours << std::setw(9) << std::setprecision(2) 
                  << charge << std::endl;

        amount += charge;
        time += parkedHours;
    }
    
    std::cout << "Total " << std::setw(7) << std::setprecision(1) << time << std::setw(9) 
              << std::setprecision(2) << amount << std::endl;
    
    return 0;
}
