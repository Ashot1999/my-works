#include <iostream>
#include <cmath>

int
main()
{
    std::cout << "Enter numbers to see how fuanctions work: ";
    double x;
    std::cin >> x;

    std::cout << "Function ceil rounds x to the smallest integer not less than x: "    << ::ceil(x)    << std::endl;
    std::cout << "Function cos trigonometric cosine of x (x in radians): "             << ::cos(x)     << std::endl;
    std::cout << "Exponential function e^x: "                                          << ::exp(x)     << std::endl;
    std::cout << "Function floor rounds x to the largest integer not greater than x: " << ::floor(x)   << std::endl; 
    std::cout << "Absolute value of x: "                                               << std::abs(x)  << std::endl;
    return 0;
}
