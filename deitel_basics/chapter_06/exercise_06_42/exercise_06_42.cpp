#include <iostream>

void
hanoiTower(const int quantity, const int column1, const int column3, const int column2)
{
    if (1 == quantity) {
        std::cout << column1 << " -> " << column3 << std::endl;
        return;    
    }

    hanoiTower(quantity - 1, column1, column2, column3);
    std::cout << column1 << " -> " << column3 << std::endl;
    hanoiTower(quantity - 1, column2, column3, column1);
}

int
main()
{
    std::cout << "Enter quantity of disks: ";
    int quantity;
    std::cin >> quantity;
    if (quantity < 0) {
        std::cerr << "Error 1: quantity must be at least 1\n";
        return 1;
    }
    const int column1 = 1;
    const int column2 = 2;
    const int column3 = 3;
    hanoiTower(quantity, column1, column3, column2);

    return 0;

}
