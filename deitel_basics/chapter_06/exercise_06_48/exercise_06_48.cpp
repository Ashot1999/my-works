#include <iostream>
#include <cmath>
#include <unistd.h>
#include <iomanip>

double
distance(const double x1, const double y1, const double x2, const double y2)
{
    const double distanceBetweenX = x1 - x2;
    const double distanceBetweenY = y1 - y2;
    const double value = std::sqrt(distanceBetweenX * distanceBetweenX + distanceBetweenY * distanceBetweenY);
    return value;
}

int
main()
{
    std::cout << "Enter coordinates (x1, y1, x2, y2): ";
    double x1, y1, x2, y2;
    std::cin >> x1 >> y1 >> x2 >> y2;
    std::cout << "Distance: " << distance(x1 ,y1, x2, y2) << std::endl;
    return 0;
}


