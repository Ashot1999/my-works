#include <iostream>

int
randomNumber()
{
    const int number = 1 + ::rand() % 1000;
    return number;
}

void
question()
{
    std::cout << "I have a number between 1 and 1000.\n"
              << "Can you guess my number?\n";
}

void
guessNumber()
{
    bool selectValue = true;
    int counter = 0;
    while (selectValue) {
        const int hiddenNumber = randomNumber();
        std::cout << "Please type your first guess. ";
        while (true) {
            int guessNumber;
            std::cin >> guessNumber;
            if (guessNumber < hiddenNumber) {
                std::cout << "Too few. Try again: ";
            } else if (guessNumber > hiddenNumber) {
                std::cout << "Too much. Try again: ";
            } else {
                std::cout << "Fine! You guessed the number!\n";
                break;
            }
            ++counter;
        }
        if (counter < 10) {
            std::cout << "Either you know the secret or you got lucky !\n";
        } else if (counter == 10) {
            std::cout << "Ahah! You know the secret!\n";
        } else {
            std::cout << "You should be able to do better!\n";
        }
        std::cout << "Would you like to play again (1 for continue 0 for break)? ";
        std::cin >> selectValue;
    }
}
int
main()
{
    ::srand(::time(0));
    question();
    guessNumber();
    return 0;
}

