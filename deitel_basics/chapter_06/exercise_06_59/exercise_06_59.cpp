#include <iostream>

template <typename T>
T
maximum(const T& number1, const T& number2)
{
    return number2 < number1 ? number1 : number2;
}

int
main()
{
    std::cout << "Input 2 integers: ";
    int number1, number2;
    std::cin >> number1 >> number2;
    std::cout << "Maximum: " << maximum(number1, number2) << std::endl;

    std::cout << "Input 2 float numbers: ";
    float numberFloat1, numberFloat2;
    std::cin >> numberFloat1 >> numberFloat2;
    std::cout << "Maximum: " << maximum(numberFloat1, numberFloat2) << std::endl;
    
    std::cout << "Input two symbols: ";
    char symbol1, symbol2;
    std::cin >> symbol1 >> symbol2;
    std::cout << "Maximum: " << maximum(symbol1, symbol2) << std::endl;
    
    return 0;
}

