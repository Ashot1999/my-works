#include <iostream>

int
main()
{
    ::srand(::time(0));
    
    std::cout << "Choose number from (2, 4, 6, 8, 10): " << 2 + (::rand() % 5) * 2 << std::endl;
    
    std::cout << "Choose number from (3, 5, 7, 9, 11): " << 3 + (::rand() % 5)  * 2 << std::endl;
    
    std::cout << "Choose number from (6, 10, 14, 18, 22): " << 6 + (::rand() % 5) * 4 << std::endl;
    return 0;
}
