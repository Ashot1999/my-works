#include <iostream>
#include <cassert>

int
gcd(const int number1, const int number2)
{
    assert(number1 > number2);
    if (0 == number2) {
        return number1;
    }
    
    return gcd(number2, number1 % number2);
}

int
main()
{
    std::cout << "Enter 2 numbers (first number must be greater than the second one): ";
    int number1;
    int number2;
    std::cin >> number1 >> number2;
    if (number2 > number1) {
        std::cerr << "Error 1: second number must be smaller than the first one" << std::endl;
        return 1;
    }
    std::cout << "Greatest common divisor: " << gcd(number1, number2) << std::endl;
    return 0;
}
