#include <iostream>
#include <cmath>

int
reverseNumber(int number)
{
    int order = 1;
    for (int i = number; i >= 1; i /= 10) {
        order *= 10;
    }
    int reminder = 0;
    for (int i = number; i >= 1; i /= 10) {
        reminder += i % 10 * order;
        order /= 10;
    }
    return reminder / 10;
}

int
main()
{
    int number;
    std::cout << "Enter number: ";
    std::cin >> number;
    std::cout << "Reversed number: " << reverseNumber(number) << std::endl;
    return 0;
}
