#include <iostream>

void
printSquare(const int sideOfSquare, const char symbolOfSquare)
{
    for (int length = 1; length <= sideOfSquare; ++length) {
        for (int width = 1; width <= sideOfSquare; ++width) {
            std::cout << symbolOfSquare;
        }
        std::cout << std::endl;
    }
}

void
printTriangle(const int triangle, const char symbolOfTriangle)
{
    for (int counter1 = 1; counter1 <= triangle; ++counter1) {
        for (int counter2 = 1; counter2 <= triangle; ++counter2) {
            if (counter2 < counter1) {
                std::cout << " ";
            } else {
                std::cout << symbolOfTriangle;
            }
        }
        std::cout << std::endl;
    }
}

void
printRectangle(const int side1, const int side2, const char symbolOfRectangle)
{
    for (int i = 1; i <= side1; ++i) {
        for (int j = 1; j <= side2; ++j) {
            std::cout << symbolOfRectangle << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

int
main()
{
    std::cout << "Enter side: ";
    int side;
    std::cin >> side;
    if (side <= 0) {
        std::cerr << "Error 1: Side cannot be negative or zero.\a" << std::endl;
        return 1;
    }
    std::cout << "Enter symbol: ";
    char symbolOfSquare;
    std::cin >> symbolOfSquare;
    printSquare(side, symbolOfSquare);

    std::cout << "Input side of triangle: ";
    int triangle;
    std::cin >> triangle;
    if (triangle < 0) {
        std::cout << "Error 2: The side of triangle cannot be negative.\a" << std::endl;
        return 2;
    }
    std::cout << "Input symbol of triangle: ";
    char symbolOfTriangle;
    std::cin >> symbolOfTriangle;
    printTriangle(triangle, symbolOfTriangle);
    
    std::cout << "Input 2 sides of rectangle: ";
    int side1, side2;
    std::cin >> side1 >> side2;
    if (side1 <= 0 || side2 <= 0) {
        std::cerr << "Error 3: side must be over 0" << std::endl;
        return 3;
    }
    std::cout << "Enter symbol: ";
    char symbolOfRectangle;
    std::cin >> symbolOfRectangle;
    printRectangle(side1, side2, symbolOfRectangle);
    return 0;
}

