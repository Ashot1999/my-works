#include <iostream>

void
askQuestion()
{
    for (int i = 1; i <= 10; ++i) {
        const int number1 = ::rand() % 10;
        const int number2 = ::rand() % 10;
        while (true) {
            int answer;
            std::cout << number1 << " * " << number2 << " = ";
            std::cin >> answer;
            if (number1 * number2 == answer) {
                std::cout << "Great!" << std::endl;
                break;
            } else {
                std::cout << "Wrong answer. Try again!" << std::endl;
            }
        }
    }
}

int
main()
{
    ::srand(::time(0));
    askQuestion();
    return 0;
}
