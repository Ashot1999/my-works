#include <iostream>

bool
isEven(const int number)
{
    const bool result = number % 2 == 0;
    return result;
}

int
main()
{
    ::srand(::time(0));
    for (int counter = 1; counter <= 5; ++counter) {
        const int number = 1 + ::rand() % 20;
        const bool result = isEven(number);
        std::cout << number << (result ? " is even" : " is odd") << std::endl;
    }

    return 0;
}
