#include <iostream>
#include <cmath>
#include <unistd.h>

int
main()
{
    for (int counter = 1; counter <= 5; ++counter) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter 5 numbers: ";
        }

        double number;
        std::cin >> number;
        const double result = ::floor(number + .5);
        std::cout << "Number: " << number << "\t"  << "Result: " << result << std::endl;
    }
    return 0;
}
