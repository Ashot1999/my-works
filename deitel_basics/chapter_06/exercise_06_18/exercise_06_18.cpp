#include <iostream>

int
integerPower(int base, const int exponent)
{
    int result = 1;
    for (int counter = exponent; counter >= 1; counter /= 2) {
        if (1 == (counter & 1)) {
            result *= base;
        }
        base *= base;
    }

    return result;
}

int
main()
{
    std::cout << "Enter base: ";
    int base;
    std::cin >> base;

    std::cout << "Enter exponent: ";
    int exponent;
    std::cin >> exponent;

    if (exponent <= 0) {
        std::cerr << "Error 1: Exponent must be more than 0" << std::endl;
        return 1;
    }

    std::cout << "Base is: " << base << "\n" << "Exponent is: " << exponent << "\n" << "Result is: " << integerPower(base, exponent) << std::endl;
    return 0;
}
