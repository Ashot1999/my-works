#include <string>

class Account
{
public:
    Account(int initialBalance);
    void credit(int addedMoney);
    void debit(int cashedMoney);
    int getBalance();
    void setBalance(int accountBalance);
private:
    int balance_;    
};
