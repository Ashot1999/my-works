#include <iostream>
#include "Account.hpp"

int
main()
{
    int initialBalance;
    std::cout << "Enter your initial balance! ";
    std::cin >> initialBalance;

    //Creating the first class object
    Account account1(initialBalance);
    std::cout << "Choose your action! " <<std::endl;
    std::cout << "To input money to your account press 1!.\n"
              << "To cash your money press 2!.\n"
              << "To check your balance press 3!" 
              << std::endl;

    int selectNumber;
    std::cin >> selectNumber;
    if (selectNumber < 0) {
        std::cout << "Error 1: Invalid number!" << std::endl;
        return 2;
    }
    if (selectNumber > 3) {
        std::cout << "Error 1: Invalid number!" << std::endl;
        return 2;
    }

    if (1 == selectNumber) {
        int addedMoney;
        std::cout << "Input money: " << std::endl;
        std::cin >> addedMoney;
        account1.credit(addedMoney);
        std::cout << "Current balance: " << account1.getBalance() << std::endl;
    }

    if (2 == selectNumber) {
        int cashedMoney;
        std::cout << "Enter ammount you are going to cash: "<< std::endl;
        std::cin >> cashedMoney;
        account1.debit(cashedMoney);
        std::cout << "Current balance: " << account1.getBalance() << std::endl;
    }

    if (3 == selectNumber) {
        std::cout << "Current balance: " << account1.getBalance() << std::endl;
    }

    //Creating the second class object
    std::cout << "Enter your initial balance! ";
    std::cin >> initialBalance;

    Account account2(initialBalance);
    std::cout << "Choose your action! " <<std::endl;
    std::cout << "To input money to your account press 1!.\n" 
              << "To cash your money press 2!.\n" 
              << "To check your balance press 3!" 
              << std::endl;
    std::cin >> selectNumber;    
    if (selectNumber < 0) {
        std::cout << "Error 1: Invalid number!" << std::endl;
        return 1;
    }
    if (selectNumber > 3) {
        std::cout << "Error 1: Invalid number!" <<std::endl;
        return 2;
    }

    if (1 == selectNumber) {
        int addedMoney;
        std::cout << "Input money: " << std::endl;
        std::cin >> addedMoney;
        account2.credit(addedMoney);
        std::cout << "Current balance: " << account2.getBalance() << std::endl;
    }
    
    if (2 == selectNumber) {
        int cashedMoney;
        std::cout << "Enter ammount you are going to cash: "<< std::endl;
        std::cin >> cashedMoney;
        account2.debit(cashedMoney);
        std::cout << "Current balance: " << account2.getBalance() << std::endl;
    }
    
    if (3 == selectNumber) {
        std::cout << "Current balance: " << account2.getBalance() << std::endl;
    }

    return 0;
}
