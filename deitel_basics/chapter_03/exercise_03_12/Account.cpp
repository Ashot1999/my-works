#include <iostream>
#include "Account.hpp"

Account::Account(int initialBalance)
{
    setBalance(initialBalance);   
}

void
Account::credit(int addedMoney)
{
    balance_ += addedMoney;
}

void
Account::debit(int cashedMoney)
{
    if (cashedMoney > balance_) {
        std::cout << "Warning 1: The amount entered exceeds the initial balance." << std::endl;
        return;
    }

    balance_ -= cashedMoney;
}

int
Account::getBalance()
{
    return balance_;
}

void
Account::setBalance(int accountBalance)
{
    balance_ = accountBalance;
    if (accountBalance < 0) {
        std::cout << "Info 1: Negative balance." << std::endl;
        balance_ = 0;
    }
}
