What is a default constructor? How are an object's data members initialized if a class has only an implicitly defined default constructor?.

Answer. In any class that does not explicitly include a constructor, the compiler provides a default constructor that is, a
constructor with no parameters. Data members must be initialized using the member functions of the object if there is only an implicitly defined default constructor.
