#include <iostream>
#include <string>
#include "GradeBook.hpp"

GradeBook::GradeBook(std::string courseName, std::string teacherName)
{
    setCourseName(courseName);
    setTeacherName(teacherName);
}
void
GradeBook::setCourseName(std::string courseName)
{
    courseName_ = courseName;
}
std::string
GradeBook::getCourseName()
{
    return courseName_;
}
void
GradeBook::setTeacherName(std::string teacherName)
{
    teacherName_ = teacherName;
}
std::string
GradeBook::getTeacherName()
{
    return teacherName_;
}
void
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for " << getCourseName() << " ! " << std::endl;
    std::cout << "This course is presented by " << getTeacherName() << std::endl;
}
